package com.calendar.interf;

import com.calendar.model.CalendarDate;

/**
 * Created by ldf on 17/6/15.
 */

public interface OnAdapterSelectListener {
    void cancelSelectState();
    void updateSelectState();
}
