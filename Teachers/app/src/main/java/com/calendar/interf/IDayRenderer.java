package com.calendar.interf;

import android.graphics.Canvas;

import com.calendar.component.State;
import com.calendar.model.CalendarDate;
import com.calendar.view.Day;
import com.calendar.view.DayView;

/**
 * Created by ldf on 17/6/26.
 */

public interface IDayRenderer {

    void refreshContent();

    void drawDay(Canvas canvas, Day day);

    IDayRenderer copy();

}
