package com.teachersapp;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.teachersapp.adapter.BatchAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.bean.BatchBeanID;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.listner.BatchOnClick;
import com.teachersapp.listner.DialogButtonListener;
import com.teachersapp.listner.PaperReloadEvent;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ShareScreen extends ActivityBase implements BatchOnClick, DatePickerDialog.OnDateSetListener {
    private static final String TAG = "ShareScreen";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mSubmit)
    Button mSubmit;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mBranchName)
    TextView mBranchName;
    @BindView(R.id.mBranch)
    Spinner mBranch;
    @BindView(R.id.mPaperList)
    RecyclerView mBatchList;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.mEmptyList)
    TextView mEmptyList;
    @BindView(R.id.mDate)
    TextView mDate;
    @BindView(R.id.mCalender)
    ImageView mCalender;
    Calendar myCalendar = Calendar.getInstance();
    RecyclerView.LayoutManager layoutManager;
    BatchAdapter batchAdapter;
    Subscription subscriptionBatch, subscriptionPublish;
    PaperBean paperBean;
    List<BatchBean> batchBeanList = new ArrayList<>();
    //[{"BatchID":"1"},{"BatchID":"2"}]
    List<BatchBeanID> batchBeanIDS = new ArrayList<>();
    String date = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_screen);
        ButterKnife.bind(this);
        prefs.getInt(Constant.selectedBranch, -1);
        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.paper);
        mPageTitle.setText("Share");
        getBranch(true);

        layoutManager = new LinearLayoutManager(this);
        mBatchList.setLayoutManager(layoutManager);
        mBatchList.setItemAnimator(new DefaultItemAnimator());
        batchAdapter = new BatchAdapter(this, batchBeanList);
        mBatchList.setAdapter(batchAdapter);

        swipeLayout.setOnRefreshListener(() -> swipeLayout.setRefreshing(false));
    }

    @OnClick({R.id.mBackBtn, R.id.mSubmit, R.id.mDate, R.id.mCalender})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mDate:
            case R.id.mCalender:
                DatePickerDialog dialog = new DatePickerDialog(this, this, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMinDate(System.currentTimeMillis() + (86400000*4));
                dialog.show();
                break;
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSubmit:
                if (TextUtils.isEmpty(date)) {
                    Toast.makeText(this, "Please select publish date.", Toast.LENGTH_SHORT).show();
                } else if (batchBeanIDS.size() > 0) {
                    publishPaper(true);
                } else {
                    Toast.makeText(this, "Please select batches.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void callbackPaper(int pos, BatchBean batchBean) {
        batchBeanIDS.clear();
        for (int i = 0; i < batchBeanList.size(); i++) {
            if (batchBeanList.get(i).isSelectBranch()) {
                batchBeanIDS.add(new BatchBeanID(batchBeanList.get(i).getBatchID()));
            }
        }
    }


    private void getBranch(boolean isShow) {
        showProgress(isShow);
        subscriptionBatch = NetworkRequest.performAsyncRequest(restApi.getBranch(paperBean.getMCQPlannerID()), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    batchBeanList.clear();
                    batchBeanList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), BatchBean.class));
                    batchAdapter.notifyDataSetChanged();
                    if (batchBeanList.size() != 0) {
                        mEmptyList.setVisibility(View.GONE);
                        mSubmit.setVisibility(View.VISIBLE);
                    } else {
                        mSubmit.setVisibility(View.GONE);
                        mEmptyList.setTextColor(ContextCompat.getColor(this, R.color.white));
                        mEmptyList.setText(jsonResponse.getString("message"));
                        mEmptyList.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void publishPaper(boolean isShow) {
        String json = gson.toJson(batchBeanIDS);
        Map<String, String> map = new HashMap<>();
        map.put(Constant.PublishDate, date);
        map.put(Constant.MCQPlannerID, paperBean.getMCQPlannerID());
        map.put(Constant.BatchID, json);

        showProgress(isShow);
        subscriptionPublish = NetworkRequest.performAsyncRequest(restApi.publishPaper(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    List<PaperBean> paperBeanList = LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), PaperBean.class);
                    EventBus.getDefault().post(new PaperReloadEvent(paperBeanList.get(0)));
                    Utils.showTwoButtonDialog(this, "", jsonResponse.getString("message"), "OK", null, new DialogButtonListener() {
                        @Override
                        public void onPositiveButtonClicked() {
                            onBackPressed();
                        }

                        @Override
                        public void onNegativButtonClicked() {

                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }


    @Override
    protected void onDestroy() {

        if (subscriptionBatch != null && !subscriptionBatch.isUnsubscribed()) {
            subscriptionBatch.unsubscribe();
            subscriptionBatch = null;
        }
        if (subscriptionPublish != null && !subscriptionPublish.isUnsubscribed()) {
            subscriptionPublish.unsubscribe();
            subscriptionPublish = null;
        }
        super.onDestroy();
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

        String strDate = year + "/" + (month+1) + "/" + dayOfMonth;
        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date da = null;
        try {
            da = (Date) formatter.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("==Date is ==" + da);

        String strDateTime = formatter.format(da);
        Log.d(TAG, "strDateTime: " + strDateTime);
        Log.d(TAG, "onDateSet: " + strDate);
        date = strDateTime;
        mDate.setText(strDateTime);
    }
}
