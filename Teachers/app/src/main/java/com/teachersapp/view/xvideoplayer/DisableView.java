package com.teachersapp.view.xvideoplayer;

public class DisableView {

    boolean isDisable;

    public DisableView(boolean isDisable) {
        this.isDisable = isDisable;
    }

    public boolean isDisable() {
        return isDisable;
    }
}
