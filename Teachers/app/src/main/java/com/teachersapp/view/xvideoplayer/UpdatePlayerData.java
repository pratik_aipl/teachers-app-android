package com.teachersapp.view.xvideoplayer;

public class UpdatePlayerData {
    String currentTime, durations;
    int  currentTimeP = 0, totalTime = 0;


    public UpdatePlayerData(String duration, String currentTime, int currentTimeP, int totalTime) {
        this.currentTime = currentTime;
        this.durations = duration;
        this.currentTimeP = currentTimeP;
        this.totalTime = totalTime;
    }

    public int getCurrentTimeP() {
        return currentTimeP;
    }

    public int getTotalTime() {
        return totalTime;
    }

    public String getCurrentTime() {
        return currentTime;
    }

    public String getDurations() {
        return durations;
    }


}
