package com.teachersapp.view;

import android.content.Context;
import android.webkit.WebView;

public class NoScrollWebView extends QuestionsWebView {
    public NoScrollWebView(Context context) {
        super(context);
    }
    @Override
    public boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY,
                                int scrollRangeX, int scrollRangeY, int maxOverScrollX,
                                int maxOverScrollY, boolean isTouchEvent) {
        return false;
    }

    @Override
    public void scrollTo(int x, int y) {
        // Do nothing
    }

    @Override
    public void computeScroll() {
        // Do nothing
    }
}
