package com.teachersapp;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.teachersapp.adapter.LegalAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.LegalData;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class Legal extends ActivityBase {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;


    Subscription subscriptionLegal;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mPaperList)
    RecyclerView mPaperList;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.mEmptyList)
    TextView mEmptyList;

    private RecyclerView.LayoutManager layoutManager;
    private LegalAdapter legalAdapter;
    List<LegalData> legalList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal);
        ButterKnife.bind(this);

        mPageTitle.setText("Legal");


        layoutManager = new LinearLayoutManager(this);
        mPaperList.setLayoutManager(layoutManager);
        mPaperList.setItemAnimator(new DefaultItemAnimator());

        legalAdapter = new LegalAdapter(this, legalList);
        mPaperList.setAdapter(legalAdapter);
        if (Utils.isNetworkAvailable(this, true))
            getLegalData(true);
    }

    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        if (subscriptionLegal != null && !subscriptionLegal.isUnsubscribed()) {
            subscriptionLegal.isUnsubscribed();
            subscriptionLegal = null;
        }
        super.onDestroy();
    }

    private void getLegalData(boolean isShow) {
        Map<String, String> map = new HashMap<>();

        showProgress(isShow);
        subscriptionLegal = NetworkRequest.performAsyncRequest(restApi.getLegal(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    legalList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), LegalData.class));
                    legalAdapter.notifyDataSetChanged();
                    mEmptyList.setTextColor(ContextCompat.getColor(this, R.color.white));
                    if (legalList.size() != 0) {
                        mEmptyList.setVisibility(View.GONE);
                    } else {
                        mEmptyList.setText(getString(R.string.empty_other_list_msg));
                        mEmptyList.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

}
