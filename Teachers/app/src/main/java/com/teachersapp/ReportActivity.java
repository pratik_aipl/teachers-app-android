package com.teachersapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.teachersapp.adapter.LevelAdapter;
import com.teachersapp.adapter.ReportQuestionsAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.LevelBean;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.bean.PlannerSummary;
import com.teachersapp.fragment.BottomSheetBatchDialog;
import com.teachersapp.fragment.BottomSheetQuestionsDialog;
import com.teachersapp.fragment.BottomSheetTypeDialog;
import com.teachersapp.listner.BatchChangeEvent;
import com.teachersapp.listner.ChangeTypeEvent;
import com.teachersapp.listner.LevelOnClick;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

import static com.teachersapp.utils.Utils.stringForTime;

public class ReportActivity extends ActivityBase implements LevelOnClick {
    private static final String TAG = "ReportActivity";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mBatchList)
    RecyclerView mBatchList;
    @BindView(R.id.mLine)
    View mLine;
    @BindView(R.id.mAccuracy)
    TextView mAccuracy;
    @BindView(R.id.mPerformance)
    TextView mPerformance;
    Subscription subscriptionReports;
    PaperBean paperBean;
    @BindView(R.id.mStudent)
    TextView mStudent;
    @BindView(R.id.mNotAttemptStudent)
    TextView mNotAttemptStudent;
    @BindView(R.id.mComplete)
    TextView mComplete;
    @BindView(R.id.mQuestions)
    TextView mQuestions;
    @BindView(R.id.mAverageTime)
    TextView mAverageTime;
    @BindView(R.id.mTimeTaken)
    TextView mTimeTaken;
    @BindView(R.id.mAvgTime)
    TextView mAvgTime;
    @BindView(R.id.mQuestionsList)
    RecyclerView mQuestionsList;
    @BindView(R.id.mDataView)
    NestedScrollView mDataView;
    @BindView(R.id.mProgress)
    ProgressBar mProgress;
    @BindView(R.id.mErrorMessage)
    TextView mErrorMessage;

    boolean isAttend = true;

    public BottomSheetQuestionsDialog bottomSheetQuestionsDialog;
    public BottomSheetBatchDialog bottomSheetBatchDialog;
    public BottomSheetTypeDialog bottomSheetTypeDialog;

    //    List<BatchBean> branchBeanList = new ArrayList<>();
    PlannerSummary plannerSummary;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        ButterKnife.bind(this);
        //      prefs.save(Constant.selectedBranch, -1);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        mPageTitle.setText("Reports");
        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.paper);

        if (Utils.isNetworkAvailable(this, true))
            getReports(false, "");

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPaperReloadEventEvent(BatchChangeEvent event) {
        if (Utils.isNetworkAvailable(this, true)) {
//            mSelectBranch.setText("Select Batch (" + event.getBatchBean().getBatchName().substring(0, event.getBatchBean().getBatchName().indexOf('(')) + ")");
            getReports(true, String.valueOf(event.getBatchBean().getBatchID()));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChangeTypeEvent(ChangeTypeEvent event) {
//        showView(event.isAttend());
    }

    @Override
    protected void onDestroy() {
        prefs.save(Constant.selectedBranch, -1);
        if (subscriptionReports != null && !subscriptionReports.isUnsubscribed()) {
            subscriptionReports.unsubscribe();
            subscriptionReports = null;
        }
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void getReports(boolean isShow, String selectBatch) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MCQPlannerID, paperBean.getMCQPlannerID());
        if (!TextUtils.isEmpty(selectBatch))
            map.put(Constant.BatchID, selectBatch);

        showProgress(isShow);
        Log.d(TAG, "getReports: " + map);
        subscriptionReports = NetworkRequest.performAsyncRequest(restApi.plannerSummary(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    mErrorMessage.setText("");
                    JSONObject jsonResponse = new JSONObject(data.body());
                    mDataView.setVisibility(View.VISIBLE);
                    mProgress.setVisibility(View.GONE);
                    plannerSummary = LoganSquare.parse(jsonResponse.get(Constant.data).toString(), PlannerSummary.class);
                    mNotAttemptStudent.setText("" + plannerSummary.getNotAttandStudent().size());
                    mComplete.setText("" + plannerSummary.getAttandStudent().size());
                    mAccuracy.setText(plannerSummary.getAccuracy() + "%");
                    mBatchList.setAdapter(new LevelAdapter(this, plannerSummary.getLevelAccuracy(), false));
                    mTimeTaken.setText(stringForTime(plannerSummary.getAvarageTakenTime()));
                    mAvgTime.setText(stringForTime(plannerSummary.getAvarageTime()));
                    mQuestionsList.setAdapter(new ReportQuestionsAdapter(this, plannerSummary.getQuestions(), Constant.report1));
                } catch (Exception e) {
                    e.printStackTrace();
                    mProgress.setVisibility(View.GONE);
                    mErrorMessage.setText(e.getLocalizedMessage());
                }
            } else {
                mProgress.setVisibility(View.GONE);
                mErrorMessage.setText(Utils.serviceStatusFalseProcess(this, data));
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
            mProgress.setVisibility(View.GONE);
            mErrorMessage.setText(e.getMessage());
        });
    }


    @OnClick({R.id.mBackBtn, R.id.mPerformance, R.id.mStudent, R.id.mQuestions})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mPerformance:
                startActivity(new Intent(this, ReportPerformanceActivity.class)
                        .putExtra(Constant.paper, paperBean)
                        .putExtra(Constant.batchList, (Serializable) plannerSummary.getBatch()));
                break;
            case R.id.mStudent:
                startActivity(new Intent(this, ReportStudentActivity.class)
                        .putExtra(Constant.paper, paperBean)
                        .putExtra(Constant.notAttandStudentList, (Serializable) plannerSummary.getNotAttandStudent())
                        .putExtra(Constant.attandStudentList, (Serializable) plannerSummary.getAttandStudent())
                        .putExtra(Constant.batchList, (Serializable) plannerSummary.getBatch()));
                break;
            case R.id.mQuestions:
                startActivity(new Intent(this, ReportQuestionsActivity.class)
                        .putExtra(Constant.paper, paperBean)
                        .putExtra(Constant.questionsList, (Serializable) plannerSummary.getQuestions()));

                break;
        }
    }

    @Override
    public void callbackLevel(int pos, LevelBean levelBean) {
        startActivity(new Intent(this, QuestionsActivity.class)
                .putExtra(Constant.levelName, levelBean.getLevelName())
                .putExtra(Constant.levelQuestionsList, (Serializable) levelBean.getLevelQuestionList()));

    }
}
