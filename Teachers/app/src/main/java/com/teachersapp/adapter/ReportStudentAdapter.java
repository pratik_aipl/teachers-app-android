package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.ReportAssignmentActivity;
import com.teachersapp.bean.AttendStudent;
import com.teachersapp.listner.StudentOnClick;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class ReportStudentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<AttendStudent> attendStudentList;
    StudentOnClick studentOnClick;


    // Provide a suitable constructor (depends on the kind of dataset)
    public ReportStudentAdapter(ReportAssignmentActivity context, List<AttendStudent> attendStudentList) {
        this.context = context;
        this.attendStudentList = attendStudentList;
        this.studentOnClick = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_student_report_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        AttendStudent attendStudent = attendStudentList.get(position);

        if(position==0){
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.top_cornor_round));
        }else if (position== attendStudentList.size()-1){
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_cornor_round));
        }else{
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));
        }

        holder.mStudentName.setText(String.format(context.getString(R.string.student_report), attendStudent.getStudentName(), attendStudent.getBatchName(), attendStudent.getBranchName()));
        Log.d(TAG, "onBindViewHolder: ");
        holder.mPercentage.setText(attendStudent.getStudentAccuracy()+"%");
        holder.mProgrees.setProgress((int)attendStudent.getStudentAccuracy());
        holder.mStudentName.setOnClickListener(v -> studentOnClick.callbackStudent(position, attendStudent));

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return attendStudentList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mStudentName)
        TextView mStudentName;
        @BindView(R.id.mPercentage)
        TextView mPercentage;
        @BindView(R.id.mProgrees)
        ProgressBar mProgrees;
        @BindView(R.id.mContainer)
        LinearLayout mContainer;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }


}
