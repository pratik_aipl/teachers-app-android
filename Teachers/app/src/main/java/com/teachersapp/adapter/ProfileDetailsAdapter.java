package com.teachersapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.bean.Standard;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileDetailsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<Standard> standardList;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProfileDetailsAdapter(Context context, List<Standard> standardList) {
        this.context = context;
        this.standardList = standardList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.branch_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        Standard standard = standardList.get(position);
        holder.mChapterName.setText(standard.getStandard());

        if (standard.getSubjectList() != null) {
            ProfileSubjectAdapter standardAdapter = new ProfileSubjectAdapter(context, standardList.get(position).getSubjectList());
            holder.mChapter.setAdapter(standardAdapter);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return standardList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mChapterName)
        TextView mChapterName;
        @BindView(R.id.mChapter)
        RecyclerView mChapter;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
