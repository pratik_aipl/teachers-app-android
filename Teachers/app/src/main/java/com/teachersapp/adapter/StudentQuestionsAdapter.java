package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.bean.LevelQuestion;
import com.teachersapp.view.OptionsWebView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StudentQuestionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<LevelQuestion> levelQuestions;
    boolean isFromPlanner;
    double performance;

    public StudentQuestionsAdapter(Context context, List<LevelQuestion> levelQuestions, boolean isFromPlanner, double performance) {
        this.context = context;
        this.levelQuestions = levelQuestions;
        this.isFromPlanner = isFromPlanner;
        this.performance = performance;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_questions_answer_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        LevelQuestion levelQuestion = levelQuestions.get(position);

        holder.mQueNo.setText((position + 1) + ")");
        holder.mQuestionText.loadHtmlFromLocal(levelQuestion.getQuestion(), "#000000");
        holder.mQuestionText.setInitialScale(50);
        holder.mOptionList.setAdapter(new OptionAdapter(context, levelQuestion.getOptions(), true, levelQuestion.getAnswerID(), levelQuestion.getIsCorrect(),  isFromPlanner,  performance));

        holder.mQueAction.setVisibility(View.VISIBLE);
        if (levelQuestion.getIsCorrect() != -1) {
            holder.mQueAction.setVisibility(View.VISIBLE);
            if (levelQuestion.getIsCorrect() == 0)
                holder.mQueAction.setImageResource(R.mipmap.wrong);
            else
                holder.mQueAction.setImageResource(R.mipmap.rigth);
        } else {
            holder.mQueAction.setVisibility(View.INVISIBLE);
        }

        if (isFromPlanner && performance==0){
            holder.mQueAction.setVisibility(View.INVISIBLE);
        }else {
            holder.mQueAction.setVisibility(View.VISIBLE);
        }

        if (position == 0 && isFromPlanner)
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
        else if (position == levelQuestions.size() - 1)
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
        else
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return levelQuestions.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mQuestionText)
        OptionsWebView mQuestionText;

        @BindView(R.id.mQueNo)
        TextView mQueNo;
        @BindView(R.id.mQueAction)
        ImageView mQueAction;

        @BindView(R.id.mOptionList)
        RecyclerView mOptionList;
        @BindView(R.id.mContainer)
        LinearLayout mContainer;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
