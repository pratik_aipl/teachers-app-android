package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teachersapp.PaperScreen;
import com.teachersapp.R;
import com.teachersapp.ShareScreen;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.bean.PublishedBatchBean;
import com.teachersapp.listner.BatchOnClick;
import com.teachersapp.listner.PublishDateOnClick;
import com.teachersapp.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class PublishBatchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<PublishedBatchBean> batchBeans;
    PublishDateOnClick batchOnClick;

    // Provide a suitable constructor (depends on the kind of dataset)
    public PublishBatchAdapter(PaperScreen context, List<PublishedBatchBean> paperBeans) {
        this.context = context;
        this.batchBeans = paperBeans;
        this.batchOnClick = (PublishDateOnClick) context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.publish_batch_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        PublishedBatchBean batchBean = batchBeans.get(position);

        holder.mBatchName.setText(context.getString(R.string.namewithbatch,batchBean.getBatch(), batchBean.getPublishDate()));



        holder.mChangeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                batchOnClick.callChangeDate(position,batchBean);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return batchBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mBatchName)
        TextView mBatchName;
        @BindView(R.id.mChangeDate)
        ImageView mChangeDate;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
