package com.teachersapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.teachersapp.PaperScreen;
import com.teachersapp.R;
import com.teachersapp.ReportActivity;
import com.teachersapp.ReportPerformanceActivity;
import com.teachersapp.bean.LevelBean;
import com.teachersapp.fragment.ReportFragment;
import com.teachersapp.listner.LevelOnClick;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class LevelAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<LevelBean> testBeans;
    int size = 0;
    LevelOnClick levelOnClick;
    boolean fromPaperScreen;

    public LevelAdapter(PaperScreen context, List<LevelBean> testBeans, boolean fromPaperScreen) {
        this.context = context;
        this.testBeans = testBeans;
        this.levelOnClick = (LevelOnClick) context;
        this.fromPaperScreen = fromPaperScreen;
    }

    public LevelAdapter(ReportActivity context, List<LevelBean> testBeans, boolean fromPaperScreen) {
        this.context = context;
        this.testBeans = testBeans;
        this.levelOnClick = (LevelOnClick) context;
        this.fromPaperScreen = fromPaperScreen;
    }

    public LevelAdapter(ReportPerformanceActivity context, List<LevelBean> testBeans, boolean fromPaperScreen) {
        this.context = context;
        this.testBeans = testBeans;
        this.fromPaperScreen = fromPaperScreen;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        if (fromPaperScreen)
            return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_report_level_item, parent, false));
        else
            return new ViewHolderBatch(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_report_batch_perform_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        LevelBean levelBean = testBeans.get(position);
        if (fromPaperScreen) {
            ViewHolder holder = (ViewHolder) holderIn;
            if (size == 0) {
                holder.mLevelName.setText(TextUtils.isEmpty(levelBean.getLevelName())?"Level 0":levelBean.getLevelName());
                holder.mLevelValue.setText("" + levelBean.getNoOfQuestions());
                if (levelBean.getNoOfQuestions() > 0) {
                    holder.mView.setVisibility(View.VISIBLE);
                } else {
                    holder.mView.setVisibility(View.GONE);
                }
                holder.mView.setOnClickListener(v -> levelOnClick.callbackLevel(position, levelBean));
                holder.mLevelValue.setOnClickListener(v -> levelOnClick.callbackLevel(position, levelBean));
                holder.mLevelProgress.setOnClickListener(v -> levelOnClick.callbackLevel(position, levelBean));

                if (position == (testBeans.size() - 1))
                    holder.mLine.setVisibility(View.GONE);
                else
                    holder.mLine.setVisibility(View.VISIBLE);
            }
        } else {
            ViewHolderBatch holder = (ViewHolderBatch) holderIn;

            holder.mLevelName.setText(levelBean.getLevelName());
            holder.mLevelValue.setText(levelBean.getTotalCorrect() + "-" + levelBean.getNoOfQuestions());
            holder.mPerformance.setText(levelBean.getTotalAvarage() + " %");

            if (levelBean.getTotalAvarage() > 0) {
                holder.mLevelValue.setVisibility(View.VISIBLE);
            } else {
                holder.mLevelValue.setVisibility(View.GONE);
            }


            if (position == (testBeans.size() - 1))
                holder.mLine.setVisibility(View.GONE);
            else
                holder.mLine.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return size == 0 ? testBeans.size() : size;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mLevelProgress)
        ProgressBar mLevelProgress;
        @BindView(R.id.mLevelValue)
        TextView mLevelValue;
        @BindView(R.id.mLevelName)
        TextView mLevelName;
        @BindView(R.id.mView)
        ImageView mView;
        @BindView(R.id.mLine)
        View mLine;


        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }


    public static class ViewHolderBatch extends RecyclerView.ViewHolder {

        @BindView(R.id.mLine)
        View mLine;
        @BindView(R.id.mLevelProgress)
        ProgressBar mLevelProgress;
        @BindView(R.id.mPerformance)
        TextView mPerformance;
        @BindView(R.id.mLevelValue)
        TextView mLevelValue;
        @BindView(R.id.mLevelName)
        TextView mLevelName;


        public ViewHolderBatch(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
