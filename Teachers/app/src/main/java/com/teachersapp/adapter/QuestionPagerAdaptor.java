package com.teachersapp.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.teachersapp.bean.LevelQuestion;
import com.teachersapp.fragment.QuestionPageFragment;

import java.util.List;

public class QuestionPagerAdaptor extends FragmentPagerAdapter {
    List<LevelQuestion> questionsBeans;
    long startTime;

    public QuestionPagerAdaptor(FragmentManager fm, List<LevelQuestion> questionsBeans,long startTime) {
        super(fm);
        this.questionsBeans = questionsBeans;
        this.startTime = startTime;
    }

    @Override
    public Fragment getItem(int i) {
          return QuestionPageFragment.newInstance(i, questionsBeans.get(i),startTime);
    }

    @Override
    public int getCount() {
        return questionsBeans.size();
    }
}
