package com.teachersapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.bean.Assignment;
import com.teachersapp.utils.Utils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AssignmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ReportQuestionsAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<Assignment> assignments;
    boolean page;

    public AssignmentAdapter(Context context, List<Assignment> assignments, boolean page) {
        this.context = context;
        this.assignments = assignments;
        this.page = page;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_assignment_pending_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;

        Assignment assignment = assignments.get(position);

        holder.mAssignmentTitle.setText(assignment.getPlannerName());
        holder.mAssignmentDes.setText(context.getString(R.string.subject_standard_midume_board,assignment.getSubjectName(), assignment.getStandardName(), assignment.getMediumName(), assignment.getBoardName()));

        holder.mStartDate.setText(String.format(context.getString(R.string.startdate), (Utils.changeDateToDDMMYYYY(assignment.getStartDate()))));
        holder.mEndDate.setText(String.format(context.getString(R.string.enddate), (Utils.changeDateToDDMMYYYY(assignment.getEndDate()))));

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return page ? assignments.size() : assignments.size() > 2 ? 2 : assignments.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.mAssignmentTitle)
        TextView mAssignmentTitle;
        @BindView(R.id.mAssignmentDes)
        TextView mAssignmentDes;
        @BindView(R.id.mStartDate)
        TextView mStartDate;
        @BindView(R.id.mEndDate)
        TextView mEndDate;
        @BindView(R.id.mLine)
        View mLine;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
