package com.teachersapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teachersapp.AllAssignmentActivity;
import com.teachersapp.R;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.fragment.DashBoardFragment;
import com.teachersapp.fragment.SearchFragment;
import com.teachersapp.listner.PaperOnClick;
import com.teachersapp.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class PaperAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    public final static int VIEW_TYPE_HEADER = 1;
    private static final int VIEW_TYPE_EMPTY = 2;
    Context context;
    List<PaperBean> paperBeans;
    PaperOnClick paperOnClick;
    boolean isFilter = false;
    boolean hideTime = false;

    ViewHolderHeader tempHeaderTextView = null;

    // Provide a suitable constructor (depends on the kind of dataset)
    public PaperAdapter(Context context, List<PaperBean> paperBeans, DashBoardFragment dashBoardFragment) {
        this.context = context;
        this.paperBeans = paperBeans;
        hideTime = false;
        this.paperOnClick = dashBoardFragment;
    }

    public PaperAdapter(Context context, List<PaperBean> paperBeans, AllAssignmentActivity assignmentActivity) {
        this.context = context;
        this.paperBeans = paperBeans;
        this.isFilter = false;
        hideTime = true;
        this.paperOnClick = assignmentActivity;
    }

    public PaperAdapter(Context context, List<PaperBean> paperBeans, SearchFragment searchFragment, boolean isFilter) {
        this.context = context;
        hideTime = false;
        this.isFilter = isFilter;
        this.paperBeans = paperBeans;
        this.paperOnClick = searchFragment;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: view type " + viewType);
        if (!hideTime) {
            if (!isFilter && viewType == VIEW_TYPE_HEADER) {
                return new ViewHolderHeader(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_paper_header_item, parent, false));
            } else if (viewType == VIEW_TYPE_EMPTY) {
                return new ViewHolderEmpty(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_paper_empty, parent, false));
            } else {
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_paper_item, parent, false));
            }
        } else {
            if (viewType == VIEW_TYPE_EMPTY) {
                return new ViewHolderEmpty(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_paper_empty, parent, false));
            } else {
                return new ViewHolderAssignment(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_assignment_item, parent, false));
            }
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        if (holderIn instanceof ViewHolder) {
            ViewHolder holder = (ViewHolder) holderIn;
            PaperBean paperBean = paperBeans.get(isFilter ? position : position - 1);
            holder.mPaperView.setOnClickListener(v -> paperOnClick.callbackPaper(position, paperBean));
            holder.mPaperName.setText(paperBean.getPlannerName());
            holder.mSubject.setText(paperBean.getSubjectName());
            holder.mStandard.setText(paperBean.getStandardName());
            holder.mMedium.setText(paperBean.getMediumName() + "(" + paperBean.getBoardName() + ")");
            holder.mBranchName.setText(paperBean.getBranchName());

            holder.mTime.setVisibility(View.VISIBLE);
            holder.mTime.setText(Utils.changeDateToDDMMYYYY(paperBean.getEndDate()));
        } else if (holderIn instanceof ViewHolderAssignment) {
            ViewHolderAssignment holder = (ViewHolderAssignment) holderIn;
            PaperBean paperBean = paperBeans.get(position);
            holder.mPaperView.setOnClickListener(v -> paperOnClick.callbackPaper(position, paperBean));
            holder.mPaperName.setText(paperBean.getPlannerName());
            holder.mSubject.setText(paperBean.getSubjectName());
            holder.mStandard.setText(paperBean.getStandardName());
            holder.mMedium.setText(paperBean.getMediumName() + "(" + paperBean.getBoardName() + ")");
            holder.mBranchName.setText(paperBean.getBranchName());


            if (paperBean.getPending_students() == 0 && paperBean.getTotal_completed_students() == 0) {
                holder.mPending.setVisibility(View.GONE);
                holder.mComplete.setVisibility(View.GONE);
            } else {
                holder.mPending.setVisibility(View.VISIBLE);
                holder.mComplete.setVisibility(View.VISIBLE);

                holder.mPending.setText(context.getString(R.string.pending, paperBean.getPending_students()));
                holder.mComplete.setText(context.getString(R.string.completed, paperBean.getTotal_completed_students()));

            }

        } else if (holderIn instanceof ViewHolderHeader) {
            ViewHolderHeader holder = (ViewHolderHeader) holderIn;
            Log.d(TAG, "onBindViewHolder: ViewHolderHeader ");
            if (tempHeaderTextView == null) {
                Calendar cal = Calendar.getInstance();
                holder.mSelectedDayLabel.setText(new SimpleDateFormat("EEEE").format(cal.getTime()) + "\'s Assignments");
            }
            tempHeaderTextView = holder;
        } else {
            Log.d(TAG, "onBindViewHolder: no instance of view holder found");
        }


    }

    @Override
    public int getItemViewType(int position) {
        Log.d(TAG, "getItemViewType: " + paperBeans.size());

        if (isFilter) {
            return VIEW_TYPE_ITEM;
        } else if (hideTime) {
            if (paperBeans.size() == 0) {
                return VIEW_TYPE_EMPTY;
            } else {
                return VIEW_TYPE_ITEM;
            }
        } else {
            if (paperBeans.size() == 0) {
                return position == 0 ? VIEW_TYPE_HEADER : VIEW_TYPE_EMPTY;
            } else {
                return position == 0 ? VIEW_TYPE_HEADER : VIEW_TYPE_ITEM;
            }
        }
    }

    @Override
    public int getItemCount() {
        if (hideTime) {
            if (paperBeans.size() == 0) {
                return 1;
            } else {
                return paperBeans.size();
            }
        } else {
            if (isFilter) {
                return paperBeans.size();
            } else {
                if (paperBeans.size() == 0) {
                    return 2;
                } else {
                    return paperBeans.size() + 1;
                }
            }
        }
    }

    public void setHeaderText(String selectedDay) {
        Log.d(TAG, "setHeaderText: " + selectedDay + " " + (tempHeaderTextView != null));
        if (tempHeaderTextView != null)
            tempHeaderTextView.mSelectedDayLabel.setText(selectedDay);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mPaperName)
        TextView mPaperName;
        @BindView(R.id.mTime)
        TextView mTime;
        @BindView(R.id.mSubject)
        TextView mSubject;
        @BindView(R.id.mStandard)
        TextView mStandard;
        @BindView(R.id.mMedium)
        TextView mMedium;
        @BindView(R.id.mBranchName)
        TextView mBranchName;
        @BindView(R.id.mPaperView)
        LinearLayout mPaperView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public static class ViewHolderAssignment extends RecyclerView.ViewHolder {

        @BindView(R.id.mPaperName)
        TextView mPaperName;
        @BindView(R.id.mStudent)
        TextView mStudent;
        @BindView(R.id.mSubject)
        TextView mSubject;
        @BindView(R.id.mStandard)
        TextView mStandard;
        @BindView(R.id.mMedium)
        TextView mMedium;
        @BindView(R.id.mPending)
        TextView mPending;
        @BindView(R.id.mComplete)
        TextView mComplete;
        @BindView(R.id.mBranchName)
        TextView mBranchName;
        @BindView(R.id.mPaperView)
        LinearLayout mPaperView;

        public ViewHolderAssignment(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public static class ViewHolderHeader extends RecyclerView.ViewHolder {
        @BindView(R.id.mSelectedDayLabel)
        public TextView mSelectedDayLabel;

        public ViewHolderHeader(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }


    public static class ViewHolderEmpty extends RecyclerView.ViewHolder {
        public ViewHolderEmpty(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
