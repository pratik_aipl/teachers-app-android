package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.ReportPerformanceActivity;
import com.teachersapp.bean.BatchBean;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReportPerformanceBatchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "ReportQuestionsAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<BatchBean> batchBeanList;

    public ReportPerformanceBatchAdapter(ReportPerformanceActivity context, List<BatchBean> batchBeanList) {
        this.context = context;
        this.batchBeanList = batchBeanList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_report_perform_item, parent, false));


    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {
        ViewHolder holder = (ViewHolder) holderIn;
        BatchBean batchBean=batchBeanList.get(position);
        if (position == 0) {
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.top_cornor_round));
        } else if (position == batchBeanList.size() - 1) {
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_cornor_round));
        } else {
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));
        }

        holder.mPerformance.setText(batchBean.getBatchName());
        holder.mAccuracy.setText(batchBean.getAccuracy()+"%");

        holder.mBatchList.setAdapter(new LevelAdapter((ReportPerformanceActivity) context, batchBean.getLevelAccuracy(), false));

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return batchBeanList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.mPerformance)
        TextView mPerformance;
        @BindView(R.id.mAccuracy)
        TextView mAccuracy;
        @BindView(R.id.mBatchList)
        RecyclerView mBatchList;
        @BindView(R.id.mContainer)
        LinearLayout mContainer;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
