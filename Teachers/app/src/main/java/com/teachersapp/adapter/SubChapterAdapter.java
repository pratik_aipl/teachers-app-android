package com.teachersapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.bean.ChapterBean;
import com.teachersapp.utils.Constant;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class SubChapterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<ChapterBean> chapterBeans;

    // Provide a suitable constructor (depends on the kind of dataset)
    public SubChapterAdapter(Context context, List<ChapterBean> chapterBeans) {
        this.context = context;
        this.chapterBeans = chapterBeans;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.sub_chapter_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        ChapterBean paperBean = chapterBeans.get(position);
        holder.mChapterName.setText(paperBean.getChapterName());
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return chapterBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mChapterName)
        TextView mChapterName;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
