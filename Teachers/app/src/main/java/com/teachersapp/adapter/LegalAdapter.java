package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.teachersapp.Legal;
import com.teachersapp.R;
import com.teachersapp.bean.LegalData;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.fragment.DashBoardFragment;
import com.teachersapp.fragment.SearchFragment;
import com.teachersapp.listner.PaperOnClick;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class LegalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    private static int currentPosition = 0;
    Context context;
    List<LegalData> legalList;

    // Provide a suitable constructor (depends on the kind of dataset)
    public LegalAdapter(Context context, List<LegalData> legalList) {
        this.context = context;
        this.legalList = legalList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_legal_item, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        LegalData paperBean = legalList.get(position);
        holder.groupTitle.setText(paperBean.getName());
        holder.mDetails.setText(paperBean.getValue());
        holder.mDetailsView.setVisibility(View.GONE);
        holder.mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.top_left_cornor_round_gray));

        if (position == 0) {
            holder.mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.top_left_cornor_round_gray));
            holder.mViewGroupD.setBackground(null);
        } else if (position == legalList.size() - 1) {
            holder.mViewGroupD.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_left_cornor_round_gray));
//            holder.mViewGroup.setBackground(null);
        } else {
            holder.mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.top_left_cornor_round_gray));
            holder.mViewGroupD.setBackground(null);
        }

        if (currentPosition == position) {
            //creating an animation
            Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);
            //toggling visibility
            holder.mDetailsView.setVisibility(View.VISIBLE);
            if (position == legalList.size() - 1) {
                holder.mViewGroup.setBackground(null);
            }else {
                holder.mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.top_left_cornor_round_gray));
            }
            //adding sliding effect
            holder.mDetailsView.startAnimation(slideDown);
        }


        holder.groupTitle.setOnClickListener(v -> {

            currentPosition = position;
            //reloding the list
            notifyDataSetChanged();
        });

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return legalList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mViewContainer)
        LinearLayout mViewContainer;
        @BindView(R.id.mViewGroup)
        View mViewGroup;
        @BindView(R.id.mDetailsView)
        RelativeLayout mDetailsView;
        @BindView(R.id.mViewGroupD)
        View mViewGroupD;
        @BindView(R.id.groupTitle)
        TextView groupTitle;
        @BindView(R.id.mDetails)
        TextView mDetails;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }


}
