package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.bean.BatchBean;

import java.util.List;

public class BatchFilterAdapter extends BaseAdapter {
    List<BatchBean> batchBeans;
    Context context;
    boolean isBatch;

    public long getItemId(int i) {
        return (long) i;
    }

    public BatchFilterAdapter(Context context2, List<BatchBean> list, boolean isBatch) {
        this.batchBeans = list;
        this.context = context2;
        this.isBatch = isBatch;
    }

    public int getCount() {
        return this.batchBeans.size();
    }

    public BatchBean getItem(int i) {
        return (BatchBean) this.batchBeans.get(i);
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        BatchBean item = getItem(i);
        View inflate = LayoutInflater.from(this.context).inflate(R.layout.raw_spinner_item, null);
        TextView textView = (TextView) inflate.findViewById(R.id.mText);
        if (i == 0) {
            textView.setTextColor(ContextCompat.getColor(this.context, R.color.gray));
        } else {
            textView.setTextColor(ContextCompat.getColor(this.context, R.color.black));
        }
        if (isBatch)
            textView.setText(item.getBatch());
        else
            textView.setText(item.getBatchName());
        return inflate;
    }
}
