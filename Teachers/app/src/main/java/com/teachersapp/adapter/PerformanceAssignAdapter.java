package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.ReportAssignmentActivity;
import com.teachersapp.ReportStudentChartActivity;
import com.teachersapp.bean.AttendStudent;
import com.teachersapp.bean.PlannerPerformance;
import com.teachersapp.listner.PlannerPerformanceOnClick;
import com.teachersapp.listner.StudentOnClick;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class PerformanceAssignAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<PlannerPerformance> performanceList;
    PlannerPerformanceOnClick performanceOnClick;

    public PerformanceAssignAdapter(ReportStudentChartActivity context, List<PlannerPerformance> performanceList) {
        this.context = context;
        this.performanceList = performanceList;
        performanceOnClick = (PlannerPerformanceOnClick) context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_student_report_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        PlannerPerformance performance = performanceList.get(position);
//        holder.mContainer.setBackground(null);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(0, 0, 0, 10);
        holder.mContainer.setLayoutParams(params);

        holder.mStudentName.setText(performance.getPlannerName());
        holder.mPercentage.setText("" + performance.getPerformance() + "%");
        holder.mProgrees.setProgress((int) performance.getPerformance());

        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performanceOnClick.callChangeDate(position, performance);
            }
        });

        holder.mMoreData.setVisibility(View.VISIBLE);

        holder.mMoreData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performanceOnClick.callChangeDate(position, performance);
            }
        });

        /*if (position == 0) {
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.top_cornor_round));
        } else*/
        if (position == performanceList.size() - 1) {
//            holder.mLine.setVisibility(View.GONE);
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_cornor_round));
        } else {
//            holder.mLine.setVisibility(View.VISIBLE);
            holder.mContainer.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));
        }

//        holder.mStudentName.setOnClickListener(v -> studentOnClick.callbackStudent(position, performance));

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return performanceList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mMoreData)
        ImageView mMoreData;
        @BindView(R.id.mStudentName)
        TextView mStudentName;
        @BindView(R.id.mPercentage)
        TextView mPercentage;
        @BindView(R.id.mProgrees)
        ProgressBar mProgrees;
        @BindView(R.id.mContainer)
        LinearLayout mContainer;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }


}
