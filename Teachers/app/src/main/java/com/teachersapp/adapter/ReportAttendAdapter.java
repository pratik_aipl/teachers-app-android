package com.teachersapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.ReportStudentActivity;
import com.teachersapp.bean.AttendStudent;
import com.teachersapp.listner.StudentOnClick;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class ReportAttendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<AttendStudent> attendStudentList;
    StudentOnClick studentOnClick;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ReportAttendAdapter(ReportStudentActivity context, List<AttendStudent> attendStudentList) {
        this.context = context;
        this.attendStudentList = attendStudentList;
        this.studentOnClick = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_report_attend_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        AttendStudent attendStudent = attendStudentList.get(position);

//        holder.mStudentName.setText(attendStudent.getStudentName());
        holder.mStudentName.setText(this.context.getString(R.string.namewithbatch, attendStudent.getStudentName(), attendStudent.getBatchName()));
        holder.mCorrectAns1.setText(""+attendStudent.getTotalRight());
        holder.mInCorrectAns.setText(""+attendStudent.getTotalWrong());
        holder.mNotAns.setText(""+(attendStudent.getTotalQuestion() - attendStudent.getTotalAttempt()));
        holder.mTotalQuestion1.setText(""+attendStudent.getTotalQuestion());
        holder.mTotalQuestion2.setText(""+attendStudent.getTotalQuestion());
        holder.mTotalQuestion3.setText(""+attendStudent.getTotalQuestion());

        holder.mStudentName.setOnClickListener(v -> studentOnClick.callbackStudent(position,attendStudent));

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return attendStudentList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mStudentName)
        TextView mStudentName;
        @BindView(R.id.mCorrectAns1)
        TextView mCorrectAns1;
        @BindView(R.id.mTotalQuestion1)
        TextView mTotalQuestion1;
        @BindView(R.id.mInCorrectAns)
        TextView mInCorrectAns;
        @BindView(R.id.mTotalQuestion2)
        TextView mTotalQuestion2;
        @BindView(R.id.mNotAns)
        TextView mNotAns;
        @BindView(R.id.mTotalQuestion3)
        TextView mTotalQuestion3;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
