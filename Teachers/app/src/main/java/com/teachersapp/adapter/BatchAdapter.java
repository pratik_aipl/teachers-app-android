package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.ShareScreen;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.bean.ChapterBean;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.fragment.BottomSheetBatchDialog;
import com.teachersapp.listner.BatchOnClick;
import com.teachersapp.listner.PaperOnClick;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Prefs;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class BatchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<BatchBean> batchBeans;
    BatchOnClick batchOnClick;
    boolean isReport = false;
    Prefs prefs;

    // Provide a suitable constructor (depends on the kind of dataset)
    public BatchAdapter(ShareScreen context, List<BatchBean> paperBeans) {
        this.context = context;
        this.batchOnClick = context;
        this.batchBeans = paperBeans;
        prefs = Prefs.with(context);
    }

    public BatchAdapter(Context context, List<BatchBean> chapterBeanList, BottomSheetBatchDialog bottomSheetBatchDialog) {
        this.context = context;
        this.batchOnClick = bottomSheetBatchDialog;
        this.batchBeans = chapterBeanList;
        isReport = true;
        prefs = Prefs.with(context);

    }
    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.batch_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        BatchBean batchBean = batchBeans.get(position);
        holder.mBatchName.setText(batchBean.getBatchName());

        if (position == 0)
            holder.mPaperView.setBackground(ContextCompat.getDrawable(context, R.drawable.list_top_cornor_round));
        else if (position == batchBeans.size() - 1)
            holder.mPaperView.setBackground(ContextCompat.getDrawable(context, R.drawable.list_bottom_cornor_round));
        else
            holder.mPaperView.setBackground(ContextCompat.getDrawable(context, R.drawable.list_no_round));

        if (isReport) {
            if (batchBean.getBatchID() == prefs.getInt(Constant.selectedBranch, -1))
                holder.mRadioBtn.setChecked(true);
            else
                holder.mRadioBtn.setChecked(false);
        }

        holder.mBatchName.setOnClickListener(v -> {
            if (holder.mRadioBtn.isChecked())
                holder.mRadioBtn.setChecked(false);
            else
                holder.mRadioBtn.setChecked(true);
            batchBean.setSelectBranch(holder.mRadioBtn.isChecked());
            batchOnClick.callbackPaper(position, batchBean);
        });

        holder.mImageView.setOnClickListener(v -> {
            if (holder.mRadioBtn.isChecked())
                holder.mRadioBtn.setChecked(false);
            else
                holder.mRadioBtn.setChecked(true);
            batchBean.setSelectBranch(holder.mRadioBtn.isChecked());

            batchOnClick.callbackPaper(position, batchBean);
        });

//        if (position == batchBeans.size() - 1)
//            holder.mView.setVisibility(View.GONE);
//        else
//            holder.mView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return batchBeans.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mPaperView)
        LinearLayout mPaperView;

        @BindView(R.id.mBatchName)
        TextView mBatchName;
        @BindView(R.id.mImageView)
        ImageView mImageView;

        @BindView(R.id.mRadioBtn)
        RadioButton mRadioBtn;
        @BindView(R.id.mView)
        View mView;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
