package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.bean.Standard;

import java.util.List;

public class StandardAdapter extends BaseAdapter {

    Context context;
    List<Standard> standardList;

    public StandardAdapter(Context context, List<Standard> standardList) {
        this.standardList = standardList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return standardList.size();
    }

    @Override
    public Standard getItem(int position) {
        return standardList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Standard rowItem = getItem(position);
        view =  (LayoutInflater.from(context)).inflate(R.layout.raw_spinner_item, null);
        TextView names = view.findViewById(R.id.mText);
        if (position == 0) {
            names.setTextColor(ContextCompat.getColor(context, R.color.gray));
        } else {
            names.setTextColor(ContextCompat.getColor(context, R.color.black));
        }
        names.setText(rowItem.getStandard());
        return view;
    }
}