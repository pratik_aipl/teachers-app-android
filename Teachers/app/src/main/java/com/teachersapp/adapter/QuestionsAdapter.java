package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.ShareScreen;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.bean.LevelQuestion;
import com.teachersapp.fragment.BottomSheetBatchDialog;
import com.teachersapp.listner.BatchOnClick;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Prefs;
import com.teachersapp.view.QuestionsWebView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuestionsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<LevelQuestion> levelQuestions;

    public QuestionsAdapter(Context context, List<LevelQuestion> levelQuestions) {
        this.context = context;
        this.levelQuestions = levelQuestions;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_question_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        LevelQuestion levelQuestion = levelQuestions.get(position);

        holder.mQueNo.setText((position + 1) + ")");

        holder.mQuestionText.loadHtmlFromLocal(levelQuestion.getQuestion());
        holder.mQuestionText.setInitialScale(50);


    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return levelQuestions.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mQuestionText)
        QuestionsWebView mQuestionText;

        @BindView(R.id.mQueNo)
        TextView mQueNo;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

}
