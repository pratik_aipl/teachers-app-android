package com.teachersapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.bean.AttendStudent;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Peep on 04/12/18.
 */
public class ReportNotAttendAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    Context context;
    List<AttendStudent> attendStudentList;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ReportNotAttendAdapter(Context context,List<AttendStudent> attendStudentList) {
        this.context = context;
        this.attendStudentList = attendStudentList;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_report_not_attend_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        AttendStudent attendStudent = attendStudentList.get(position);
        holder.mStudentName.setText(this.context.getString(R.string.namewithbatch, attendStudent.getStudentName(), attendStudent.getBatchName()));
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return attendStudentList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mStudentName)
        TextView mStudentName;
        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }
}
