package com.teachersapp.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.teachersapp.R;
import com.teachersapp.bean.Subject;

import java.util.List;

public class SubjectAdapter extends BaseAdapter {

    Context context;
    List<Subject> subjectList;

    public SubjectAdapter(Context context, List<Subject> subjectList) {
        this.subjectList = subjectList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return subjectList.size();
    }

    @Override
    public Subject getItem(int position) {
        return subjectList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Subject rowItem = getItem(position);
        view = (LayoutInflater.from(context)).inflate(R.layout.raw_spinner_item, null);
        TextView names = view.findViewById(R.id.mText);
        if (position == 0) {
            names.setTextColor(ContextCompat.getColor(context, R.color.gray));
        } else {
            names.setTextColor(ContextCompat.getColor(context, R.color.black));
        }
        names.setText(rowItem.getSubjectName());
        return view;
    }
}