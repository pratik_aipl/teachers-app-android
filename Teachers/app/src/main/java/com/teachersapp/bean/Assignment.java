package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class Assignment implements Serializable {

    @JsonField
    int MCQPlannerID;
    @JsonField
    String PlannerName;
    @JsonField
    int TotalQuestion;
    @JsonField
    String SubjectName;
    @JsonField
    String StandardName;
    @JsonField
    String MediumName;
    @JsonField
    String BoardName;
    @JsonField
    String StartDate;
    @JsonField
    String EndDate;
    @JsonField
    String Time;
    @JsonField
    String Type;
    @JsonField
    int VideoID;
    @JsonField
    long StartTime;
    @JsonField
    long EndTime;

    public int getMCQPlannerID() {
        return MCQPlannerID;
    }

    public void setMCQPlannerID(int MCQPlannerID) {
        this.MCQPlannerID = MCQPlannerID;
    }

    public String getPlannerName() {
        return PlannerName;
    }

    public void setPlannerName(String plannerName) {
        PlannerName = plannerName;
    }

    public int getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public int getVideoID() {
        return VideoID;
    }

    public void setVideoID(int videoID) {
        VideoID = videoID;
    }

    public long getStartTime() {
        return StartTime;
    }

    public void setStartTime(long startTime) {
        StartTime = startTime;
    }

    public long getEndTime() {
        return EndTime;
    }

    public void setEndTime(long endTime) {
        EndTime = endTime;
    }
}
