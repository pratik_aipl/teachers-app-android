package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class StudentDetailsBean implements Serializable {

    @JsonField(name = "test_details")
    TestDetails testDetails;

    @JsonField(name = "appeared_questions")
    List<LevelQuestion> appearedQuestions;

    @JsonField(name = "not_appeared_questions")
    List<LevelQuestion> notAppearedQuestions;


    public TestDetails getTestDetails() {
        return testDetails;
    }

    public void setTestDetails(TestDetails testDetails) {
        this.testDetails = testDetails;
    }

    public List<LevelQuestion> getAppearedQuestions() {
        return appearedQuestions;
    }

    public void setAppearedQuestions(List<LevelQuestion> appearedQuestions) {
        this.appearedQuestions = appearedQuestions;
    }

    public List<LevelQuestion> getNotAppearedQuestions() {
        return notAppearedQuestions;
    }

    public void setNotAppearedQuestions(List<LevelQuestion> notAppearedQuestions) {
        this.notAppearedQuestions = notAppearedQuestions;
    }
}
