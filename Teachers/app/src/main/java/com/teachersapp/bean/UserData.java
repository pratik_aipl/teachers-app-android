package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.List;

@JsonObject
public class UserData {
    @JsonField(name = "id")
    String id;

    @JsonField(name = "firstname")
    String firstName;

    @JsonField(name = "lastname")
    String lastName;

    @JsonField(name = "username")
    String userName;

    @JsonField(name = "email")
    String email;

    @JsonField(name = "mobileno")
    String mobileNo;

    @JsonField(name = "branch")
    List<BranchBean> branchList;

    @JsonField(name = "batch")
    List<BatchBean> batchBeanList;

    public List<BranchBean> getBranchList() {
        return branchList;
    }

    public void setBranchList(List<BranchBean> branchList) {
        this.branchList = branchList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public List<BatchBean> getBatchBeanList() {
        return batchBeanList;
    }

    public void setBatchBeanList(List<BatchBean> batchBeanList) {
        this.batchBeanList = batchBeanList;
    }
}

