package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class LevelQuestion implements Serializable {

    @JsonField
    int LevelQuestion;
    @JsonField
    int MCQQuestionID;
    @JsonField
    int AnswerID;
    @JsonField
    int IsCorrect=-1;
    @JsonField
    int Correct;
    @JsonField
    int InCorrect =-1;
    @JsonField
    int NotAttempt;
    @JsonField
    String Question;
    @JsonField(name = "Options")
    List<OptionBean> Options;

    @JsonField(name = "QuestionMarkers")
    long QuestionMarkers;

    public int getMCQQuestionID() {
        return MCQQuestionID;
    }

    public void setMCQQuestionID(int MCQQuestionID) {
        this.MCQQuestionID = MCQQuestionID;
    }

    public int getAnswerID() {
        return AnswerID;
    }

    public void setAnswerID(int answerID) {
        AnswerID = answerID;
    }

    public int getIsCorrect() {
        return IsCorrect;
    }

    public void setIsCorrect(int isCorrect) {
        IsCorrect = isCorrect;
    }

    public int getCorrect() {
        return Correct;
    }

    public void setCorrect(int correct) {
        Correct = correct;
    }

    public int getInCorrect() {
        return InCorrect;
    }

    public void setInCorrect(int inCorrect) {
        InCorrect = inCorrect;
    }

    public int getNotAttempt() {
        return NotAttempt;
    }

    public void setNotAttempt(int notAttempt) {
        NotAttempt = notAttempt;
    }

    public long getQuestionMarkers() {
        return QuestionMarkers;
    }

    public void setQuestionMarkers(long questionMarkers) {
        QuestionMarkers = questionMarkers;
    }

    public int getLevelQuestion() {
        return LevelQuestion;
    }

    public List<OptionBean> getOptions() {
        return Options;
    }

    public void setOptions(List<OptionBean> options) {
        Options = options;
    }

    public void setLevelQuestion(int levelQuestion) {
        LevelQuestion = levelQuestion;
    }

    public String getQuestion() {
        return Question;
    }

    public void setQuestion(String question) {
        Question = question;
    }
}

