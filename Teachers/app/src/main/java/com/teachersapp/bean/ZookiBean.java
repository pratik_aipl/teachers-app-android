package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

/**
 * Created by pratik on 12/22/2017.
 */
@JsonObject
public class ZookiBean implements Serializable {

    @JsonField(name = "ZookiID")
    int zookiId;
    @JsonField(name = "Title")
    String title;
    @JsonField(name = "Description")
    String description;
    @JsonField(name = "Image")
    String image;
    @JsonField(name = "CreatedDate")
    String createdDate;

    public int getZookiId() {
        return zookiId;
    }

    public void setZookiId(int zookiId) {
        this.zookiId = zookiId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
}
