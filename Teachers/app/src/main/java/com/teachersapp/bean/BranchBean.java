package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class BranchBean implements Serializable {

    @JsonField(name = "BranchName")
    String BranchName;

    @JsonField(name = "BranchID")
    int BranchID;

    @JsonField(name = "data")
    List<BatchBean> batchBeanList;

    @JsonField(name = "standard")
    List<Standard> standardList;



    public List<Standard> getStandardList() {
        return standardList;
    }

    public void setStandardList(List<Standard> standardList) {
        this.standardList = standardList;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public int getBranchID() {
        return BranchID;
    }

    public void setBranchID(int branchID) {
        BranchID = branchID;
    }

    public List<BatchBean> getBatchBeanList() {
        return batchBeanList;
    }

    public void setBatchBeanList(List<BatchBean> batchBeanList) {
        this.batchBeanList = batchBeanList;
    }
}
