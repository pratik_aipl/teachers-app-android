package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class ChapterBean implements Serializable {

    @JsonField(name = "ChapterID")
    int ChapterID;
    @JsonField(name = "ChapterName")
    String ChapterName;
    @JsonField(name = "sub_chapter")
    List<ChapterBean> subChapter;

    public int getChapterID() {
        return ChapterID;
    }

    public void setChapterID(int chapterID) {
        ChapterID = chapterID;
    }

    public String getChapterName() {
        return ChapterName;
    }

    public void setChapterName(String chapterName) {
        ChapterName = chapterName;
    }

    public List<ChapterBean> getSubChapter() {
        return subChapter;
    }

    public void setSubChapter(List<ChapterBean> subChapter) {
        this.subChapter = subChapter;
    }
}
