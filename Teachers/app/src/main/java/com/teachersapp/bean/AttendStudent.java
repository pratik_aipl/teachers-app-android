package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class AttendStudent implements Serializable {

    @JsonField(name = "StudentID")
    int StudentID;
    @JsonField(name = "StudentName")
    String StudentName;
    @JsonField(name = "BatchID")
    int BatchID;
    @JsonField(name = "StandardID")
    int StandardID;
    @JsonField(name = "TotalQuestion")
    int TotalQuestion;
    @JsonField(name = "TotalRight")
    int TotalRight;
    @JsonField(name = "TotalWrong")
    int TotalWrong;
    @JsonField(name = "TotalAttempt")
    int TotalAttempt;
    @JsonField(name = "Accuracy")
    int Accuracy;
    @JsonField(name = "StudentAccuracy")
    float StudentAccuracy;
    @JsonField(name = "BatchName")
    String BatchName;
    @JsonField(name = "BranchName")
    String BranchName;

    public String getBatchName() {
        return BatchName;
    }

    public void setBatchName(String batchName) {
        BatchName = batchName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public float getStudentAccuracy() {
        return StudentAccuracy;
    }

    public void setStudentAccuracy(float studentAccuracy) {
        StudentAccuracy = studentAccuracy;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        StudentID = studentID;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String studentName) {
        StudentName = studentName;
    }

    public int getBatchID() {
        return BatchID;
    }

    public void setBatchID(int batchID) {
        BatchID = batchID;
    }

    public int getStandardID() {
        return StandardID;
    }

    public void setStandardID(int standardID) {
        StandardID = standardID;
    }

    public int getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(int totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public int getTotalRight() {
        return TotalRight;
    }

    public void setTotalRight(int totalRight) {
        TotalRight = totalRight;
    }

    public int getTotalWrong() {
        return TotalWrong;
    }

    public void setTotalWrong(int totalWrong) {
        TotalWrong = totalWrong;
    }

    public int getTotalAttempt() {
        return TotalAttempt;
    }

    public void setTotalAttempt(int totalAttempt) {
        TotalAttempt = totalAttempt;
    }

    public int getAccuracy() {
        return Accuracy;
    }

    public void setAccuracy(int accuracy) {
        Accuracy = accuracy;
    }
}
