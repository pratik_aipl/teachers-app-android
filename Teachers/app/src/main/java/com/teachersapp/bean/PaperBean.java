package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class PaperBean implements Serializable {

    @JsonField(name = "MCQPlannerID")
    String MCQPlannerID;

    @JsonField(name = "TotalQuestion")
    String TotalQuestion;

    @JsonField(name = "SubjectName")
    String SubjectName;

    @JsonField(name = "StandardName")
    String StandardName;

    @JsonField(name = "MediumName")
    String MediumName;

    @JsonField(name = "BoardName")
    String BoardName;

    @JsonField(name = "BranchName")
    String BranchName;

    @JsonField(name = "BranchID")
    int BranchID;

    @JsonField(name = "avilable_batch")
    int availableBatch;

    @JsonField(name = "StartDate")
    String StartDate;

    @JsonField(name = "EndDate")
    String EndDate;

    @JsonField(name = "PublishDate")
    String PublishDate;

    @JsonField(name = "PlannerName")
    String PlannerName;

    @JsonField(name = "total_completed_students")
    int total_completed_students;
    @JsonField(name = "pending_students")
    int pending_students;

    @JsonField(name = "chapter")
    List<ChapterBean> chapter;

    @JsonField(name = "level")
    List<LevelBean> level;

    @JsonField(name = "published_batch")
    List<PublishedBatchBean> publishedBatch;


    @JsonField(name = "Type")
    String Type="0";
    @JsonField(name = "VideoID")
    String VideoID;
    @JsonField(name = "StartTime")
    long StartTime;
    @JsonField(name = "EndTime")
    long EndTime;
    @JsonField(name = "VideoName")
    String VideoName;
    @JsonField(name = "VideoImage")
    String VideoImage;
    @JsonField(name = "VideoURL")
    String VideoURL;

    public int getTotal_completed_students() {
        return total_completed_students;
    }

    public void setTotal_completed_students(int total_completed_students) {
        this.total_completed_students = total_completed_students;
    }

    public int getPending_students() {
        return pending_students;
    }

    public void setPending_students(int pending_students) {
        this.pending_students = pending_students;
    }

    public String getPublishDate() {
        return PublishDate;
    }

    public void setPublishDate(String publishDate) {
        PublishDate = publishDate;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getVideoID() {
        return VideoID;
    }

    public void setVideoID(String videoID) {
        VideoID = videoID;
    }

    public long getStartTime() {
        return StartTime;
    }

    public void setStartTime(long startTime) {
        StartTime = startTime;
    }

    public long getEndTime() {
        return EndTime;
    }

    public void setEndTime(long endTime) {
        EndTime = endTime;
    }

    public String getVideoName() {
        return VideoName;
    }

    public void setVideoName(String videoName) {
        VideoName = videoName;
    }

    public String getVideoImage() {
        return VideoImage;
    }

    public void setVideoImage(String videoImage) {
        VideoImage = videoImage;
    }

    public String getVideoURL() {
        return VideoURL;
    }

    public void setVideoURL(String videoURL) {
        VideoURL = videoURL;
    }

    public int getAvailableBatch() {
        return availableBatch;
    }

    public void setAvailableBatch(int availableBatch) {
        this.availableBatch = availableBatch;
    }


    public List<LevelBean> getLevel() {
        return level;
    }

    public void setLevel(List<LevelBean> level) {
        this.level = level;
    }

    public List<PublishedBatchBean> getPublishedBatch() {
        return publishedBatch;
    }

    public void setPublishedBatch(List<PublishedBatchBean> publishedBatch) {
        this.publishedBatch = publishedBatch;
    }

//    public PaperBean() {
//    }

//    public PaperBean(PaperBean paperBean) {
//        MCQPlannerID = paperBean.getMCQPlannerID();
//        TotalQuestion = paperBean.getTotalQuestion();
//        SubjectName = paperBean.getSubjectName();
//        StandardName = paperBean.getStandardName();
//        MediumName = paperBean.getMediumName();
//        BoardName = paperBean.getBoardName();
//        BranchName = paperBean.getBranchName();
//        BranchID = paperBean.getBranchID();
//        this.availableBatch = paperBean.getAvailableBatch();
//        StartDate = paperBean.getStartDate();
//        EndDate = paperBean.getEndDate();
//        PlannerName =paperBean.getPlannerName();
//        this.chapter = paperBean.getChapter();
//        this.level = paperBean.getLevel();
//        this.publishedBatch = paperBean.getPublishedBatch();
//    }

    public String getPlannerName() {
        return PlannerName;
    }

    public void setPlannerName(String plannerName) {
        PlannerName = plannerName;
    }

    public List<ChapterBean> getChapter() {
        return chapter;
    }

    public void setChapter(List<ChapterBean> chapter) {
        this.chapter = chapter;
    }

    public int getBranchID() {
        return BranchID;
    }

    public void setBranchID(int branchID) {
        BranchID = branchID;
    }

    public String getMCQPlannerID() {
        return MCQPlannerID;
    }

    public void setMCQPlannerID(String MCQPlannerID) {
        this.MCQPlannerID = MCQPlannerID;
    }

    public String getTotalQuestion() {
        return TotalQuestion;
    }

    public void setTotalQuestion(String totalQuestion) {
        TotalQuestion = totalQuestion;
    }

    public String getSubjectName() {
        return SubjectName;
    }

    public void setSubjectName(String subjectName) {
        SubjectName = subjectName;
    }

    public String getStandardName() {
        return StandardName;
    }

    public void setStandardName(String standardName) {
        StandardName = standardName;
    }

    public String getMediumName() {
        return MediumName;
    }

    public void setMediumName(String mediumName) {
        MediumName = mediumName;
    }

    public String getBoardName() {
        return BoardName;
    }

    public void setBoardName(String boardName) {
        BoardName = boardName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }
}
