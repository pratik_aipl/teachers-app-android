package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class OptionBean implements Serializable {

    @JsonField(name = "MCQOptionID")
    int MCQOptionID;
    @JsonField(name = "MCQQuestionID")
    int MCQQuestionID;
    @JsonField(name = "Options")
    String Options;
    @JsonField(name = "OptionNo")
    String OptionNo;
    @JsonField(name = "IsCorrect")
    String IsCorrect;

    public int getMCQOptionID() {
        return MCQOptionID;
    }

    public void setMCQOptionID(int MCQOptionID) {
        this.MCQOptionID = MCQOptionID;
    }

    public int getMCQQuestionID() {
        return MCQQuestionID;
    }

    public void setMCQQuestionID(int MCQQuestionID) {
        this.MCQQuestionID = MCQQuestionID;
    }

    public String getOptions() {
        return Options;
    }

    public void setOptions(String options) {
        Options = options;
    }

    public String getOptionNo() {
        return OptionNo;
    }

    public void setOptionNo(String optionNo) {
        OptionNo = optionNo;
    }

    public String getIsCorrect() {
        return IsCorrect;
    }

    public void setIsCorrect(String isCorrect) {
        IsCorrect = isCorrect;
    }

    boolean isSelected;
    boolean isAttempt =false;

    public boolean isAttempt() {
        return isAttempt;
    }

    public void setAttempt(boolean attempt) {
        isAttempt = attempt;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
