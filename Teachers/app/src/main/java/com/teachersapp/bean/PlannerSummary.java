package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class PlannerSummary implements Serializable {

    @JsonField(name = "batch")
    List<BatchBean> batch;
    @JsonField(name = "accuracy")
    float accuracy;
    @JsonField(name = "attand_student")
    List<AttendStudent> attandStudent;
    @JsonField(name = "not_attand_student")
    List<AttendStudent> notAttandStudent;

    @JsonField(name = "level_accuracy")
    List<LevelBean> levelAccuracy;

    @JsonField(name = "Questions")
    List<LevelQuestion> Questions;
    @JsonField(name = "AvarageTime")
    long AvarageTime;
    @JsonField(name = "AvarageTakenTime")
    long AvarageTakenTime;

    public List<LevelQuestion> getQuestions() {
        return Questions;
    }

    public void setQuestions(List<LevelQuestion> questions) {
        Questions = questions;
    }

    public long getAvarageTime() {
        return AvarageTime;
    }

    public void setAvarageTime(long avarageTime) {
        AvarageTime = avarageTime;
    }

    public long getAvarageTakenTime() {
        return AvarageTakenTime;
    }

    public void setAvarageTakenTime(long avarageTakenTime) {
        AvarageTakenTime = avarageTakenTime;
    }

    public List<BatchBean> getBatch() {
        return batch;
    }

    public void setBatch(List<BatchBean> batch) {
        this.batch = batch;
    }

    public float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    public List<AttendStudent> getAttandStudent() {
        return attandStudent;
    }

    public void setAttandStudent(List<AttendStudent> attandStudent) {
        this.attandStudent = attandStudent;
    }

    public List<AttendStudent> getNotAttandStudent() {
        return notAttandStudent;
    }

    public void setNotAttandStudent(List<AttendStudent> notAttandStudent) {
        this.notAttandStudent = notAttandStudent;
    }

    public List<LevelBean> getLevelAccuracy() {
        return levelAccuracy;
    }

    public void setLevelAccuracy(List<LevelBean> levelAccuracy) {
        this.levelAccuracy = levelAccuracy;
    }
}
