package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class PlannerPerformance implements Serializable {

    @JsonField
    int MCQPlannerID;
    @JsonField
    int StudentMCQTestID;
    @JsonField
    String PlannerName;
    @JsonField
    double Performance;

    public int getStudentMCQTestID() {
        return StudentMCQTestID;
    }

    public void setStudentMCQTestID(int studentMCQTestID) {
        StudentMCQTestID = studentMCQTestID;
    }

    public int getMCQPlannerID() {
        return MCQPlannerID;
    }

    public void setMCQPlannerID(int MCQPlannerID) {
        this.MCQPlannerID = MCQPlannerID;
    }

    public String getPlannerName() {
        return PlannerName;
    }

    public void setPlannerName(String plannerName) {
        PlannerName = plannerName;
    }

    public double getPerformance() {
        return Performance;
    }

    public void setPerformance(double performance) {
        Performance = performance;
    }
}
