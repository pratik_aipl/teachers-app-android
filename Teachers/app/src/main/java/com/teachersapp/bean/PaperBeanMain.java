package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class PaperBeanMain implements Serializable {

    @JsonField(name = "paper_info")
    List<PaperBean> paperInfo;

    @JsonField(name = "dates")
    List<PaperDates> dates;

    public List<PaperBean> getPaperInfo() {
        return paperInfo;
    }

    public void setPaperInfo(List<PaperBean> paperInfo) {
        this.paperInfo = paperInfo;
    }

    public List<PaperDates> getDates() {
        return dates;
    }

    public void setDates(List<PaperDates> dates) {
        this.dates = dates;
    }
}
