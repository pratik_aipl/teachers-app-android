package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class Standard implements Serializable {

    @JsonField(name = "StandardName")
    String standard;
    @JsonField(name = "StandardID")
    int standardID;

    @JsonField(name = "Subject")
    List<Subject> subjectList;

    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public List<Subject> getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(List<Subject> subjectList) {
        this.subjectList = subjectList;
    }

    public int getStandardID() {
        return standardID;
    }

    public void setStandardID(int standardID) {
        this.standardID = standardID;
    }
}
