package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class LevelBean implements Serializable {
    @JsonField
    int LevelID;
    @JsonField
    String LevelName;
    @JsonField
    int NoOfQuestions;
    @JsonField
    int TotalCorrect;
    @JsonField
    int TotalStudents;
    @JsonField
    int TotalAvarage;
    @JsonField(name = "level_question")
    List<LevelQuestion> levelQuestionList;

    public int getTotalCorrect() {
        return TotalCorrect;
    }

    public void setTotalCorrect(int totalCorrect) {
        TotalCorrect = totalCorrect;
    }

    public int getTotalStudents() {
        return TotalStudents;
    }

    public void setTotalStudents(int totalStudents) {
        TotalStudents = totalStudents;
    }

    public int getTotalAvarage() {
        return TotalAvarage;
    }

    public void setTotalAvarage(int totalAvarage) {
        TotalAvarage = totalAvarage;
    }

    public List<LevelQuestion> getLevelQuestionList() {
        return levelQuestionList;
    }

    public void setLevelQuestionList(List<LevelQuestion> levelQuestionList) {
        this.levelQuestionList = levelQuestionList;
    }

    public int getLevelID() {
        return LevelID;
    }

    public void setLevelID(int levelID) {
        LevelID = levelID;
    }

    public String getLevelName() {
        return LevelName;
    }

    public void setLevelName(String levelName) {
        LevelName = levelName;
    }

    public int getNoOfQuestions() {
        return NoOfQuestions;
    }

    public void setNoOfQuestions(int noOfQuestions) {
        NoOfQuestions = noOfQuestions;
    }
}
