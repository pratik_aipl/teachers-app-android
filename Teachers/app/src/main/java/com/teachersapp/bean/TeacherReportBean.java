package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class TeacherReportBean implements Serializable {


    @JsonField(name = "TotalAccuracy")
    String totalAccuracy;

    @JsonField(name = "AssignmentPending")
    List<Assignment> pendingAssignmentList;
    @JsonField(name = "AssignmentFuture")
    List<Assignment> futureAssignmentList;
    @JsonField(name = "AssignmentMissed")
    List<Assignment> missedAssignmentList;
    @JsonField(name = "StudentInfo")
    List<AttendStudent> studentInfo;

    public String getTotalAccuracy() {
        return totalAccuracy;
    }

    public void setTotalAccuracy(String totalAccuracy) {
        this.totalAccuracy = totalAccuracy;
    }

    public List<Assignment> getPendingAssignmentList() {
        return pendingAssignmentList;
    }

    public void setPendingAssignmentList(List<Assignment> pendingAssignmentList) {
        this.pendingAssignmentList = pendingAssignmentList;
    }

    public List<Assignment> getFutureAssignmentList() {
        return futureAssignmentList;
    }

    public void setFutureAssignmentList(List<Assignment> futureAssignmentList) {
        this.futureAssignmentList = futureAssignmentList;
    }

    public List<Assignment> getMissedAssignmentList() {
        return missedAssignmentList;
    }

    public void setMissedAssignmentList(List<Assignment> missedAssignmentList) {
        this.missedAssignmentList = missedAssignmentList;
    }

    public List<AttendStudent> getStudentInfo() {
        return studentInfo;
    }

    public void setStudentInfo(List<AttendStudent> studentInfo) {
        this.studentInfo = studentInfo;
    }
}
