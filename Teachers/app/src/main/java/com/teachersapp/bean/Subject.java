package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class Subject implements Serializable {
    /*
      "SubjectID": "1",
                    "standardID": "10",
                    "SubjectName": "English",
                    "SubjectOrder": "1"
     */
    @JsonField(name = "SubjectID")
    int subjectID;
    @JsonField(name = "standardID")
    int standardID;
    @JsonField(name = "SubjectOrder")
    int subjectOrder;
    @JsonField(name = "SubjectName")
    String subjectName;

    public int getSubjectID() {
        return subjectID;
    }

    public void setSubjectID(int subjectID) {
        this.subjectID = subjectID;
    }

    public int getStandardID() {
        return standardID;
    }

    public void setStandardID(int standardID) {
        this.standardID = standardID;
    }

    public int getSubjectOrder() {
        return subjectOrder;
    }

    public void setSubjectOrder(int subjectOrder) {
        this.subjectOrder = subjectOrder;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }
}
