package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class BatchBean implements Serializable {

    @JsonField(name = "BatchID")
    int BatchID;

    @JsonField(name = "Batch")
    String BatchName;

    @JsonField(name = "BatchName")
    String Batch;

    @JsonField(name = "accuracy")
    String accuracy;

    @JsonField(name = "level_accuracy")
    List<LevelBean> levelAccuracy;


    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    public List<LevelBean> getLevelAccuracy() {
        return levelAccuracy;
    }

    public void setLevelAccuracy(List<LevelBean> levelAccuracy) {
        this.levelAccuracy = levelAccuracy;
    }

    boolean isSelectBranch = false;

    public int getBatchID() {
        return BatchID;
    }

    public void setBatchID(int batchID) {
        BatchID = batchID;
    }

    public String getBatchName() {
        return BatchName;
    }

    public String getBatch() {
        return Batch;
    }

    public void setBatch(String batch) {
        Batch = batch;
    }

    public void setBatchName(String batchName) {
        BatchName = batchName;
    }

    public boolean isSelectBranch() {
        return isSelectBranch;
    }

    public void setSelectBranch(boolean selectBranch) {
        isSelectBranch = selectBranch;
    }
}
