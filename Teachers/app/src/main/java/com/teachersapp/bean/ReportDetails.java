package com.teachersapp.bean;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;
import java.util.List;

@JsonObject
public class ReportDetails implements Serializable {
    @JsonField
    String Performance;
    @JsonField(name = "SubjectPerformance")
    List<SubjectPerformance> subjectPerformance;
    @JsonField(name = "PlannerPerformance")
    List<PlannerPerformance> plannerPerformance;

    public String getPerformance() {
        return Performance;
    }

    public void setPerformance(String performance) {
        Performance = performance;
    }

    public List<SubjectPerformance> getSubjectPerformance() {
        return subjectPerformance;
    }

    public void setSubjectPerformance(List<SubjectPerformance> subjectPerformance) {
        this.subjectPerformance = subjectPerformance;
    }

    public List<PlannerPerformance> getPlannerPerformance() {
        return plannerPerformance;
    }

    public void setPlannerPerformance(List<PlannerPerformance> plannerPerformance) {
        this.plannerPerformance = plannerPerformance;
    }
}
