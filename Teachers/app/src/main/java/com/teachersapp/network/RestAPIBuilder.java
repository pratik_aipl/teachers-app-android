package com.teachersapp.network;


import android.os.Build;
import android.util.Log;

import com.google.android.gms.security.ProviderInstaller;
import com.teachersapp.Apps;
import com.teachersapp.BuildConfig;
import com.teachersapp.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import okhttp3.ConnectionSpec;
import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.TlsVersion;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

/**
 * Created by MoDDA on 03/12/16
 */

public class RestAPIBuilder {

    private static final String TAG = "RestAPIBuilder";

    public static RestApi buildRetrofitService() {
        OkHttpClient okHttpClient = getNewHttpClient();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BuildConfig.API_URL)
                .client(okHttpClient).addConverterFactory(new ToStringConverterFactory())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create()).build();

        return retrofit.create(RestApi.class);
    }


    private static OkHttpClient getNewHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .cache(null)
                .connectTimeout(2, TimeUnit.MINUTES)
                .writeTimeout(2, TimeUnit.MINUTES)
                .readTimeout(2, TimeUnit.MINUTES);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }

        builder.addInterceptor(chain -> {
            Request request;
            String versionName = BuildConfig.VERSION_NAME;
            if (Utils.isLogin(Apps.mContext)) {
                Log.d(TAG, "buildRetrofitService: USER-ID " + Utils.getUser(Apps.mContext).getId());
                Log.d(TAG, "buildRetrofitService: LOGIN-TOKEN " +  Utils.getAuthtoken(Apps.mContext));
                request = chain.request().newBuilder()
                        .addHeader("Authorization", Credentials.basic(BuildConfig.userId, BuildConfig.password))
                        .addHeader("X-CI-CHILDRENS-ACADEMY-API-KEY", BuildConfig.apiKry)
                        .addHeader("X-CI-CHILDRENS-ACADEMY-LOGIN-TOKEN", Utils.getAuthtoken(Apps.mContext))
                        .addHeader("USER-ID", Utils.getUser(Apps.mContext).getId())
                        .build();
            } else {
                request = chain.request().newBuilder()
                        .addHeader("Authorization", Credentials.basic(BuildConfig.userId, BuildConfig.password))
                        .addHeader("X-CI-CHILDRENS-ACADEMY-API-KEY", BuildConfig.apiKry)
                        .build();
            }

            return chain.proceed(request);
        });
        return enableTls12OnPreLollipop(builder).build();
    }

    public static OkHttpClient.Builder enableTls12OnPreLollipop(OkHttpClient.Builder client) {
        if (Build.VERSION.SDK_INT >= 16 && Build.VERSION.SDK_INT < 22) {
            try {
                SSLContext sc = SSLContext.getInstance("TLSv1.2");
                sc.init(null, null, null);
                client.sslSocketFactory(new Tls12SocketFactory(sc.getSocketFactory()));
                ConnectionSpec cs = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS).tlsVersions(TlsVersion.TLS_1_2).build();
                List<ConnectionSpec> specs = new ArrayList<>();
                specs.add(cs);
                specs.add(ConnectionSpec.COMPATIBLE_TLS);
                specs.add(ConnectionSpec.CLEARTEXT);
                client.connectionSpecs(specs);
            } catch (Exception exc) {
                Log.e("OkHttpTLSCompat", "Error while setting TLS 1.2", exc);
            }
        }

        return client;
    }

}
