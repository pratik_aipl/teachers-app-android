package com.teachersapp.network;

import android.support.annotation.NonNull;

import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;
import rx.Observable;

public interface RestApi {

    @FormUrlEncoded
    @POST("app_version")
    Observable<Response<String>> checkUpdate(@FieldMap Map<String, String> stringMap);


    @FormUrlEncoded
    @POST("teacher/login")
    Observable<Response<String>> getLogin(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("logout")
    Observable<Response<String>> logOut(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("teacher/login/resendotp")
    Observable<Response<String>> reSendOTP(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("teacher/login/confirmotp")
    Observable<Response<String>> confirmOTP(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("querymessage")
    Observable<Response<String>> sendFeedBack(@FieldMap Map<String, String> stringMap);


    @FormUrlEncoded
    @POST("teacher/paperlist")
    Observable<Response<String>> getPaperList(@FieldMap Map<String, String> map);

    @FormUrlEncoded
    @POST("teacher/paper_dates")
    Observable<Response<String>> getDates(@FieldMap Map<String, String> map);

    @GET("zooki")
    Observable<Response<String>> getZookiList(@QueryMap Map<String, String> map);

    @GET("lists/subject_list")
    Observable<Response<String>> getSubjectList(@QueryMap Map<String, String> map);

    @GET("legal/teacher")
    Observable<Response<String>> getLegal(@QueryMap Map<String, String> map);

    @GET("teacher/profile")
    Observable<Response<String>> getProfile(@QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST("lists/branch_batch")
    Observable<Response<String>> getBranch(@Field("MCQPlannerID") String batchId);

    @FormUrlEncoded
    @POST("teacher/publish_paper")
    Observable<Response<String>> publishPaper(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("teacher/planner_summary")
    Observable<Response<String>> plannerSummary(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("teacher/Student_summary")
    Observable<Response<String>> studentSummary(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("teacher/Batch_summary")
    Observable<Response<String>> batchSummary(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("teacher/teacher_report")
    Observable<Response<String>> teacherReport(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("teacher/AssignPaperlist")
    Observable<Response<String>> AssignPaperlist(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("teacher/Student_details")
    Observable<Response<String>> studentDetails(@FieldMap Map<String, String> stringMap);

    @FormUrlEncoded
    @POST("teacher/Planner_details")
    Observable<Response<String>> plannerDetails(@FieldMap Map<String, String> stringMap);


    //    @Multipart
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @HTTP(method = "DELETE", path = "peep/", hasBody = true)
    Observable<Response<String>> peepDelete(@Body RequestBody object);

    String MULTIPART_FORM_DATA = "multipart/form-data";

    static RequestBody createRequestBody(@NonNull String s) {
        return RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), s);
    }

}
