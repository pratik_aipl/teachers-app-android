package com.teachersapp;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.teachersapp.adapter.PaperAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.bean.Standard;
import com.teachersapp.fragment.AllAssignmentFilterDialog;
import com.teachersapp.fragment.BottomSheetBatchFilter2;
import com.teachersapp.listner.AllAssignmentFilter;
import com.teachersapp.listner.PaperOnClick;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class AllAssignmentActivity extends ActivityBase implements PaperOnClick {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mAceDes)
    ImageView mAceDes;

    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mAssignmentList)
    RecyclerView mAssignmentList;
    List<PaperBean> assignmentList = new ArrayList<>();
    private List<Standard> standardList = new ArrayList<>();

    PaperAdapter paperAdapter;
    boolean isChange = false;

    private static final String TAG = "ReportAssignment";

    Subscription subscriptionAllAssignment, subscriptionStandard;
    @BindView(R.id.mTotalAssignment)
    TextView mTotalAssignment;
    @BindView(R.id.mTotalStudent)
    TextView mTotalStudent;
    public AllAssignmentFilterDialog allAssignmentFilterDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_assignment);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        prefs.save(Constant.selectMonth, "");
        prefs.save(Constant.selectStandard, -1);
        prefs.save(Constant.selectedBatch, -1);

        mSearch.setVisibility(View.VISIBLE);
        mAceDes.setVisibility(View.VISIBLE);

        mPageTitle.setText(getIntent().getStringExtra(Constant.page));

        paperAdapter=new PaperAdapter(this, assignmentList, AllAssignmentActivity.this);
        mAssignmentList.setAdapter(paperAdapter);
        if (Utils.isNetworkAvailable(this, true)) {
//            Map<String, String> map = new HashMap<>();
            getStandardList(false);
            getAllAssignment(true);
        }


    }

    private void getAllAssignment(boolean isShow) {

        Map<String, String> map = new HashMap<>();
        if (!TextUtils.isEmpty(prefs.getString(Constant.selectMonth, "")))
            map.put(Constant.DateMonth, prefs.getString(Constant.selectMonth, ""));
        if (prefs.getInt(Constant.selectedBatch, -1) != -1)
            map.put(Constant.BatchID, String.valueOf(prefs.getInt(Constant.selectedBatch, -1)));
        if (prefs.getInt(Constant.selectStandard, -1) != -1)
            map.put(Constant.StandardID, String.valueOf(prefs.getInt(Constant.selectStandard, -1)));
        showProgress(isShow);
        subscriptionAllAssignment = NetworkRequest.performAsyncRequest(restApi.AssignPaperlist(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    Log.d(TAG, "All Assignment: " + jsonResponse);
                    int totalAssignment = 0;
//                    if (jsonResponse.has("total_assignment")) {
                        totalAssignment = jsonResponse.getJSONObject(Constant.data).getInt("total_assignment");
//                    }
                    mTotalAssignment.setText(getString(R.string.total_assignment, totalAssignment));
                    int total_students = 0;
//                    if (jsonResponse.has("total_students")) {
                        total_students = jsonResponse.getJSONObject(Constant.data).getInt("total_students");
//                    }
                    mTotalStudent.setText(getString(R.string.student, total_students));
                    assignmentList.clear();
                    assignmentList.addAll(LoganSquare.parseList(jsonResponse.getJSONObject(Constant.data).getJSONArray(Constant.paper_info).toString(), PaperBean.class));
                    paperAdapter.notifyDataSetChanged();
                    Log.d(TAG, "getAllAssignment: 000--->  " + assignmentList.size());
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void getStandardList(boolean isShow) {
        Map<String, String> map = new HashMap<>();
//        showProgress(isShow);
        Log.d(TAG, "getPaperByDate: " + map);
        subscriptionStandard = NetworkRequest.performAsyncRequest(restApi.getSubjectList(map), (data) -> {
//            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    standardList.clear();
                    Standard standard = new Standard();
                    standard.setStandard("Select Standard");
                    standard.setStandardID(-1);
                    standardList.add(standard);
                    standardList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), Standard.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                com.teachersapp.utils.Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @OnClick({R.id.mBackBtn, R.id.mAceDes, R.id.mSearch})
    public void onViewClicked(View v) {

        switch (v.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mAceDes:

                if (!isChange) {
                    isChange = true;
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
                    Collections.sort(assignmentList, (s1, s2) -> LocalDateTime.parse(s1.getStartDate(), formatter).
                            compareTo(LocalDateTime.parse(s2.getStartDate(), formatter)));

                } else {
                    isChange = false;
                    Collections.reverse(assignmentList);
                }
                paperAdapter.notifyDataSetChanged();
                break;
            case R.id.mSearch:
                Log.d(TAG, "onViewClicked: " + gson.toJson(user));
                allAssignmentFilterDialog = AllAssignmentFilterDialog.newInstance(standardList);
                allAssignmentFilterDialog.show(getSupportFragmentManager(), "Custom Bottom Sheet");

                break;

        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAllAssignmentFilter(AllAssignmentFilter batchFilterEvent) {

        getAllAssignment(true);

    }


    @Override
    protected void onDestroy() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);

        if (subscriptionAllAssignment != null && !subscriptionAllAssignment.isUnsubscribed()) {
            subscriptionAllAssignment.unsubscribe();
            subscriptionAllAssignment = null;
        }
        if (subscriptionStandard != null && !subscriptionStandard.isUnsubscribed()) {
            subscriptionStandard.unsubscribe();
            subscriptionStandard = null;
        }
        prefs.save(Constant.selectMonth, "");
        prefs.save(Constant.selectStandard, -1);
        prefs.save(Constant.selectedBatch, -1);
        super.onDestroy();
    }

    @Override
    public void callbackPaper(int pos, PaperBean paperBean) {
        startActivity(new Intent(this, PaperScreen.class).putExtra(Constant.paper, paperBean));
    }
}
