package com.teachersapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.teachersapp.adapter.QuestionPagerAdaptor;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.LevelBean;
import com.teachersapp.bean.LevelQuestion;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;
import com.teachersapp.view.xvideoplayer.MxMediaManager;
import com.teachersapp.view.xvideoplayer.MxUserAction;
import com.teachersapp.view.xvideoplayer.MxVideoPlayer;
import com.teachersapp.view.xvideoplayer.MxVideoPlayerWidget;
import com.teachersapp.view.xvideoplayer.PlayerPause;
import com.teachersapp.view.xvideoplayer.UpdatePlayerData;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.teachersapp.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_AUTO_COMPLETE;
import static com.teachersapp.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_ERROR;
import static com.teachersapp.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_NORMAL;
import static com.teachersapp.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_PAUSE;
import static com.teachersapp.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_PLAYING;
import static com.teachersapp.view.xvideoplayer.MxVideoPlayer.CURRENT_STATE_PLAYING_BUFFERING_START;
import static com.teachersapp.view.xvideoplayer.MxVideoPlayer.SCREEN_WINDOW_FULLSCREEN;
import static com.teachersapp.view.xvideoplayer.MxVideoPlayer.backPress;

public class VideoAssignment extends ActivityBase {

    private static final String TAG = "VideoAssignment";
    Unbinder unbinder;

    QuestionPagerAdaptor questionPagerAdaptor;
    List<LevelQuestion> questionsBeans = new ArrayList<>();

    PaperBean paperBean;
    LevelBean levelBean;

    boolean isAlertShow = false;

    int mCurrentScreen = CURRENT_STATE_NORMAL, mCurrentState = 0;
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.videoPlayerWidget)
    MxVideoPlayerWidget videoPlayerWidget;
    @BindView(R.id.mProgress)
    ProgressBar mProgress;
    @BindView(R.id.mVolume)
    ImageButton mVolume;
    @BindView(R.id.forward)
    ImageButton forward;
    @BindView(R.id.play_pause_video1)
    ImageButton playPauseVideo1;
    @BindView(R.id.rewind)
    ImageButton rewind;
    @BindView(R.id.mZoomBtn)
    ImageButton mZoomBtn;
    @BindView(R.id.play_pause_video)
    ImageButton playPauseVideo;
    @BindView(R.id.play_time)
    TextView playTime;
    @BindView(R.id.mDurations)
    TextView mDurations;
    @BindView(R.id.mPointerView)
    RelativeLayout mPointerView;
    @BindView(R.id.mProgressVideo)
    SeekBar mProgressVideo;
    @BindView(R.id.mPager)
    ViewPager mPager;
    @BindView(R.id.mPrev)
    ImageView mPrev;
    @BindView(R.id.mLabel)
    TextView mLabel;
    @BindView(R.id.mTimer)
    TextView mTimer;
    @BindView(R.id.mNext)
    ImageView mNext;
    @BindView(R.id.mShowScreen)
    RelativeLayout mShowScreen;
    @BindView(R.id.mActionView)
    LinearLayout mActionView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        ButterKnife.bind(this);
        unSubScribeEvent(true);
        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.paper);
        levelBean = (LevelBean) getIntent().getSerializableExtra(Constant.levelName);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mPageTitle.setText(TextUtils.isEmpty(levelBean.getLevelName()) ? "Level 0" : levelBean.getLevelName());

        mDurations.setText(Utils.stringForTime(paperBean.getEndTime() - paperBean.getStartTime()));
        mProgressVideo.setMax((int) (paperBean.getEndTime() - paperBean.getStartTime()));

        playTime.setText("00:00");
        videoPlayerWidget.startPlay(paperBean.getVideoURL().replace("https", "http"), paperBean.getStartTime(), MxVideoPlayer.SCREEN_LAYOUT_NORMAL, "");
        Utils.loadImageWithPicasso(this, paperBean.getVideoImage(), videoPlayerWidget.mThumbImageView, mProgress);
        setQuestionPage();
        questionsBeans.clear();
        questionsBeans.addAll(levelBean.getLevelQuestionList() != null ? levelBean.getLevelQuestionList() : new ArrayList<>());
        questionPagerAdaptor.notifyDataSetChanged();
        updateIndicatorTv();

        for (int i = 0; i < questionsBeans.size(); i++) {
            SeekBar seekBar = new SeekBar(this);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, 0);
            seekBar.setLayoutParams(layoutParams);
            seekBar.setThumb(ContextCompat.getDrawable(this, R.mipmap.pointer));
            seekBar.setProgressDrawable(ContextCompat.getDrawable(this, R.drawable.seek_trans_dra));
            seekBar.setMax((int) (paperBean.getEndTime() - paperBean.getStartTime()));
            seekBar.setProgress((int) (questionsBeans.get(i).getQuestionMarkers() - paperBean.getStartTime()));
            mPointerView.addView(seekBar);
        }

        if (mCurrentScreen == SCREEN_WINDOW_FULLSCREEN) {
            mZoomBtn.setImageResource(R.mipmap.ic_exitfullscreen);
        } else {
            mZoomBtn.setImageResource(R.mipmap.ic_fullscreen);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPlayerPauseEvent(PlayerPause event) {
        mCurrentScreen = event.getmCurrentScreen();
        mCurrentState = event.getState();
        Log.d(TAG, "onPlayerPauseEvent: " + event.getState());
        if (mCurrentState == CURRENT_STATE_NORMAL || mCurrentState == CURRENT_STATE_ERROR) {
            playPauseVideo.setImageResource(R.mipmap.ic_play);
        } else if (mCurrentState == CURRENT_STATE_PLAYING) {
            playPauseVideo.setImageResource(R.mipmap.ic_pause);
        } else if (mCurrentState == CURRENT_STATE_PLAYING_BUFFERING_START) {
            playPauseVideo.setImageResource(R.mipmap.ic_pause);
        } else if (mCurrentState == CURRENT_STATE_PAUSE) {
            playPauseVideo.setImageResource(R.mipmap.ic_play);
        } else if (mCurrentState == CURRENT_STATE_AUTO_COMPLETE) {
            playPauseVideo.setImageResource(R.mipmap.ic_play);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onUpdatePlayerDataEvent(UpdatePlayerData event) {

        playTime.setText(Utils.stringForTime((event.getCurrentTimeP() - paperBean.getStartTime())));
        mProgressVideo.setProgress((int) (event.getCurrentTimeP() - paperBean.getStartTime()));

        if (event.getCurrentTimeP() >= paperBean.getEndTime()) {
            playTime.setText("00:00");
            mProgressVideo.setProgress(0);
            videoPlayerWidget.obtainCache();
            videoPlayerWidget.onActionEvent(MxUserAction.ON_CLICK_PAUSE);
            MxMediaManager.getInstance().getPlayer().pause();
            MxMediaManager.getInstance().getPlayer().seekTo(paperBean.getStartTime());
            videoPlayerWidget.setUiPlayState(CURRENT_STATE_PAUSE);
        }
    }


    private void unSubScribeEvent(boolean isSubScribe) {
        if (isSubScribe && !EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        } else if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @OnClick({R.id.mBackBtn, R.id.play_pause_video, R.id.mPrev, R.id.mNext, R.id.mZoomBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.play_pause_video:

                if (mCurrentState == 0) {
                    videoPlayerWidget.mThumbImageView.performClick();
                    videoPlayerWidget.mThumbImageView.setVisibility(View.GONE);
                } else {
                    videoPlayerWidget.mPlayControllerButton.performClick();
                }
                break;
            case R.id.mZoomBtn:
                if (mCurrentScreen == SCREEN_WINDOW_FULLSCREEN) {
                    videoPlayerWidget.mFullscreenButton.performClick();
                } else {
                    backPress();
                }
                break;
            case R.id.mPrev:
                if (mPager.getCurrentItem() > 0) {
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                }
                break;
            case R.id.mNext:
                if (mPager.getCurrentItem() != mPager.getAdapter().getCount()) {
                    mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        MxMediaManager.getInstance().releaseMediaPlayer();
        finish();


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unSubScribeEvent(true);
    }

    private void setQuestionPage() {
        questionPagerAdaptor = new QuestionPagerAdaptor(getSupportFragmentManager(), questionsBeans, paperBean.getStartTime());

        mPager.setAdapter(questionPagerAdaptor);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    mPrev.setBackground(ContextCompat.getDrawable(VideoAssignment.this, R.drawable.circle));
                    mNext.setBackground(ContextCompat.getDrawable(VideoAssignment.this, R.drawable.circle_primary));
                } else if (position == (mPager.getAdapter().getCount() - 1)) {
                    mNext.setBackground(ContextCompat.getDrawable(VideoAssignment.this, R.drawable.circle));
                    mPrev.setBackground(ContextCompat.getDrawable(VideoAssignment.this, R.drawable.circle_primary));
                } else {
                    mPrev.setBackground(ContextCompat.getDrawable(VideoAssignment.this, R.drawable.circle_primary));
                    mNext.setBackground(ContextCompat.getDrawable(VideoAssignment.this, R.drawable.circle_primary));
                }
                updateIndicatorTv();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if (questionsBeans.size() != 0)
            mPager.setOffscreenPageLimit((questionsBeans.size() - 1));
        updateIndicatorTv();

    }


    private void updateIndicatorTv() {
        int totalNum = mPager.getAdapter().getCount();
        int currentItem = mPager.getCurrentItem() + 1;
        if (totalNum != 0) {
            mActionView.setVisibility(View.VISIBLE);
        } else {
            mActionView.setVisibility(View.GONE);
        }
        mLabel.setText(currentItem + " / " + totalNum);
    }

}
