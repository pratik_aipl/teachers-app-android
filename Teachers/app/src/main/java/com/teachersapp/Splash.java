package com.teachersapp;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;

import com.teachersapp.base.ActivityBase;
import com.teachersapp.base.NetworkStateReceiver;
import com.teachersapp.listner.DialogButtonListener;
import com.teachersapp.listner.InternetConnetionLostEvent;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class Splash extends ActivityBase implements NetworkStateReceiver.NetworkStateReceiverListener {

    private static final String TAG = "SplashScreen";
    Subscription subscriptionUpdate;
    @BindView(R.id.mView)
    LinearLayout mView;
    Snackbar snackbar;
    public NetworkStateReceiver networkStateReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
//        int SPLASH_TIME_OUT = 1500;
//        new Handler().postDelayed(() -> {
//
//        }, SPLASH_TIME_OUT);
    }

    private void callUpdate(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DeviceType, Constant.android);
        map.put(Constant.AppType, Constant.teacher);
        map.put(Constant.AppCode, String.valueOf(BuildConfig.VERSION_CODE));
        Log.d(TAG, "callLogin: " + map);
        showProgress(isShow);
        subscriptionUpdate = NetworkRequest.performAsyncRequest(restApi.checkUpdate(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if (BuildConfig.DEBUG  || jsonResponse.getBoolean(Constant.status)) {
                        if (prefs.getBoolean(Constant.isLogin, false)) {
                            Intent i = new Intent(this, DashBoard.class);
                            startActivity(i);
                        } else {
                            Intent i = new Intent(this, MobileScreen.class);
                            startActivity(i);
                        }
                        finish();
                    } else {
                        Utils.showTwoButtonDialog(this, getString(R.string.network_error_title), jsonResponse.getString(Constant.message), "Update", null, new DialogButtonListener() {
                            @Override
                            public void onPositiveButtonClicked() {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" +BuildConfig.APPLICATION_ID)));
                            }

                            @Override
                            public void onNegativButtonClicked() {
                            }
                        });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
//            showConnectionSnackBarUserLogin();
        });
    }

    private void callSnackBar() {
        snackbar = Snackbar
                .make(mView, "Please check internet connection", Snackbar.LENGTH_INDEFINITE)
                .setAction("Retry", view -> {
                    if (Utils.isNetworkAvailable(Splash.this, false))
                        callUpdate(true);
                    else
                        callSnackBar();
                });
        snackbar.show();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onInternetConnetionLostEvent(InternetConnetionLostEvent event) {
        callSnackBar();
    }

    @Override
    public void networkAvailable() {
        callUpdate(true);
    }

    @Override
    public void networkUnavailable() {
        callSnackBar();
    }

    @Override
    public void onDestroy() {
        if (subscriptionUpdate != null && !subscriptionUpdate.isUnsubscribed()) {
            subscriptionUpdate.unsubscribe();
            subscriptionUpdate = null;
        }
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

}

