package com.teachersapp.service;

public interface SmsListener
{
    void messageReceived(String messageText);
}