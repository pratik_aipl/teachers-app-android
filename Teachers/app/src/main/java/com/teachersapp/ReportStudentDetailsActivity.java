package com.teachersapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.teachersapp.adapter.StudentQuestionsAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.AttendStudent;
import com.teachersapp.bean.LevelQuestion;
import com.teachersapp.bean.StudentDetailsBean;
import com.teachersapp.bean.TestDetails;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;
import com.teachersapp.view.NonScrollRecyclerView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

import static com.teachersapp.utils.Utils.stringForTime;

public class ReportStudentDetailsActivity extends ActivityBase {

    private static final String TAG = "ReportStudentDetailsAct";

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mStudent)
    TextView mStudent;
    @BindView(R.id.mTimeTaken)
    TextView mTimeTaken;
    @BindView(R.id.mStudentView)
    LinearLayout mStudentView;
    @BindView(R.id.mSummaryView)
    RelativeLayout mSummaryView;
    @BindView(R.id.mStudentViewLine)
    View mStudentViewLine;
    @BindView(R.id.mCorrectAns1)
    TextView mCorrectAns1;
    @BindView(R.id.mInCorrectAns)
    TextView mInCorrectAns;
    @BindView(R.id.mNotAns)
    TextView mNotAns;
    @BindView(R.id.mQuestionsList)
    NonScrollRecyclerView mQuestionsList;
    String PlannerID;
    AttendStudent student;
    Subscription subscriptionStudentReports;
    double performance;

    StudentDetailsBean studentDetailsBean;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_student_2);
        ButterKnife.bind(this);
        PlannerID = getIntent().getStringExtra(Constant.PlannerID);

        if (getIntent().getBooleanExtra(Constant.isPlanner, false)) {
            student = (AttendStudent) getIntent().getSerializableExtra(Constant.attandStudentList);
            mPageTitle.setText(getIntent().getStringExtra(Constant.levelName));
            mSummaryView.setVisibility(View.GONE);
            mRightText.setVisibility(View.VISIBLE);
            performance=getIntent().getDoubleExtra(Constant.Performance, 0);
            mRightText.setText(performance + "%");
            if (Utils.isNetworkAvailable(this, true))
                getPlannerReports(true,getIntent().getIntExtra(Constant.studentMcqTestId, 0));


        } else {
            mSummaryView.setVisibility(View.VISIBLE);
            mRightText.setVisibility(View.GONE);
            student = (AttendStudent) getIntent().getSerializableExtra(Constant.StudentID);
            mPageTitle.setText("Student");
            mStudent.setText(student.getStudentName());
            if (Utils.isNetworkAvailable(this, true))
                getStudentReports(false, "");
        }

    }

    private void getPlannerReports(boolean isShow, int studentMcqTestId) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.PlannerID, PlannerID);
        map.put(Constant.StudentMCQTestID, ""+studentMcqTestId);

        showProgress(isShow);
        subscriptionStudentReports = NetworkRequest.performAsyncRequest(restApi.plannerDetails(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    List<LevelQuestion> questionList=LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(),LevelQuestion.class);
                    mQuestionsList.setAdapter(new StudentQuestionsAdapter(this, questionList, true,performance));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void getStudentReports(boolean isShow, String selectBatch) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.PlannerID, PlannerID);
        map.put(Constant.StudentID, String.valueOf(student.getStudentID()));

        showProgress(isShow);
        subscriptionStudentReports = NetworkRequest.performAsyncRequest(restApi.studentSummary(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    Log.d(TAG, "getStudentReports: " + jsonResponse);
                    studentDetailsBean = LoganSquare.parse(jsonResponse.get(Constant.data).toString(), StudentDetailsBean.class);

                    TestDetails testDetails = studentDetailsBean.getTestDetails();

                    mTimeTaken.setText(stringForTime(testDetails.getTakenTime()));

                    mCorrectAns1.setText(String.format(getString(R.string.summary), testDetails.getTotalRight(), testDetails.getTotalQuestion()));
                    mInCorrectAns.setText(String.format(getString(R.string.summary), testDetails.getTotalWrong(), testDetails.getTotalQuestion()));
                    mNotAns.setText(String.format(getString(R.string.summary), (testDetails.getTotalQuestion() - testDetails.getTotalAttempt()), testDetails.getTotalQuestion()));

                    List<LevelQuestion> qList = new ArrayList<>();
                    qList.addAll(studentDetailsBean.getAppearedQuestions());
                    qList.addAll(studentDetailsBean.getNotAppearedQuestions());

                    mQuestionsList.setAdapter(new StudentQuestionsAdapter(this, qList, false, performance));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }


    @OnClick({R.id.mBackBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
