package com.teachersapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.teachersapp.adapter.ReportPerformanceBatchAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ReportPerformanceActivity extends ActivityBase {

    private static final String TAG = "ReportPerformanceActivi";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mBatchPerformanceList)
    RecyclerView mBatchPerformanceList;
    PaperBean paperBean;
    List<BatchBean> batchBeanList = new ArrayList<>();

    Subscription subscriptionBatchPerformance;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_performance);
        ButterKnife.bind(this);
        mPageTitle.setText("Performance");
        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.paper);

        if (Utils.isNetworkAvailable(this, true))
            getBatchPerformanceReports(true, "");


    }

    private void getBatchPerformanceReports(boolean isShow, String selectBatch) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MCQPlannerID, String.valueOf(paperBean.getMCQPlannerID()));

        showProgress(isShow);
        subscriptionBatchPerformance = NetworkRequest.performAsyncRequest(restApi.batchSummary(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
//                    mErrorMessage.setText("");
                    JSONObject jsonResponse = new JSONObject(data.body());
                    Log.d(TAG, "getBatchPerformanceReports: " + jsonResponse);
                    mBatchPerformanceList.setAdapter(new ReportPerformanceBatchAdapter(ReportPerformanceActivity.this, LoganSquare.parseList(jsonResponse.get(Constant.data).toString(), BatchBean.class)));

                } catch (Exception e) {
                    e.printStackTrace();
//                    mProgress.setVisibility(View.GONE);
//                    mErrorMessage.setText(e.getLocalizedMessage());
                }
            } else {
//                mProgress.setVisibility(View.GONE);
//                mErrorMessage.setText(Utils.serviceStatusFalseProcess(this, data));
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
//            mProgress.setVisibility(View.GONE);
//            mErrorMessage.setText(e.getMessage());
        });
    }

    @OnClick(R.id.mBackBtn)
    public void onViewClicked() {
        onBackPressed();
    }
}
