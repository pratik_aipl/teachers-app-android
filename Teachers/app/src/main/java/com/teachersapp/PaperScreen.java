package com.teachersapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.teachersapp.adapter.LevelAdapter;
import com.teachersapp.adapter.PublishBatchAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.BatchBeanID;
import com.teachersapp.bean.LevelBean;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.bean.PublishedBatchBean;
import com.teachersapp.fragment.BottomSheetChapterDialog;
import com.teachersapp.fragment.BottomSheetQuestionsDialog;
import com.teachersapp.fragment.ReportFragment;
import com.teachersapp.listner.DialogButtonListener;
import com.teachersapp.listner.LevelOnClick;
import com.teachersapp.listner.PageReloadEvent;
import com.teachersapp.listner.PaperReloadEvent;
import com.teachersapp.listner.PublishDateOnClick;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class PaperScreen extends ActivityBase implements LevelOnClick, PublishDateOnClick, DatePickerDialog.OnDateSetListener {

    private static final String TAG = "PaperScreen";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mShareBtn)
    Button mShareBtn;
    @BindView(R.id.mReport)
    Button mReport;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;

    PaperBean paperBean;
    @BindView(R.id.mStandard)
    TextView mStandard;
    @BindView(R.id.mSubject)
    TextView mSubject;
    @BindView(R.id.mChapter)
    TextView mChapter;
    @BindView(R.id.mChapterClick)
    TextView mChapterClick;
    @BindView(R.id.mStartDate)
    TextView mStartDate;
    @BindView(R.id.mEndDate)
    TextView mEndDate;
    @BindView(R.id.mReportLevel)
    RecyclerView mReportLevel;
    @BindView(R.id.mPublishBatchList)
    RecyclerView mPublishBatchList;


    public BottomSheetChapterDialog bottomSheetChapterDialog;
    public BottomSheetQuestionsDialog bottomSheetQuestionsDialog;
    Subscription  subscriptionPublish;


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPaperReloadEventEvent(PaperReloadEvent event) {
        setData(event.getPaperBean());
    }
    List<BatchBeanID> batchBeanIDS = new ArrayList<>();
    private void setData(PaperBean paperBean) {
        mPageTitle.setText(paperBean.getPlannerName());

        mStandard.setText(paperBean.getStandardName() + "(" + paperBean.getBoardName() + " - " + paperBean.getMediumName() + ")");
        mSubject.setText(paperBean.getSubjectName());
        mStartDate.setText(Utils.changeDateToDDMMYYYY(paperBean.getStartDate()));
        mEndDate.setText(Utils.changeDateToDDMMYYYY(paperBean.getEndDate()));
        mChapter.setText(paperBean.getChapter().size() + " Chapter Selected");


        LevelAdapter levelAdapter = new LevelAdapter(this, paperBean.getLevel(), true);
        mReportLevel.setAdapter(levelAdapter);

        PublishBatchAdapter standardAdapter = new PublishBatchAdapter(this, paperBean.getPublishedBatch());
        mPublishBatchList.setAdapter(standardAdapter);

        if (paperBean.getAvailableBatch() > 0) {
            mShareBtn.setVisibility(View.VISIBLE);
        } else {
            mShareBtn.setVisibility(View.GONE);
        }
        if (paperBean.getPublishedBatch().size() > 0) {
            mReport.setVisibility(View.VISIBLE);
        } else {
            mReport.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paper_screen);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.paper);
        setData(paperBean);

    }

    @OnClick({R.id.mBackBtn, R.id.mShareBtn, R.id.mReport, R.id.mChapterClick})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mShareBtn:
                startActivity(new Intent(this, ShareScreen.class).putExtra(Constant.paper, paperBean));
                break;

            case R.id.mReport:
                startActivity(new Intent(this, ReportActivity.class).putExtra(Constant.paper, paperBean));
                break;
            case R.id.mChapterClick:
                if (paperBean.getChapter().size() != 0) {
                    bottomSheetChapterDialog = BottomSheetChapterDialog.newInstance(paperBean.getChapter());
                    bottomSheetChapterDialog.show(getSupportFragmentManager(), "Custom Bottom Sheet");
                } else {
                    Toast.makeText(this, "Chapter not selected.", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void callbackLevel(int pos, LevelBean levelBean) {

        if (paperBean.getType() != null) {
            if (paperBean.getType().equalsIgnoreCase("0")) {
                startActivity(new Intent(this, QuestionsActivity.class)
                        .putExtra(Constant.levelName, levelBean.getLevelName())
                        .putExtra(Constant.paper, paperBean)
                        .putExtra(Constant.levelQuestionsList, (Serializable) levelBean.getLevelQuestionList()));
            } else {
                startActivity(new Intent(this, VideoAssignment.class)
                        .putExtra(Constant.paper, paperBean)
                        .putExtra(Constant.levelName, levelBean));
            }
        }else {
            Utils.showToast("Paper Type Missing.",PaperScreen.this);
        }
    }

    @Override
    public void callChangeDate(int pos, PublishedBatchBean batchBean) {
        batchBeanIDS.clear();
        batchBeanIDS.add(new BatchBeanID(batchBean.getBatchID()));
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date da = null;
        Calendar myCalendar = Calendar.getInstance();
        try {
            da = (Date) formatter.parse(batchBean.getPublishDate());
            myCalendar.setTime(da);
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        if (da.getTime()>Calendar.getInstance().getTimeInMillis()){
            DatePickerDialog dialog = new DatePickerDialog(this, this, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
            dialog.getDatePicker().setMinDate(System.currentTimeMillis() /*+ 86400000*/);
            dialog.show();
//        }else {
//
//        }
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String strDate = year + "/" + (month+1) + "/" + dayOfMonth;
        DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Date da = null;
        try {
            da = (Date) formatter.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("==Date is ==" + da);
        String strDateTime = formatter.format(da);
        Log.d(TAG, "strDateTime: " + strDateTime);
        Log.d(TAG, "onDateSet: " + strDate);
        publishPaper(strDateTime,true);
    }

    private void publishPaper(String date, boolean isShow) {
        String json = gson.toJson(batchBeanIDS);
        Map<String, String> map = new HashMap<>();
        map.put(Constant.PublishDate, date);
        map.put(Constant.MCQPlannerID, paperBean.getMCQPlannerID());
        map.put(Constant.BatchID, json);

        Log.d(TAG, "publishPaper: "+map.toString());
        showProgress(isShow);
        subscriptionPublish = NetworkRequest.performAsyncRequest(restApi.publishPaper(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    List<PaperBean> paperBeanList = LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), PaperBean.class);
                    Log.d(TAG, "publishPaper: "+gson.toJson(paperBeanList));
                    paperBean=paperBeanList.get(0);
                    setData(paperBean);
                    EventBus.getDefault().post(new PaperReloadEvent(paperBean));
//                    Utils.showTwoButtonDialog(this, "", jsonResponse.getString("message"), "OK", null, new DialogButtonListener() {
//                        @Override
//                        public void onPositiveButtonClicked() {
//                            onBackPressed();
//                        }
//
//                        @Override
//                        public void onNegativButtonClicked() {
//
//                        }
//                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    @Override
    protected void onDestroy() {

        if (subscriptionPublish != null && !subscriptionPublish.isUnsubscribed()) {
            subscriptionPublish.unsubscribe();
            subscriptionPublish = null;
        }
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }


}

