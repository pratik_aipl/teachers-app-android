package com.teachersapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.teachersapp.base.ActivityBase;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class Query extends ActivityBase {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mCall)
    ImageView mCall;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mFeedback)
    EditText mFeedback;
    @BindView(R.id.mSendBtn)
    Button mSendBtn;

    Subscription subscriptionFeedBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_query);
        ButterKnife.bind(this);

        mPageTitle.setText("Query");
    }


    @OnClick({R.id.mBackBtn, R.id.mSendBtn, R.id.mCall})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mCall:
                String phone = "+917710012112";
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                startActivity(intent);

                break;

            case R.id.mSendBtn:
                if (TextUtils.isEmpty(mFeedback.getText().toString().trim())) {
                    mFeedback.setError("Please enter feedback");
                } else {
                    if (Utils.isNetworkAvailable(this, true))
                        callFeedBack(true);
                }
                break;
        }
    }

    private void callFeedBack(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.Message, mFeedback.getText().toString().trim());

        showProgress(isShow);
        subscriptionFeedBack = NetworkRequest.performAsyncRequest(restApi.sendFeedBack(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    android.support.v7.app.AlertDialog alertDialog = Utils.showOneButtonDialog(this, "", jsonResponse.getString(Constant.message));
                    alertDialog.show();
                    mFeedback.setText("");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    @Override
    protected void onDestroy() {
        if (subscriptionFeedBack != null && !subscriptionFeedBack.isUnsubscribed()) {
            subscriptionFeedBack.unsubscribe();
            subscriptionFeedBack = null;
        }
        super.onDestroy();
    }
}
