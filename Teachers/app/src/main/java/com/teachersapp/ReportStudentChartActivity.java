package com.teachersapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.teachersapp.adapter.PerformanceAssignAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.AttendStudent;
import com.teachersapp.bean.PlannerPerformance;
import com.teachersapp.bean.ReportDetails;
import com.teachersapp.bean.SubjectPerformance;
import com.teachersapp.listner.PlannerPerformanceOnClick;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;
import com.teachersapp.view.NonScrollRecyclerView;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ReportStudentChartActivity extends ActivityBase implements OnChartValueSelectedListener, PlannerPerformanceOnClick {

    private static final String TAG = "ReportStudentChartActiv";

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mAceDes)
    ImageView mAceDes;
    @BindView(R.id.mRightText)
    TextView mRightText;

    AttendStudent student;
    Subscription subscription;

    ReportDetails reportDetails;
    @BindView(R.id.mLevelProgress)
    ProgressBar mLevelProgress;
    @BindView(R.id.mPerformance)
    TextView mPerformance;
    @BindView(R.id.mSubjectName)
    TextView mSubjectName;
    @BindView(R.id.mPercentage)
    TextView mPercentage;
    @BindView(R.id.mProgrees)
    ProgressBar mProgrees;
    @BindView(R.id.barchart)
    HorizontalBarChart barchart;
    @BindView(R.id.mAssignmentList)
    NonScrollRecyclerView mAssignmentList;

    protected Typeface tfLight;
    ArrayList<String> xLabel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_report_chart);
        ButterKnife.bind(this);
        tfLight = Typeface.createFromAsset(getAssets(), "Sylfaen.ttf");
        student = (AttendStudent) getIntent().getSerializableExtra(Constant.attandStudentList);
        mPageTitle.setText(student.getStudentName());

        if (Utils.isNetworkAvailable(this, true))
            getStudentChartData(true);
    }

    private BarDataSet getDataSet() {

        ArrayList<BarEntry> yvalues = new ArrayList();
        for (int i = 0; i < reportDetails.getSubjectPerformance().size(); i++) {
            yvalues.add(new BarEntry(i, Float.parseFloat(reportDetails.getSubjectPerformance().get(i).getPerformance())));
        }
        BarDataSet dataset = new BarDataSet(yvalues, "Student");
        dataset.setDrawValues(true);
        //set bar colour
        dataset.setColors(R.color.colorPrimary);
        return dataset;
    }


    @OnClick(R.id.mBackBtn)
    public void onViewClicked() {
        onBackPressed();
    }


    private void getStudentChartData(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.StudentID, String.valueOf(student.getStudentID()));
        map.put(Constant.BatchID, String.valueOf(student.getBatchID()));
        showProgress(isShow);
        subscription = NetworkRequest.performAsyncRequest(restApi.studentDetails(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    reportDetails = LoganSquare.parse(jsonResponse.getJSONObject(Constant.data).toString(), ReportDetails.class);
                    mPerformance.setText(reportDetails.getPerformance() + "%");
                    mAssignmentList.setAdapter(new PerformanceAssignAdapter(this, reportDetails.getPlannerPerformance()));
//                    setData();

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void setData() {
        BarData data = new BarData(getDataSet());
        data.setValueFormatter(new PercentFormatter());

        barchart.setData(data);
        barchart.animateXY(1000, 1000);
        barchart.invalidate();
        barchart.setScaleEnabled(false);
        barchart.setTouchEnabled(false);
        barchart.setDrawValueAboveBar(true);

        // Hide grid lines
//        barchart.getAxisLeft().setEnabled(false);
        barchart.getAxisRight().setEnabled(true);
        // Hide graph description
        barchart.getDescription().setEnabled(false);
        // Hide graph legend
        barchart.getLegend().setEnabled(false);
        barchart.setExtraOffsets(85, 0, 0, 0);
        xLabel = new ArrayList<>();
        for (int i = 0; i < reportDetails.getSubjectPerformance().size(); i++) {
            xLabel.add(reportDetails.getSubjectPerformance().get(i).getSubjectName());
        }
        // Display labels for bars
        barchart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xLabel));
        // barchart.getAxisRight().setAxisMaximum(100);

        //when chart zoom it's not repeated
        XAxis xAxis = barchart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setGranularityEnabled(true);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setLabelCount(xLabel.size());

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void callChangeDate(int pos, PlannerPerformance plannerPerformance) {
//        startActivity(new Intent(this, QuestionsActivity.class)
//                .putExtra(Constant.levelName, levelBean.getLevelName())
//                .putExtra(Constant.paper, paperBean)
//                .putExtra(Constant.levelQuestionsList, (Serializable) levelBean.getLevelQuestionList()));

        startActivity(new Intent(this, ReportStudentDetailsActivity.class)
                .putExtra(Constant.attandStudentList,student)
                .putExtra(Constant.levelName,plannerPerformance.getPlannerName())
                .putExtra(Constant.Performance,plannerPerformance.getPerformance())
                .putExtra(Constant.PlannerID, ""+plannerPerformance.getMCQPlannerID())
                .putExtra(Constant.studentMcqTestId, plannerPerformance.getStudentMCQTestID())
                .putExtra(Constant.isPlanner, true));


    }
}