package com.teachersapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.teachersapp.adapter.AssignmentAdapter;
import com.teachersapp.adapter.ReportStudentAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.Assignment;
import com.teachersapp.bean.AttendStudent;
import com.teachersapp.fragment.BottomSheetBatchFilter2;
import com.teachersapp.listner.BatchFilterEvent;
import com.teachersapp.listner.StudentOnClick;
import com.teachersapp.utils.Constant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ReportAssignmentActivity extends ActivityBase implements StudentOnClick {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mAceDes)
    ImageView mAceDes;

    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mStudentViewLabel)
    TextView mStudentViewLabel;
    @BindView(R.id.mAssignmentList)
    RecyclerView mAssignmentList;
    @BindView(R.id.mContainer)
    LinearLayout mContainer;

    List<AttendStudent> attendStudents = new ArrayList<>();
    List<AttendStudent> attendStudentsDefault = new ArrayList<>();
    ReportStudentAdapter studentAdapter;
    public BottomSheetBatchFilter2 bottomSheetBatchFilter;
    boolean isChange = false;

    private static final String TAG = "ReportAssignment";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_assignment);
        ButterKnife.bind(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        Log.d(TAG, "onBatchFilterEvent: " + prefs.getInt(Constant.selectedBatch, -1));

        mPageTitle.setText(getIntent().getStringExtra(Constant.page));

        if (getIntent().getBooleanExtra(Constant.isAssignment, true)) {
            mStudentViewLabel.setVisibility(View.GONE);
            mContainer.setBackground(ContextCompat.getDrawable(this, R.drawable.top_cornor_round));
            mAssignmentList.setAdapter(new AssignmentAdapter(this, (List<Assignment>) getIntent().getSerializableExtra(Constant.assignment), true));
        } else {
            mStudentViewLabel.setVisibility(View.VISIBLE);
            mSearch.setVisibility(View.VISIBLE);
            mAceDes.setVisibility(View.VISIBLE);
            mContainer.setBackground(null);
            try {
                attendStudentsDefault.addAll(LoganSquare.parseList(this.prefs.getString(Constant.data, ""), AttendStudent.class));
                attendStudents.addAll(attendStudentsDefault);
                studentAdapter = new ReportStudentAdapter(this, attendStudents);
                mAssignmentList.setAdapter(studentAdapter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    @OnClick({R.id.mBackBtn, R.id.mAceDes, R.id.mSearch})
    public void onViewClicked(View v) {

        switch (v.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mAceDes:

                if (!isChange) {
                    isChange = true;
                    Collections.sort(attendStudents, (l1, l2) -> {
                        if (l1.getStudentAccuracy() > l2.getStudentAccuracy()) {
                            return 1;
                        } else if (l1.getStudentAccuracy() < l2.getStudentAccuracy()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    });
                } else {
                    isChange = false;
                    Collections.reverse(attendStudents);
                }
                studentAdapter.notifyDataSetChanged();
                break;
            case R.id.mSearch:
                Log.d(TAG, "onViewClicked: " + gson.toJson(user));
                bottomSheetBatchFilter = BottomSheetBatchFilter2.newInstance(user.getBatchBeanList());
                bottomSheetBatchFilter.show(getSupportFragmentManager(), "Custom Bottom Sheet");
                break;

        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBatchFilterEvent(BatchFilterEvent batchFilterEvent) {
        int selId = prefs.getInt(Constant.selectedBatch, -1);
        attendStudents.clear();
        if (selId != -1) {
            for (int i = 0; i < attendStudentsDefault.size(); i++) {
                Log.d(TAG, "onBatchFilterEvent: " + selId);
                if (selId == attendStudentsDefault.get(i).getBatchID()) {
                    Log.d(TAG, "onBatchFilterEvent: matched");
                    attendStudents.add(attendStudentsDefault.get(i));
                } else {
                    Log.d(TAG, "onBatchFilterEvent: not matched");
                }
            }
        } else {
            attendStudents.addAll(attendStudentsDefault);
        }
        studentAdapter.notifyDataSetChanged();
    }


    @Override
    public void callbackStudent(int pos, AttendStudent paperBean) {
        startActivity(new Intent(this, ReportStudentChartActivity.class).putExtra(Constant.attandStudentList, paperBean));
    }

    @Override
    protected void onDestroy() {
        this.prefs.save(Constant.selectedBatch, -1);
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
