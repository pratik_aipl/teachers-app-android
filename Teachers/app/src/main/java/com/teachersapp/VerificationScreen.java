package com.teachersapp;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.onesignal.OneSignal;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.UserData;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.service.SmsReceiver;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class VerificationScreen extends ActivityBase {

    private static final String TAG = "VerificationScreen";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mLogoView)
    LinearLayout mLogoView;
    @BindView(R.id.mVerifyBtn)
    Button mVerifyBtn;
    @BindView(R.id.mReSend)
    TextView mReSend;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mEdtOpt)
    EditText mEdtOpt;

    Subscription subscriptionResendOtp, subscriptionVerify;

    String mobileNo = "", otp = "", msg;
    android.support.v7.app.AlertDialog alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_screen);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        mobileNo = intent.getStringExtra(Constant.MobileNo);
        otp = intent.getStringExtra(Constant.otp);
        msg = intent.getStringExtra(Constant.message);

        hideDialog();
        if (BuildConfig.DEBUG){
            mEdtOpt.setText("123456");
        }
        OneSignal.idsAvailable((userId, registrationId) -> {
            Log.d(TAG, "onCreate: User:" + userId);
            this.registrationId=userId;
//            if (registrationId != null)
//                this.registrationId = registrationId;
//            Log.d(TAG, "onCreate: registrationId:" + registrationId);
        });
//        rxPermissions.request(Manifest.permission.READ_SMS, Manifest.permission.RECEIVE_SMS)
//                .subscribe(granted -> {
//                    if (granted) {
//                        readOTP();
//                    } else {
//                        // Oops permission denied
//                    }
//                });
    }


    private void readOTP() {
        SmsReceiver.bindListener(messageText -> {
            mEdtOpt.setText(messageText);
            Log.d(TAG, "messageReceived: " + messageText.substring(0, 6));
            mEdtOpt.setText(messageText.substring(0, 6));
            mVerifyBtn.performClick();
            //Note: "edt_verify_otp" is your Edittext Object.
        });
    }

    @OnClick({R.id.mBackBtn, R.id.mLogoView, R.id.mVerifyBtn, R.id.mReSend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mVerifyBtn:
                Utils.hideKeyboardFrom(this, view);
                if (!TextUtils.isEmpty(mEdtOpt.getText().toString().trim())) {
                    if (Utils.isNetworkAvailable(this, true))
                        verifyOTP();
                } else {
                    mEdtOpt.setError("Please enter otp.");
                }
                break;
            case R.id.mReSend:
                if (Utils.isNetworkAvailable(this, true))
                    resendOTP();
                break;
        }
    }

    private void resendOTP() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MobileNo, mobileNo);
        map.put(Constant.DeviceID, Utils.getDeviceId(this));
        showProgress(true);
        hideDialog();
        subscriptionResendOtp = NetworkRequest.performAsyncRequest(restApi.reSendOTP(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    alertDialog = Utils.showOneButtonDialog(this, "", jsonResponse.getString(Constant.message));
                    alertDialog.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void hideDialog() {

        if (alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
            alertDialog = null;
        }
    }

    private void verifyOTP() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MobileNo, mobileNo);
        map.put(Constant.OTP, mEdtOpt.getText().toString().trim());
        map.put(Constant.PlayerID, registrationId);
        map.put(Constant.DeviceID, Utils.getDeviceId(this));
        hideDialog();
        showProgress(true);
        subscriptionResendOtp = NetworkRequest.performAsyncRequest(restApi.confirmOTP(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    JSONObject jsonObject = jsonResponse.getJSONObject(Constant.data);

                    UserData user = LoganSquare.parse(jsonObject.getJSONObject(Constant.teacher).toString(), UserData.class);
                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    prefs.save(Constant.login_token, jsonObject.getString(Constant.login_token));
                    prefs.save(Constant.isLogin, true);

                    startActivity(new Intent(this, DashBoard.class)
                            .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                                    Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                    Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    @Override
    protected void onDestroy() {
        if (subscriptionResendOtp != null && !subscriptionResendOtp.isUnsubscribed()) {
            subscriptionResendOtp.unsubscribe();
            subscriptionResendOtp = null;
        }
        if (subscriptionVerify != null && !subscriptionVerify.isUnsubscribed()) {
            subscriptionVerify.unsubscribe();
            subscriptionVerify = null;
        }
        super.onDestroy();
    }
}
