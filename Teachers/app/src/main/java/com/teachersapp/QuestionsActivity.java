package com.teachersapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.teachersapp.adapter.QuestionPagerAdaptor;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.LevelQuestion;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.utils.Constant;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;


public class QuestionsActivity extends ActivityBase {

    private static final String TAG = "TodayTest";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mLabel)
    TextView mRightText;
    @BindView(R.id.mTimer)
    TextView mTimer;
    @BindView(R.id.mPager)
    ViewPager mPager;
    QuestionPagerAdaptor questionPagerAdaptor;
    List<LevelQuestion> questionsBeans = new ArrayList<>();
    @BindView(R.id.mPrev)
    ImageView mPrev;
    @BindView(R.id.mNext)
    ImageView mNext;
    boolean isAutoEndTest = false;
    Subscription subscriptionQuestionsList, subscriptionSubmitTest;
    boolean isAlertShow = false;
    PaperBean paperBean;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.questions_activity);
        ButterKnife.bind(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        mPageTitle.setText(getIntent().getStringExtra(Constant.levelName));
        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.paper);
        setQuestionPage();
        questionsBeans.clear();
        questionsBeans.addAll((Collection<? extends LevelQuestion>) getIntent().getSerializableExtra(Constant.levelQuestionsList));
        questionPagerAdaptor.notifyDataSetChanged();
        updateIndicatorTv();
    }

    private void setQuestionPage() {
        questionPagerAdaptor = new QuestionPagerAdaptor(getSupportFragmentManager(), questionsBeans,paperBean.getStartTime());

        mPager.setAdapter(questionPagerAdaptor);

        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    mPrev.setBackground(ContextCompat.getDrawable(QuestionsActivity.this, R.drawable.circle));
                } else if (position == (mPager.getAdapter().getCount() - 1)) {
                    mNext.setBackground(ContextCompat.getDrawable(QuestionsActivity.this, R.drawable.circle));
                } else {
                    mPrev.setBackground(ContextCompat.getDrawable(QuestionsActivity.this, R.drawable.circle_primary));
                    mNext.setBackground(ContextCompat.getDrawable(QuestionsActivity.this, R.drawable.circle_primary));
                }
                updateIndicatorTv();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if (questionsBeans.size() != 0)
            mPager.setOffscreenPageLimit((questionsBeans.size() - 1));
        updateIndicatorTv();

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume: " + isAlertShow);
    }


    private void updateIndicatorTv() {
        int totalNum = mPager.getAdapter().getCount();
        int currentItem = mPager.getCurrentItem() + 1;
        mRightText.setText(currentItem + " / " + totalNum);
    }


    @OnClick({R.id.mPrev, R.id.mNext, R.id.mBackBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;

            case R.id.mPrev:
                if (mPager.getCurrentItem() > 0) {
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                }
                break;
            case R.id.mNext:
                if (mPager.getCurrentItem() != mPager.getAdapter().getCount()) {
                    mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (subscriptionQuestionsList != null && !subscriptionQuestionsList.isUnsubscribed()) {
            subscriptionQuestionsList.unsubscribe();
            subscriptionQuestionsList = null;
        }
        if (subscriptionSubmitTest != null && !subscriptionSubmitTest.isUnsubscribed()) {
            subscriptionSubmitTest.unsubscribe();
            subscriptionSubmitTest = null;
        }
        super.onDestroy();
    }


}
