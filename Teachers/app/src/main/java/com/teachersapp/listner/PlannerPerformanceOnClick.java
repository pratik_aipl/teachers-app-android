package com.teachersapp.listner;


import com.teachersapp.bean.PlannerPerformance;

public interface PlannerPerformanceOnClick {
    public void callChangeDate(int pos, PlannerPerformance plannerPerformance);
}
