package com.teachersapp.listner;


import com.teachersapp.bean.PublishedBatchBean;

public interface PublishDateOnClick {
    public void callChangeDate(int pos, PublishedBatchBean batchBean);
}
