package com.teachersapp.listner;

import com.teachersapp.bean.PaperBean;

public class PaperReloadEvent {
    PaperBean paperBean;


    public PaperReloadEvent(PaperBean paperBean) {
        this.paperBean = paperBean;
    }

    public PaperBean getPaperBean() {
        return paperBean;
    }
}
