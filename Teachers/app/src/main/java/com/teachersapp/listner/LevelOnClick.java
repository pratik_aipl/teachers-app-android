package com.teachersapp.listner;


import com.teachersapp.bean.LevelBean;

public interface LevelOnClick {
    public void callbackLevel(int pos, LevelBean levelBean);
}
