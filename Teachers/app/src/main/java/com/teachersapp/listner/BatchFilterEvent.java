package com.teachersapp.listner;

public class BatchFilterEvent {
    boolean isReload;

    public BatchFilterEvent(boolean z) {
        this.isReload = z;
    }

    public boolean isReload() {
        return this.isReload;
    }
}
