package com.teachersapp.listner;

public class PageReloadEvent {
    boolean isReload;

    public PageReloadEvent(boolean isReload) {
        this.isReload=isReload;
    }

    public boolean isReload() {
        return isReload;
    }
}
