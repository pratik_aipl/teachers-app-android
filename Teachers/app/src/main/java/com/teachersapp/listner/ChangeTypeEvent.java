package com.teachersapp.listner;

public class ChangeTypeEvent {
    boolean isAttend;

    public boolean isAttend() {
        return isAttend;
    }

    public ChangeTypeEvent(boolean isAttend) {
        this.isAttend = isAttend;
    }
}
