package com.teachersapp.listner;


import com.teachersapp.bean.PaperBean;

public interface PaperOnClick {
    public void callbackPaper(int pos, PaperBean paperBean);
}
