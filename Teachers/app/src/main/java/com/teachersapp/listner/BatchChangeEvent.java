package com.teachersapp.listner;

import com.teachersapp.bean.BatchBean;

public class BatchChangeEvent {
    BatchBean batchBean;

    public BatchChangeEvent(BatchBean batchBean) {
        this.batchBean = batchBean;
    }

    public BatchBean getBatchBean() {
        return batchBean;
    }
}
