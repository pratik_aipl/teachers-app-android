package com.teachersapp.listner;


import com.teachersapp.bean.AttendStudent;
import com.teachersapp.bean.PaperBean;

public interface StudentOnClick {
    public void callbackStudent(int pos, AttendStudent paperBean);
}
