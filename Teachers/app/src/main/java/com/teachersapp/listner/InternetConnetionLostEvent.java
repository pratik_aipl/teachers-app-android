package com.teachersapp.listner;

public class InternetConnetionLostEvent {
    boolean isConnections;

    public InternetConnetionLostEvent(boolean isConnections) {
        this.isConnections = isConnections;
    }

    public boolean isReload() {
        return isConnections;
    }
}
