package com.teachersapp;

import android.animation.Animator;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.onesignal.OneSignal;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.Standard;
import com.teachersapp.bean.ZookiBean;
import com.teachersapp.fragment.BottomSheet3DialogFragment;
import com.teachersapp.fragment.DashBoardFragment;
import com.teachersapp.fragment.ReportFragment;
import com.teachersapp.fragment.SearchFragment;
import com.teachersapp.listner.DialogButtonListener;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.CustPagerTransformer;
import com.teachersapp.fragment.ZookiFragment;
import com.teachersapp.utils.Utils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class DashBoard extends ActivityBase {

    private static final String TAG = "DashBoard";
    boolean doubleBackToExitPressedOnce = false;
    boolean isReload = false;

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    public TextView mRightText;
    @BindView(R.id.mClose)
    ImageView mClose;
    @BindView(R.id.mDashboard)
    TextView mDashboard;
    @BindView(R.id.mMyProfile)
    TextView mMyProfile;
    @BindView(R.id.mQuery)
    TextView mQuery;
    @BindView(R.id.mRateUs)
    TextView mRateUs;
    @BindView(R.id.mLegal)
    TextView mLegal;
    @BindView(R.id.mLogout)
    TextView mLogout;
    @BindView(R.id.layoutButtons)
    RelativeLayout layoutButtons;
    @BindView(R.id.layoutMain)
    RelativeLayout mLayoutMain;
    @BindView(R.id.layoutContent)
    LinearLayout layoutContent;
    @BindView(R.id.mBackView)
    View mBackView;
    @BindView(R.id.mContainer)
    FrameLayout mContainer;
    @BindView(R.id.mBottomGroup)
    RadioGroup mBottomGroup;
    @BindView(R.id.rDashboard)
    RadioButton rDashboard;
    @BindView(R.id.rSearch)
    RadioButton rSearch;
    @BindView(R.id.rReport)
    RadioButton rReport;
    @BindView(R.id.rZooki)
    RadioButton rZooki;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mPager)
    ViewPager viewPager;
    public ArrayList<ZookiBean> zookiList = new ArrayList<>();
    private List<ZookiFragment> fragments = new ArrayList<>();
    private List<Standard> standardList = new ArrayList<>();
    private int Postion = 0;

    private boolean isOpen = false;
    Subscription subscriptionZooki, subscriptionStandard, subscriptionLogout;
    public BottomSheet3DialogFragment bottomSheet3DialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        mPageTitle.setVisibility(View.VISIBLE);
        mPageTitle.setText("Children's Academy school");
        mBackBtn.setImageResource(R.mipmap.menu);

        OneSignal.idsAvailable((userId, registrationId) -> {
            this.registrationId = userId;
        });


//        Utils.clearFilter(prefs);
        rDashboard.setChecked(true);
        navigateTo(new DashBoardFragment(), null, false);

        mBottomGroup.setOnCheckedChangeListener((group, checkedId) -> {
            mSearchText.setVisibility(View.GONE);
            mSearch.setVisibility(View.GONE);
            mPageTitle.setVisibility(View.GONE);
            mRightText.setVisibility(View.GONE);
            viewPager.setVisibility(View.GONE);
            switch (checkedId) {
                case R.id.rDashboard:
                    mContainer.setVisibility(View.VISIBLE);
                    mPageTitle.setVisibility(View.VISIBLE);
                    mPageTitle.setText("Children's Academy school");
                    if (!isReload)
                        navigateTo(new DashBoardFragment(), null, false);
                    else
                        isReload = false;
                    break;
                case R.id.rSearch:
                    mContainer.setVisibility(View.VISIBLE);
//                    mSearchText.setVisibility(View.VISIBLE);
                    mSearch.setVisibility(View.VISIBLE);
                    mPageTitle.setVisibility(View.VISIBLE);
                    mPageTitle.setText("Search");
                    if (!isReload) {
                        navigateTo(new SearchFragment(), null, false);
                    } else
                        isReload = false;
                    break;
                case R.id.rReport:
                    mContainer.setVisibility(View.VISIBLE);
                    mPageTitle.setVisibility(View.VISIBLE);
                    mPageTitle.setText("Reports");
                    if (!isReload)
                        navigateTo(new ReportFragment(), null, false);
                    else
                        isReload = false;

                    break;
                case R.id.rZooki:
                    mContainer.setVisibility(View.GONE);
                    viewPager.setVisibility(View.VISIBLE);
                    mPageTitle.setVisibility(View.VISIBLE);
                    mRightText.setVisibility(View.VISIBLE);
                    mPageTitle.setText("Zooki");
                    break;
            }
        });

        if (Utils.isNetworkAvailable(this, true)) {
            getSubjectList(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.isNetworkAvailable(this, true))
            callZooki(false);
    }

    private void callZooki(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress(isShow);
        subscriptionZooki = NetworkRequest.performAsyncRequest(restApi.getZookiList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    if (jsonResponse.has(Constant.folderPath))
                        prefs.save(Constant.folderPath, jsonResponse.getString(Constant.folderPath));
                    zookiList.clear();
                    zookiList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), ZookiBean.class));
                    fillViewPager();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                com.teachersapp.utils.Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void getSubjectList(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress(isShow);
        Log.d(TAG, "getPaperByDate: " + map);
        subscriptionStandard = NetworkRequest.performAsyncRequest(restApi.getSubjectList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    standardList.clear();
                    Standard standard = new Standard();
                    standard.setStandard("Select Standard");
                    standard.setStandardID(-1);
                    standardList.add(standard);
                    standardList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray(Constant.data).toString(), Standard.class));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                com.teachersapp.utils.Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void fillViewPager() {
        viewPager.setPageTransformer(false, new CustPagerTransformer(this));
        for (int i = 0; i < zookiList.size(); i++) {
            fragments.add(new ZookiFragment());
        }
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                ZookiFragment fragment = fragments.get(position);
                fragment.bindData(zookiList.get(position));
                return fragment;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return super.isViewFromObject(view, object);
            }

            @Override
            public int getCount() {
                return zookiList.size();
            }
        });

        viewPager.setCurrentItem(Postion);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                updateIndicatorTv();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        updateIndicatorTv();
    }

    private void updateIndicatorTv() {
        int totalNum = viewPager.getAdapter().getCount();
        int currentItem = viewPager.getCurrentItem() + 1;
        mRightText.setText(currentItem + " / " + totalNum);
    }


    public void navigateTo(Fragment newFragment, Bundle bundle, boolean isAdd) {
        newFragment.setArguments(bundle);
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (isAdd)
            ft.add(R.id.mContainer, newFragment);
        else
            ft.replace(R.id.mContainer, newFragment);
        ft.commit();
        //manager .popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @OnClick({R.id.mBackBtn, R.id.mBackView, R.id.mClose, R.id.mDashboard, R.id.mMyProfile,
            R.id.mQuery, R.id.mRateUs, R.id.mLegal, R.id.mLogout, R.id.mSearch})
    public void onViewClicked(View view) {
        Intent i = null;
        switch (view.getId()) {
            case R.id.mClose:
            case R.id.mBackBtn:
            case R.id.mBackView:
                viewMenu();
                break;
            case R.id.mDashboard:
                viewMenu();
                rDashboard.setChecked(true);
                break;
            case R.id.mMyProfile:
                viewMenu();
                i = new Intent(this, Profile.class);
                break;
            case R.id.mQuery:
                viewMenu();
                i = new Intent(this, Query.class);
                break;
            case R.id.mRateUs:
                viewMenu();
                launchMarket();
                break;
            case R.id.mLegal:
                viewMenu();
                i = new Intent(this, Legal.class);
                break;
            case R.id.mLogout:
                viewMenu();
                Utils.showTwoButtonDialog(this, "Logout", "Are you sure you want logout?", "Logout", "Cancel", new DialogButtonListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        callLogOut();
                    }

                    @Override
                    public void onNegativButtonClicked() {
                    }
                });
                break;
            case R.id.mSearch:
                bottomSheet3DialogFragment = BottomSheet3DialogFragment.newInstance(standardList);
                bottomSheet3DialogFragment.show(getSupportFragmentManager(), "Custom Bottom Sheet");
                break;
        }
        if (i != null)
            startActivity(i);
    }

    private void launchMarket() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, " unable to find market app", Toast.LENGTH_LONG).show();
        }
    }

    private void viewMenu() {

        if (!isOpen) {
            int x = layoutContent.getLeft();
            int y = layoutContent.getTop();
            int startRadius = 0;
            int endRadius = (int) Math.hypot(mLayoutMain.getWidth(), mLayoutMain.getHeight());
//            fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), android.R.color.white, null)));
//            fab.setImageResource(R.drawable.ic_close_grey);
            Animator anim = ViewAnimationUtils.createCircularReveal(layoutButtons, x, y, startRadius, endRadius);
            layoutButtons.setVisibility(View.VISIBLE);
            anim.start();
            isOpen = true;
        } else {
            int x = layoutButtons.getLeft();
            int y = layoutButtons.getTop();
            int startRadius = Math.max(layoutContent.getWidth(), layoutContent.getHeight());
            int endRadius = 0;
//            fab.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(getResources(), R.color.colorAccent, null)));
//            fab.setImageResource(R.drawable.ic_plus_white);
            Animator anim = ViewAnimationUtils.createCircularReveal(layoutButtons, x, y, startRadius, endRadius);
            anim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    layoutButtons.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
            anim.start();
            isOpen = false;
        }
    }


    private void callLogOut() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.DeviceID, Utils.getDeviceId(this));
        map.put(Constant.PlayerID, registrationId);
        showProgress(true);
        subscriptionLogout = NetworkRequest.performAsyncRequest(restApi.logOut(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                prefs.save(Constant.UserData, "");
                prefs.save(Constant.isLogin, false);
                startActivity(new Intent(DashBoard.this, MobileScreen.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        | Intent.FLAG_ACTIVITY_CLEAR_TASK |
                        Intent.FLAG_ACTIVITY_SINGLE_TOP |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
//            showConnectionSnackBarUserLogin();
        });
    }


    @Override
    public void onBackPressed() {
        if (isOpen) {
            viewMenu();
        } else {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Touch again to exit.", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 1000);
        }
    }

    @Override
    protected void onDestroy() {
        if (subscriptionLogout != null && !subscriptionLogout.isUnsubscribed()) {
            subscriptionLogout.unsubscribe();
            subscriptionLogout = null;
        }
        if (subscriptionStandard != null && !subscriptionStandard.isUnsubscribed()) {
            subscriptionStandard.unsubscribe();
            subscriptionStandard = null;
        }
        super.onDestroy();
    }
}
