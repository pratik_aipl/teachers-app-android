package com.teachersapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.teachersapp.adapter.ReportQuestionsAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.LevelQuestion;
import com.teachersapp.fragment.BottomSheetBatchFilter2;
import com.teachersapp.utils.Constant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportQuestionsActivity extends ActivityBase {

    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mQuestionsList)
    RecyclerView mQuestionsList;

    List<LevelQuestion> levelQuestion = new ArrayList<>();
    ReportQuestionsAdapter reportQuestionsAdapter;
    boolean isChange = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_questions);
        ButterKnife.bind(this);
        mPageTitle.setText("Questions");
        mSearch.setVisibility(View.VISIBLE);
        mSearch.setImageDrawable(ContextCompat.getDrawable(this, R.mipmap.sort));
        levelQuestion = (List<LevelQuestion>) getIntent().getSerializableExtra(Constant.questionsList);
        reportQuestionsAdapter = new ReportQuestionsAdapter(this, levelQuestion, Constant.questionsList);
        mQuestionsList.setAdapter(reportQuestionsAdapter);

    }


    @OnClick({R.id.mBackBtn, R.id.mSearch})
    public void onViewClicked(View v) {

        switch (v.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                if (!isChange) {
                    isChange = true;
                    Collections.sort(levelQuestion, (l1, l2) -> {
                        if (l1.getCorrect() > l2.getCorrect()) {
                            return 1;
                        } else if (l1.getCorrect() < l2.getCorrect()) {
                            return -1;
                        } else {
                            return 0;
                        }
                    });
                } else {
                    isChange = false;
                    Collections.reverse(levelQuestion);
                }

                reportQuestionsAdapter.notifyDataSetChanged();
                break;

        }

    }
}
