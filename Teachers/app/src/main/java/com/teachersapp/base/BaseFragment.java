package com.teachersapp.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;

import com.google.gson.Gson;
import com.tbruyelle.rxpermissions2.RxPermissions;
import com.teachersapp.bean.UserData;
import com.teachersapp.network.RestAPIBuilder;
import com.teachersapp.network.RestApi;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Prefs;

public class BaseFragment extends Fragment {
    protected LinearLayoutManager layoutManager;
    private static final String TAG = "BaseFragment";

    protected RestApi restApi;
    public Prefs prefs;
    public UserData user;
    protected RxPermissions rxPermissions;
    ProgressDialog progressDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        layoutManager = new LinearLayoutManager(getActivity());
        rxPermissions = new RxPermissions(this);
        restApi = RestAPIBuilder.buildRetrofitService();

        Gson gson = new Gson();
        prefs = Prefs.with(getActivity());
        user = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);

    }

    public void showProgress(boolean isShow) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading...");
        }
        if (isShow) {
            if (!progressDialog.isShowing())
                progressDialog.show();
        } else {
            progressDialog.dismiss();
        }
    }
}
