package com.teachersapp;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.teachersapp.adapter.ViewPagerAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.BranchBean;
import com.teachersapp.bean.UserData;
import com.teachersapp.fragment.BatchFragment;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class Profile extends ActivityBase {


    boolean isEdit = false;
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mUserImage)
    ImageView mUserImage;
    @BindView(R.id.mUserName)
    TextView mUserName;
    @BindView(R.id.mStudentRegisterId)
    TextView mStudentRegisterId;
    @BindView(R.id.mMobileNo)
    EditText mMobileNo;
    @BindView(R.id.mEmail)
    EditText mEmail;
    @BindView(R.id.mEditProfile)
    ImageView mEditProfile;
    @BindView(R.id.mTabs)
    TabLayout mTabs;
    @BindView(R.id.mViewPager)
    ViewPager mViewPager;

    Subscription subscriptionProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        mPageTitle.setText("Profile");
        editMode(isEdit);
        mPageTitle.requestFocus();

//        setData(user);

        if (Utils.isNetworkAvailable(this, true))
            getUserData(true);


    }


    private void getUserData(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        showProgress(isShow);
        subscriptionProfile = NetworkRequest.performAsyncRequest(restApi.getProfile(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    user = LoganSquare.parse(jsonResponse.getJSONObject(Constant.data).toString(), UserData.class);
                    prefs.save(Constant.UserData, new Gson().toJson(user));
                    setData(user);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void setData(UserData user) {

        mUserName.setText(user.getFirstName() + " " + user.getLastName());
        mEmail.setText(user.getEmail());
        mMobileNo.setText(user.getMobileNo());


        setupViewPager(mViewPager);

        mTabs.setupWithViewPager(mViewPager);
    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        if (user.getBranchList() != null && user.getBranchList().size() > 0) {
            for (int i = 0; i < user.getBranchList().size(); i++) {
                BranchBean branchBean = user.getBranchList().get(i);
                adapter.addFragment(BatchFragment.newInstance(branchBean), branchBean.getBranchName());
                viewPager.setAdapter(adapter);
            }
        }
        mViewPager.setOffscreenPageLimit(user.getBranchList().size()-1);
    }

    private void editMode(boolean isEdit) {
        if (isEdit) {
            mEditProfile.setImageResource(R.mipmap.save);
        } else {
            mEditProfile.setImageResource(R.mipmap.edit);
        }
        mEmail.setEnabled(isEdit);
        mMobileNo.setEnabled(false);
//        mBoard.setEnabled(isEdit);
//        mMedium.setEnabled(isEdit);
//        mStandard.setEnabled(isEdit);
//        mBranch.setEnabled(isEdit);
//        mBatch.setEnabled(isEdit);
    }

    @OnClick({R.id.mBackBtn, R.id.mSearch, R.id.mEditProfile})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mSearch:
                break;
            case R.id.mEditProfile:
                isEdit = !isEdit;
                editMode(isEdit);
                break;
        }
    }


}
