package com.teachersapp.notificationreciver;

import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

/**
 * Created by abc on 12/22/2017.
 */

public class NotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

    public NotificationReceivedHandler() {
    }

    @Override
    public void notificationReceived(OSNotification notification) {


        JSONObject data = notification.payload.additionalData;
        String customKey;

        System.out.println("data:::" + data);

        if (data != null) {
            customKey = data.optString("IsOnline", null);
            if (customKey != null) {
                Log.i("OneSignalExample", "customkey set with value: " + customKey);


            }
        }
    }
}
