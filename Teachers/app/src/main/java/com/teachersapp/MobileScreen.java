package com.teachersapp;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.onesignal.OneSignal;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class MobileScreen extends ActivityBase {

    private static final String TAG = "MobileScreen";
    @BindView(R.id.mMobileNo)
    EditText mMobileNo;
    @BindView(R.id.mSubmit)
    Button mSubmit;
    private Subscription subscriptionLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_screen);
        ButterKnife.bind(this);
        if (BuildConfig.DEBUG) {
            mMobileNo.setText("1234567891");
            mMobileNo.setText("9624049054");
        }
        OneSignal.idsAvailable((userId, registrationId) -> {
            this.registrationId = userId;
            Log.d(TAG, "onCreate: playerID "+this.registrationId);
        });
    }

    private void callLogin() {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MobileNo, mMobileNo.getText().toString().trim());
        map.put(Constant.DeviceID, Utils.getDeviceId(this));
        Log.d(TAG, "callLogin: " + map);
        showProgress(true);
        subscriptionLogin = NetworkRequest.performAsyncRequest(restApi.getLogin(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    JSONObject jsonObject = jsonResponse.getJSONObject(Constant.data);
                    String userId = "", Otp = "";
                    if (jsonObject.has(Constant.user_id)) {
                        userId = jsonObject.getString(Constant.user_id);
                    }
                    if (jsonObject.has(Constant.otp)) {
                        Otp = jsonObject.getString(Constant.otp);
                    }
                    Intent i = new Intent(this, VerificationScreen.class);
                    i.putExtra(Constant.MobileNo, mMobileNo.getText().toString().trim());
                    i.putExtra(Constant.otp, Otp);
                    i.putExtra(Constant.message, jsonResponse.getString(Constant.message));
                    startActivity(i);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utils.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
//            showConnectionSnackBarUserLogin();
        });
    }

    @Override
    protected void onDestroy() {

        if (subscriptionLogin != null && !subscriptionLogin.isUnsubscribed()) {
            subscriptionLogin.unsubscribe();
            subscriptionLogin = null;
        }
        super.onDestroy();
    }

    @OnClick({R.id.mMobileNo, R.id.mSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mMobileNo:
                break;
            case R.id.mSubmit:
                if (!TextUtils.isEmpty(mMobileNo.getText().toString().trim())) {
                    if (Utils.isNetworkAvailable(this, true))
                        callLogin();

                } else
                    mMobileNo.setError("Please enter mobile no.");
                break;
        }
    }
}
