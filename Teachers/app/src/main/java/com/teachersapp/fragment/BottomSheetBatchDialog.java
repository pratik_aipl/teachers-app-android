package com.teachersapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.teachersapp.PaperScreen;
import com.teachersapp.R;
import com.teachersapp.ReportActivity;
import com.teachersapp.adapter.BatchAdapter;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.listner.BatchChangeEvent;
import com.teachersapp.listner.BatchOnClick;
import com.teachersapp.listner.PageReloadEvent;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Prefs;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BottomSheetBatchDialog extends BottomSheetDialogFragment implements BatchOnClick {

    private static final String TAG = "BottomSheet3DialogFragm";
    @BindView(R.id.mClose)
    Button mReset;
    @BindView(R.id.mBatch)
    RecyclerView mBatch;
    Unbinder unbinder;

    List<BatchBean> batchBeanList;
    @BindView(R.id.bottomSheetLayout)
    LinearLayout bottomSheetLayout;


    Prefs prefs;

    public static BottomSheetBatchDialog newInstance(List<BatchBean> standardList) {
        BottomSheetBatchDialog f = new BottomSheetBatchDialog();
        Bundle b = new Bundle();
        b.putSerializable(Constant.chapterBeanList, (Serializable) standardList);
        f.setArguments(b);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fillter_batch_bottom_dailog, container, false);
        unbinder = ButterKnife.bind(this, view);
        prefs = Prefs.with(getActivity());
        mBatch.setAdapter(new BatchAdapter(getActivity(), (List<BatchBean>) getArguments().getSerializable(Constant.chapterBeanList), BottomSheetBatchDialog.this));


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }



    @Override
    public void callbackPaper(int pos, BatchBean batchBean) {
        ((ReportActivity) getActivity()).bottomSheetBatchDialog.dismiss();
        prefs.save(Constant.selectedBranch, batchBean.getBatchID());
        EventBus.getDefault().post(new BatchChangeEvent(batchBean));
    }

    @OnClick({R.id.mClose})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mClose:
                ((ReportActivity) getActivity()).bottomSheetBatchDialog.dismiss();
                break;
        }
    }
}
