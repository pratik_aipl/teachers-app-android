package com.teachersapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.teachersapp.R;
import com.teachersapp.ReportAssignmentActivity;
import com.teachersapp.adapter.BatchFilterAdapter;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.bean.UserData;
import com.teachersapp.listner.BatchFilterEvent;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Prefs;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BottomSheetBatchFilter2 extends BottomSheetDialogFragment {
    private static final String TAG = "BottomSheet3DialogFragm";
    List<BatchBean> batchBeans = new ArrayList();
    @BindView(R.id.mBatchList)
    Spinner mBatchList;
    @BindView(R.id.mReset)
    Button mReset;
    @BindView(R.id.mSave)
    Button mSave;
    Prefs prefs;
    Unbinder unbinder;
    Gson gson;

    public static BottomSheetBatchFilter2 newInstance(List<BatchBean> list) {
        BottomSheetBatchFilter2 bottomSheetBatchFilter = new BottomSheetBatchFilter2();
        Bundle bundle = new Bundle();
        //  bundle.putSerializable(Constant.batchList, (Serializable) list);
        bottomSheetBatchFilter.setArguments(bundle);
        return bottomSheetBatchFilter;
    }

    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fillter_batch_dailog, viewGroup, false);
        unbinder = ButterKnife.bind((Object) this, inflate);
        prefs = Prefs.with(getActivity());
        batchBeans.clear();
        gson = new Gson();
        UserData userData = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);
        BatchBean batchBean = new BatchBean();
        batchBean.setBatchID(0);
        batchBean.setBatch("Select Batch");
        batchBean.setAccuracy("0.0");
        batchBean.setLevelAccuracy(new ArrayList());
        batchBeans.add(batchBean);
        batchBeans.addAll(userData.getBatchBeanList());
        mBatchList.setAdapter(new BatchFilterAdapter(getActivity(), batchBeans, true));

        for (int i = 0; i < batchBeans.size(); i++) {
            Log.d(TAG, "onCreateView: " + batchBeans.get(i).getBatch());
        }
        if (prefs.getInt(Constant.selectedBatch, -1) != -1) {
            for (int i = 0; i < batchBeans.size(); i++) {
                Log.d(TAG, "onCreateView: " + batchBeans.get(i).getBatch());
                if (batchBeans.get(i).getBatchID() == prefs.getInt(Constant.selectedBatch, -1)) {
                    mBatchList.setSelection(i);
                    break;
                }
            }
        }

        mBatchList.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onNothingSelected(AdapterView<?> adapterView) {
            }

            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                BottomSheetBatchFilter2.this.prefs.save(Constant.selectedBatch, ((BatchBean) adapterView.getItemAtPosition(i)).getBatchID());
            }
        });
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.mSave, R.id.mReset})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mReset:
                mBatchList.setSelection(0);
                prefs.save(Constant.selectedBatch, -1);
                break;
            case R.id.mSave:
                EventBus.getDefault().post(new BatchFilterEvent(true));
                ((ReportAssignmentActivity) getActivity()).bottomSheetBatchFilter.dismiss();
                break;
        }
    }

}
