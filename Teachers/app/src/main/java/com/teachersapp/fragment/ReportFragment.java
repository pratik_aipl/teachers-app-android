package com.teachersapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;
import com.teachersapp.AllAssignmentActivity;
import com.teachersapp.R;
import com.teachersapp.ReportAssignmentActivity;
import com.teachersapp.adapter.AssignmentAdapter;
import com.teachersapp.base.BaseFragment;
import com.teachersapp.bean.TeacherReportBean;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ReportFragment extends BaseFragment {

    private static final String TAG = "ReportFragment";
    @BindView(R.id.mAccuracy)
    TextView mAccuracy;
    @BindView(R.id.mReportLevel)
    RecyclerView mReportLevel;
    @BindView(R.id.mAssignmentPending)
    TextView mAssignmentPending;
    @BindView(R.id.mAssignmentPendingLine)
    View mAssignmentPendingLine;
    @BindView(R.id.mAssignmentPendingList)
    RecyclerView mAssignmentPendingList;
    @BindView(R.id.mAssignmentFuture)
    TextView mAssignmentFuture;
    @BindView(R.id.mAssignmentFutureLine)
    View mAssignmentFutureLine;
    @BindView(R.id.mAssignmentFutureList)
    RecyclerView mAssignmentFutureList;
    @BindView(R.id.mAssignmentMissed)
    TextView mAssignmentMissed;
    @BindView(R.id.mAssignmentMissedLine)
    View mAssignmentMissedLine;
    @BindView(R.id.mAssignmentMissedList)
    RecyclerView mAssignmentMissedList;
    @BindView(R.id.mStudent)
    TextView mStudentReport;
    @BindView(R.id.mAllAssignment)
    TextView mAllAssignment;
    @BindView(R.id.mPerformance)
    TextView mPerformance;
    @BindView(R.id.mPerformanceView)
    LinearLayout mPerformanceView;

    Subscription subscriptionTeacherReport;
    TeacherReportBean teacherReportBean;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        ButterKnife.bind(this, view);

        if (Utils.isNetworkAvailable(getActivity(), true))
            getTeacherReports(true);
        mPerformance.setCompoundDrawables(null,null,null,null);
        mPerformanceView.setPadding(0,0,0,0);
        mReportLevel.setVisibility(View.GONE);
        return view;
    }


    private void getTeacherReports(boolean isShow) {
        Map<String, String> map = new HashMap<>();

        showProgress(isShow);
        subscriptionTeacherReport = NetworkRequest.performAsyncRequest(restApi.teacherReport(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    Log.d(TAG, "getTeacherReports: " + jsonResponse);
                    teacherReportBean = LoganSquare.parse(jsonResponse.getJSONObject(Constant.data).toString(), TeacherReportBean.class);

                    mAccuracy.setText(teacherReportBean.getTotalAccuracy() + "%");

                    mAssignmentPendingList.setAdapter(new AssignmentAdapter(getActivity(), teacherReportBean.getPendingAssignmentList(), false));
                    mAssignmentFutureList.setAdapter(new AssignmentAdapter(getActivity(), teacherReportBean.getFutureAssignmentList(),false));
                    mAssignmentMissedList.setAdapter(new AssignmentAdapter(getActivity(), teacherReportBean.getMissedAssignmentList(), false));

                } catch (Exception e) {
                    e.printStackTrace();

                }
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @OnClick({R.id.mAssignmentPending, R.id.mAssignmentFuture, R.id.mAssignmentMissed, R.id.mStudent, R.id.mAllAssignment})
    public void onViewClicked(View view) {
        prefs.save(Constant.selectedBatch, -1);
        switch (view.getId()) {
            case R.id.mAssignmentPending:
                if (teacherReportBean !=null && teacherReportBean.getPendingAssignmentList().size() > 0)
                    getActivity().startActivity(new Intent(getActivity(), ReportAssignmentActivity.class).putExtra(Constant.fromAll,false).putExtra(Constant.assignment, (Serializable) teacherReportBean.getPendingAssignmentList()).putExtra(Constant.page, Constant.assignmentpending).putExtra(Constant.isAssignment, true));
                else
                    Toast.makeText(getActivity(), "Pending Assignment Not Available.", Toast.LENGTH_SHORT).show();
                break;
            case R.id.mAssignmentFuture:
                if (teacherReportBean !=null && teacherReportBean.getFutureAssignmentList().size() > 0)
                    getActivity().startActivity(new Intent(getActivity(), ReportAssignmentActivity.class).putExtra(Constant.fromAll,false).putExtra(Constant.assignment, (Serializable) teacherReportBean.getFutureAssignmentList()).putExtra(Constant.page, Constant.assignmentnext).putExtra(Constant.isAssignment, true));
                else
                    Toast.makeText(getActivity(), "Next Assignment Not Available.", Toast.LENGTH_SHORT).show();

                break;
            case R.id.mAssignmentMissed:
                if (teacherReportBean!=null && teacherReportBean.getMissedAssignmentList().size() > 0)
                    getActivity().startActivity(new Intent(getActivity(), ReportAssignmentActivity.class).putExtra(Constant.fromAll,false).putExtra(Constant.assignment, (Serializable) teacherReportBean.getMissedAssignmentList()).putExtra(Constant.page, Constant.assignmentmissed).putExtra(Constant.isAssignment, true));
                else
                    Toast.makeText(getActivity(), "Missed Assignment Not Available.", Toast.LENGTH_SHORT).show();

                break;
            case R.id.mStudent:
                if (teacherReportBean!=null && teacherReportBean.getStudentInfo().size() > 0){
                    this.prefs.save(Constant.data, new Gson().toJson((Object) teacherReportBean.getStudentInfo()));
                    getActivity().startActivity(new Intent(getActivity(), ReportAssignmentActivity.class).putExtra(Constant.fromAll,false).putExtra(Constant.page, Constant.studentreport).putExtra(Constant.isAssignment, false));
                }
                else
                    Toast.makeText(getActivity(), "Student not available.", Toast.LENGTH_SHORT).show();
            case R.id.mAllAssignment:
                if (teacherReportBean!=null && teacherReportBean.getStudentInfo().size() > 0){
//                    this.prefs.save(Constant.data, new Gson().toJson((Object) teacherReportBean.getStudentInfo()));
                    getActivity().startActivity(new Intent(getActivity(), AllAssignmentActivity.class).putExtra(Constant.fromAll,true).putExtra(Constant.page, Constant.allAssignment).putExtra(Constant.isAssignment, false));
                }
                else
                    Toast.makeText(getActivity(), "Student not available.", Toast.LENGTH_SHORT).show();

                break;
        }
    }
}
