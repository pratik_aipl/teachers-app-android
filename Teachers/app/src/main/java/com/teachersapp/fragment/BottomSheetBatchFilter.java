package com.teachersapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

import com.teachersapp.R;
import com.teachersapp.ReportStudentActivity;
import com.teachersapp.adapter.BatchFilterAdapter;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.listner.BatchFilterEvent;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Prefs;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BottomSheetBatchFilter extends BottomSheetDialogFragment {
    private static final String TAG = "BottomSheet3DialogFragm";
    List<BatchBean> batchBeans = new ArrayList();
    @BindView(R.id.mBatchList)
    Spinner mBatchList;
    @BindView(R.id.mReset)
    Button mReset;
    @BindView(R.id.mSave)
    Button mSave;
    Prefs prefs;
    Unbinder unbinder;

    public static BottomSheetBatchFilter newInstance(List<BatchBean> list) {
        BottomSheetBatchFilter bottomSheetBatchFilter = new BottomSheetBatchFilter();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constant.batchList, (Serializable) list);
        bottomSheetBatchFilter.setArguments(bundle);
        return bottomSheetBatchFilter;
    }

    @Nullable
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup viewGroup, @Nullable Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.fillter_batch_dailog, viewGroup, false);
        unbinder = ButterKnife.bind((Object) this, inflate);
        prefs = Prefs.with(getActivity());
        batchBeans.clear();
        BatchBean batchBean = new BatchBean();
        batchBean.setBatchID(0);
        batchBean.setBatchName("Select Batch");
        batchBean.setAccuracy("0.0");
        batchBean.setLevelAccuracy(new ArrayList());
        batchBeans.add(batchBean);
        batchBeans.addAll((Collection) getArguments().getSerializable(Constant.batchList));
        mBatchList.setAdapter(new BatchFilterAdapter(getActivity(), batchBeans,false));

        if (prefs.getInt(Constant.selectedBatch, -1) != -1) {
            for (int i = 0; i < batchBeans.size(); i++) {
                if (batchBeans.get(i).getBatchID() == prefs.getInt(Constant.selectedBatch, -1)) {
                    mBatchList.setSelection(i);
                    break;
                }
            }
        }

        mBatchList.setOnItemSelectedListener(new OnItemSelectedListener() {
            public void onNothingSelected(AdapterView<?> adapterView) {
            }

            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                BottomSheetBatchFilter.this.prefs.save(Constant.selectedBatch, ((BatchBean) adapterView.getItemAtPosition(i)).getBatchID());
            }
        });
        return inflate;
    }

    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.mSave, R.id.mReset})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mReset:
                mBatchList.setSelection(0);
                prefs.save(Constant.selectedBatch, -1);
                break;
            case R.id.mSave:
                EventBus.getDefault().post(new BatchFilterEvent(true));
                ((ReportStudentActivity) getActivity()).bottomSheetBatchFilter.dismiss();
                break;
        }
    }

}
