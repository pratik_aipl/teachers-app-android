package com.teachersapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.teachersapp.PaperScreen;
import com.teachersapp.R;
import com.teachersapp.ShareScreen;
import com.teachersapp.adapter.ChapterAdapter;
import com.teachersapp.bean.ChapterBean;
import com.teachersapp.bean.Standard;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Prefs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BottomSheetChapterDialog extends BottomSheetDialogFragment {

    private static final String TAG = "BottomSheet3DialogFragm";
    @BindView(R.id.mClose)
    Button mClose;
    @BindView(R.id.mChapter)
    RecyclerView mChapter;
    Unbinder unbinder;

    public static BottomSheetChapterDialog newInstance(List<ChapterBean> standardList) {
        BottomSheetChapterDialog f = new BottomSheetChapterDialog();
        Bundle b = new Bundle();
        b.putSerializable(Constant.chapterBeanList, (Serializable) standardList);
        f.setArguments(b);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fillter_chapter_bottom_dailog, container, false);
        unbinder = ButterKnife.bind(this, view);
        mChapter.setAdapter(new ChapterAdapter(getActivity(), (List<ChapterBean>) getArguments().getSerializable(Constant.chapterBeanList)));
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.mClose})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mClose:
                ((PaperScreen)getActivity()).bottomSheetChapterDialog.dismiss();
                break;
        }
    }


}
