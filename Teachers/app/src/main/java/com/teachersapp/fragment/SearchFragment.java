package com.teachersapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.teachersapp.PaperScreen;
import com.teachersapp.R;
import com.teachersapp.adapter.PaperAdapter;
import com.teachersapp.base.BaseFragment;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.bean.PaperBeanMain;
import com.teachersapp.listner.PageReloadEvent;
import com.teachersapp.listner.PaperOnClick;
import com.teachersapp.listner.PaperReloadEvent;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class SearchFragment extends BaseFragment implements PaperOnClick {
    private static final String TAG = "SearchFragment";
    @BindView(R.id.mPaperList)
    RecyclerView mPaperList;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeLayout;
    @BindView(R.id.mEmptyList)
    TextView mEmptyList;

    private RecyclerView.LayoutManager layoutManager;
    private PaperAdapter paperAdapter;
    List<PaperBean> paperBeanList = new ArrayList<>();
    Subscription subscriptionPaperList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //getActivity().getWindow().setBackgroundDrawableResource(R.mipmap.category_intro);
        View view = inflater.inflate(R.layout.list_view, container, false);
        ButterKnife.bind(this, view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        layoutManager = new LinearLayoutManager(getActivity());
        mPaperList.setLayoutManager(layoutManager);
        mPaperList.setItemAnimator(new DefaultItemAnimator());

        paperAdapter = new PaperAdapter(getActivity(), paperBeanList, SearchFragment.this, true);
        mPaperList.setAdapter(paperAdapter);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPaper(false);
            }
        });
        if (Utils.isNetworkAvailable(getActivity(), true)) {
            getPaper(true);
        }
        return view;
    }

    @Override
    public void onDestroyView() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);

        if (subscriptionPaperList != null && !subscriptionPaperList.isUnsubscribed()) {
            subscriptionPaperList.unsubscribe();
            subscriptionPaperList = null;
        }
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);

        if (subscriptionPaperList != null && !subscriptionPaperList.isUnsubscribed()) {
            subscriptionPaperList.unsubscribe();
            subscriptionPaperList = null;
        }
        Utils.clearFilter(prefs);
        super.onDestroy();
    }

    @Override
    public void callbackPaper(int pos, PaperBean paperBean) {
        startActivity(new Intent(getActivity(), PaperScreen.class).putExtra(Constant.paper, paperBean));
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPageReloadEvent(PageReloadEvent event) {
        getPaper(true);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPaperReloadEventEvent(PaperReloadEvent event) {
        for (int i = 0; i < paperBeanList.size(); i++) {
            PaperBean paperBean = paperBeanList.get(i);
            if (paperBean.getMCQPlannerID().equalsIgnoreCase(event.getPaperBean().getMCQPlannerID())) {
                paperBeanList.set(i, event.getPaperBean());
                paperAdapter.notifyDataSetChanged();
            }
        }
    }
    private void getPaper(boolean isShow) {
        Map<String, String> map = new HashMap<>();
        if (!TextUtils.isEmpty(prefs.getString(Constant.selectDate, "")))
            map.put(Constant.Date, prefs.getString(Constant.selectDate, ""));
        if (prefs.getInt(Constant.selectSubject, -1) != -1)
            map.put(Constant.SubjectID, String.valueOf(prefs.getInt(Constant.selectSubject, -1)));
        if ( prefs.getInt(Constant.selectStandard, -1) != -1)
            map.put(Constant.StandardID, String.valueOf(prefs.getInt(Constant.selectStandard, -1)));
        showProgress(isShow);
        Log.d(TAG, "getPaperByDate: " + map);
        subscriptionPaperList = NetworkRequest.performAsyncRequest(restApi.getPaperList(map), (data) -> {
            swipeLayout.setRefreshing(false);
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    paperBeanList.clear();
                    PaperBeanMain paperBeanMain = LoganSquare.parse(jsonResponse.get(Constant.data).toString(), PaperBeanMain.class);
                    paperBeanList.addAll(paperBeanMain.getPaperInfo());
                    paperAdapter.notifyDataSetChanged();
                    mEmptyList.setTextColor(ContextCompat.getColor(getActivity(),R.color.white));
                    if (paperBeanList.size() != 0) {
                        mEmptyList.setVisibility(View.GONE);
                    } else {
                        mEmptyList.setText(getString(R.string.empty_other_list_msg));
                        mEmptyList.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                com.teachersapp.utils.Utils.serviceStatusFalseProcess(getActivity(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }


}
