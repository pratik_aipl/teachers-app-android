package com.teachersapp.fragment;

import android.app.DatePickerDialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.teachersapp.AllAssignmentActivity;
import com.teachersapp.DashBoard;
import com.teachersapp.R;
import com.teachersapp.adapter.BatchFilterAdapter;
import com.teachersapp.adapter.StandardAdapter;
import com.teachersapp.adapter.SubjectAdapter;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.bean.BranchBean;
import com.teachersapp.bean.Standard;
import com.teachersapp.bean.Subject;
import com.teachersapp.bean.UserData;
import com.teachersapp.listner.AllAssignmentFilter;
import com.teachersapp.listner.PageReloadEvent;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Prefs;
import com.teachersapp.utils.Utils;
import com.whiteelephant.monthpicker.MonthPickerDialog;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class AllAssignmentFilterDialog extends BottomSheetDialogFragment implements MonthPickerDialog.OnDateSetListener {

    private static final String TAG = "BottomSheet3DialogFragm";
    @BindView(R.id.mReset)
    Button mReset;
    @BindView(R.id.mSave)
    Button mSave;
    Unbinder unbinder;
    @BindView(R.id.mDate)
    TextView mDate;
    @BindView(R.id.mCalender)
    ImageView mCalender;
    @BindView(R.id.mBatchList)
    Spinner mBatchList;
    @BindView(R.id.mStandard)
    Spinner mStandard;
    List<Standard> standardList;
    List<BatchBean> batchBeans = new ArrayList();
    Gson gson;
    Calendar myCalendar = Calendar.getInstance();

    Prefs prefs;

    public static AllAssignmentFilterDialog newInstance(List<Standard> standardList) {
        AllAssignmentFilterDialog f = new AllAssignmentFilterDialog();
        Bundle b = new Bundle();
        b.putSerializable(Constant.standardList, (Serializable) standardList);
        f.setArguments(b);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.all_assignment_fillter_dailog, container, false);
        unbinder = ButterKnife.bind(this, view);
        prefs = Prefs.with(getActivity());
        gson = new Gson();
        standardList = (List<Standard>) getArguments().getSerializable(Constant.standardList);
        if (!TextUtils.isEmpty(prefs.getString(Constant.selectMonth, "")))
            mDate.setText(prefs.getString(Constant.selectMonth, ""));
        else
            mDate.setText("Select Month");

        StandardAdapter standardAdapter = new StandardAdapter(getActivity(), standardList);
        mStandard.setAdapter(standardAdapter);

        UserData userData = gson.fromJson(prefs.getString(Constant.UserData, ""), UserData.class);
        BatchBean batchBean = new BatchBean();
        batchBean.setBatchID(0);
        batchBean.setBatch("Select Batch");
        batchBean.setAccuracy("0.0");
        batchBean.setLevelAccuracy(new ArrayList());
        batchBeans.add(batchBean);
        batchBeans.addAll(userData.getBatchBeanList());
        mBatchList.setAdapter(new BatchFilterAdapter(getActivity(), batchBeans, true));

        if (prefs.getInt(Constant.selectStandard, -1) != -1) {
            for (int i = 0; i < standardList.size(); i++) {
                if (standardList.get(i).getStandardID() == prefs.getInt(Constant.selectStandard, -1)) {
                    mStandard.setSelection(i);
                    break;
                }
            }
        }

        if (prefs.getInt(Constant.selectedBatch, -1) != -1) {
            for (int i = 0; i < batchBeans.size(); i++) {
                if (batchBeans.get(i).getBatchID() == prefs.getInt(Constant.selectedBatch, -1)) {
                    mBatchList.setSelection(i);
                    break;
                }
            }
        }

        mStandard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Standard standard = (Standard) parent.getItemAtPosition(position);

                Log.d(TAG, "onItemSelected: Standard " + standard.getStandardID());
                prefs.save(Constant.selectStandard, standard.getStandardID());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mBatchList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BatchBean batchBean = (BatchBean) parent.getItemAtPosition(position);
                prefs.save(Constant.selectedBatch, batchBean.getBatchID());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.mReset, R.id.mSave, R.id.mDate, R.id.mCalender})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mDate:
            case R.id.mCalender:
//                DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
//                dialog.show();


                MonthPickerDialog.Builder builder = new MonthPickerDialog.Builder(getActivity(), this, myCalendar.get(Calendar.YEAR), (myCalendar.get(Calendar.MONTH)));
                builder
//                        .setActivatedMonth(Calendar.JULY)
                        .setMinYear(2000)
//                        .setActivatedYear(2017)
//                        .setMaxYear(2030)
//                        .setMinMonth(Calendar.FEBRUARY)
                        .setTitle("Select Month")
//                        .setMonthRange(Calendar.FEBRUARY, Calendar.NOVEMBER)
                        // .setMaxMonth(Calendar.OCTOBER)
                        // .setYearRange(1890, 1890)
                        // .setMonthAndYearRange(Calendar.FEBRUARY, Calendar.OCTOBER, 1890, 1890)
                        //.showMonthOnly()
                        // .showYearOnly()

                        .build()
                        .show();

                break;
            case R.id.mReset:
                mStandard.setSelection(0);
                mBatchList.setSelection(0);
                mDate.setText("Select Month");
                Utils.clearFilter(prefs);
                break;
            case R.id.mSave:
                ((AllAssignmentActivity) getActivity()).allAssignmentFilterDialog.dismiss();
                EventBus.getDefault().post(new AllAssignmentFilter(true));
                break;
        }
    }


    @Override
    public void onDateSet(int selectedMonth, int selectedYear) {
        String date = Utils.changeDated(String.valueOf(new StringBuilder().append(selectedYear).append("/").append(selectedMonth + 1)));
        mDate.setText(date);
        prefs.save(Constant.selectMonth, date);
    }
}
