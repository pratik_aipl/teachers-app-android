package com.teachersapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.teachersapp.R;
import com.teachersapp.adapter.ProfileDetailsAdapter;
import com.teachersapp.base.BaseFragment;
import com.teachersapp.bean.BranchBean;
import com.teachersapp.utils.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BatchFragment extends BaseFragment {

    @BindView(R.id.mBatchList)
    RecyclerView mBatchList;
//    BranchBean branchBean;

    public static Fragment newInstance(BranchBean branchBean) {
        BatchFragment f = new BatchFragment();
        Bundle b = new Bundle();
        b.putSerializable(Constant.selectSubject, branchBean);
        f.setArguments(b);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //getActivity().getWindow().setBackgroundDrawableResource(R.mipmap.category_intro);
        View view = inflater.inflate(R.layout.batch_frafment, container, false);
        ButterKnife.bind(this, view);
        BranchBean branchBean = (BranchBean) getArguments().getSerializable(Constant.selectSubject);

        layoutManager = new LinearLayoutManager(getActivity());
        mBatchList.setLayoutManager(layoutManager);
        mBatchList.setItemAnimator(new DefaultItemAnimator());

        ProfileDetailsAdapter standardAdapter = new ProfileDetailsAdapter(getActivity(), branchBean.getStandardList());
        mBatchList.setAdapter(standardAdapter);


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
