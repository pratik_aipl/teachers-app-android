package com.teachersapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.teachersapp.R;
import com.teachersapp.ReportActivity;
import com.teachersapp.ReportStudentActivity;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.listner.BatchChangeEvent;
import com.teachersapp.listner.BatchOnClick;
import com.teachersapp.listner.ChangeTypeEvent;
import com.teachersapp.utils.Prefs;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BottomSheetTypeDialog extends BottomSheetDialogFragment implements BatchOnClick {

    private static final String TAG = "BottomSheet3DialogFragm";
    @BindView(R.id.mAttend)
    Button mAttend;
    @BindView(R.id.mNotAttend)
    Button mNotAttend;
    @BindView(R.id.mClose)
    Button mReset;
    Unbinder unbinder;

    @BindView(R.id.bottomSheetLayout)
    LinearLayout bottomSheetLayout;


    public static BottomSheetTypeDialog newInstance() {
        BottomSheetTypeDialog f = new BottomSheetTypeDialog();
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fillter_attempt_dailog, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void callbackPaper(int pos, BatchBean batchBean) {

    }


    @OnClick({R.id.mAttend, R.id.mNotAttend, R.id.mClose})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mAttend:
                EventBus.getDefault().post(new ChangeTypeEvent(true));
                ((ReportStudentActivity) getActivity()).bottomSheetTypeDialog.dismiss();
                break;
            case R.id.mNotAttend:
                EventBus.getDefault().post(new ChangeTypeEvent(false));
                ((ReportStudentActivity) getActivity()).bottomSheetTypeDialog.dismiss();
                break;
            case R.id.mClose:
                ((ReportStudentActivity) getActivity()).bottomSheetTypeDialog.dismiss();
                break;
        }
    }
}
