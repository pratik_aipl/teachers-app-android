package com.teachersapp.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.teachersapp.PaperScreen;
import com.teachersapp.R;
import com.teachersapp.ReportActivity;
import com.teachersapp.adapter.QuestionsAdapter;
import com.teachersapp.bean.LevelQuestion;
import com.teachersapp.utils.Constant;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BottomSheetQuestionsDialog extends BottomSheetDialogFragment {

    private static final String TAG = "BottomSheet3DialogFragm";
    @BindView(R.id.mClose)
    Button mReset;
    @BindView(R.id.mQuestions)
    RecyclerView mQuestions;
    Unbinder unbinder;
    boolean isFrom=false;

    public static BottomSheetQuestionsDialog newInstance(List<LevelQuestion> standardList, boolean isReport) {
        BottomSheetQuestionsDialog f = new BottomSheetQuestionsDialog();
        Bundle b = new Bundle();
        b.putSerializable(Constant.questionsList, (Serializable) standardList);
        b.putSerializable(Constant.isReport, isReport);
        f.setArguments(b);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.question_bottom_dailog, container, false);
        unbinder = ButterKnife.bind(this, view);
        isFrom=getArguments().getBoolean(Constant.isReport,false);
        mQuestions.setAdapter(new QuestionsAdapter(getActivity(), (List<LevelQuestion>) getArguments().getSerializable(Constant.questionsList)));
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.mClose})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mClose:
                if (isFrom)
                ((ReportActivity) getActivity()).bottomSheetQuestionsDialog.dismiss();
                else
                ((PaperScreen) getActivity()).bottomSheetQuestionsDialog.dismiss();
                break;
        }
    }
}
