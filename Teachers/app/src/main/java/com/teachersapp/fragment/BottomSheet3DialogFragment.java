package com.teachersapp.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.teachersapp.DashBoard;
import com.teachersapp.R;
import com.teachersapp.adapter.StandardAdapter;
import com.teachersapp.adapter.SubjectAdapter;
import com.teachersapp.bean.Standard;
import com.teachersapp.bean.Subject;
import com.teachersapp.listner.PageReloadEvent;
import com.teachersapp.utils.Constant;
import com.teachersapp.utils.Prefs;
import com.teachersapp.utils.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class BottomSheet3DialogFragment extends BottomSheetDialogFragment implements DatePickerDialog.OnDateSetListener {

    private static final String TAG = "BottomSheet3DialogFragm";
    @BindView(R.id.mReset)
    Button mReset;
    @BindView(R.id.mSave)
    Button mSave;
    Unbinder unbinder;
    @BindView(R.id.mDate)
    TextView mDate;
    @BindView(R.id.mCalender)
    ImageView mCalender;
    @BindView(R.id.mBoard)
    Spinner mBoard;
    @BindView(R.id.mMedium)
    Spinner mMedium;
    @BindView(R.id.mStandard)
    Spinner mStandard;
    @BindView(R.id.mSubject)
    Spinner mSubject;
    List<Standard> standardList;

    Calendar myCalendar = Calendar.getInstance();

    Prefs prefs;

    public static BottomSheet3DialogFragment newInstance(List<Standard> standardList) {
        BottomSheet3DialogFragment f = new BottomSheet3DialogFragment();
        Bundle b = new Bundle();
        b.putSerializable(Constant.standardList, (Serializable) standardList);
        f.setArguments(b);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fillter_bottom_dailog, container, false);
        unbinder = ButterKnife.bind(this, view);
        prefs = Prefs.with(getActivity());
        standardList = (List<Standard>) getArguments().getSerializable(Constant.standardList);
        if (!TextUtils.isEmpty(prefs.getString(Constant.selectDate, "")))
            mDate.setText(prefs.getString(Constant.selectDate, ""));
        else
            mDate.setText("Select Date");

        StandardAdapter standardAdapter = new StandardAdapter(getActivity(), standardList);
        mStandard.setAdapter(standardAdapter);

        if (prefs.getInt(Constant.selectStandard, -1) != -1) {
            for (int i = 0; i < standardList.size(); i++) {
                if (standardList.get(i).getStandardID() ==prefs.getInt(Constant.selectStandard, -1)){
                    mStandard.setSelection(i);
                    break;
                }
            }
        }




        mStandard.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Standard standard = (Standard) parent.getItemAtPosition(position);

                Log.d(TAG, "onItemSelected: Standard " + standard.getStandardID());
                prefs.save(Constant.selectStandard, standard.getStandardID());
                List<Subject> subjectList = new ArrayList<>();
                if (standard.getSubjectList() != null) {
                    Subject subject = new Subject();
                    subject.setStandardID(-1);
                    subject.setSubjectID(-1);
                    subject.setSubjectName("Select Subject");
                    subject.setSubjectOrder(0);
                    subjectList.add(subject);
                    for (int i = 0; i < (standard.getSubjectList().size()); i++) {
                        subjectList.add(standard.getSubjectList().get(i));
                    }
                } else {
                    Subject subject = new Subject();
                    subject.setStandardID(-1);
                    subject.setSubjectID(-1);
                    subject.setSubjectName("Select Subject");
                    subject.setSubjectOrder(0);
                    subjectList.add(subject);
                }
                SubjectAdapter subjectAdapter = new SubjectAdapter(getActivity(), subjectList);
                mSubject.setAdapter(subjectAdapter);

                if (prefs.getInt(Constant.selectSubject, -1) != -1) {
                    for (int i = 0; i < subjectList.size(); i++) {
                        if (subjectList.get(i).getSubjectID() ==prefs.getInt(Constant.selectSubject, -1)){
                            mSubject.setSelection(i);
                            break;
                        }
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mSubject.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Subject subject = (Subject) parent.getItemAtPosition(position);
                Log.d(TAG, "onItemSelected: Subject " + subject.getSubjectID());
                prefs.save(Constant.selectSubject, subject.getSubjectID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick({R.id.mReset, R.id.mSave, R.id.mDate, R.id.mCalender})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mDate:
            case R.id.mCalender:
                DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.show();
                break;
            case R.id.mReset:
                mStandard.setSelection(0);
                mDate.setText("Select Date");
                Utils.clearFilter(prefs);
                break;
            case R.id.mSave:
                ((DashBoard) getActivity()).bottomSheet3DialogFragment.dismiss();
                EventBus.getDefault().post(new PageReloadEvent(true));
                break;
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = Utils.changeDate(String.valueOf(new StringBuilder().append(year).append("/").append(month + 1).append("/").append(dayOfMonth).append(" ")));
        mDate.setText(date);
        prefs.save(Constant.selectDate, date);
    }

}
