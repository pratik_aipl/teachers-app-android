package com.teachersapp.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.calendar.Utils;
import com.calendar.component.CalendarAttr;
import com.calendar.component.CalendarViewAdapter;
import com.calendar.interf.OnSelectDateListener;
import com.calendar.model.CalendarDate;
import com.calendar.view.Calendar;
import com.calendar.view.MonthPager;
import com.teachersapp.PaperScreen;
import com.teachersapp.R;
import com.teachersapp.adapter.PaperAdapter;
import com.teachersapp.base.BaseFragment;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.bean.PaperBeanMain;
import com.teachersapp.bean.PaperDates;
import com.teachersapp.listner.PaperOnClick;
import com.teachersapp.listner.PaperReloadEvent;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;
import com.teachersapp.view.CustomDayView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Subscription;

public class DashBoardFragment extends BaseFragment implements PaperOnClick {

    private static final String TAG = "DashBoardFragment";
    @BindView(R.id.mPrev)
    ImageView mPrev;
    @BindView(R.id.mMonthName)
    TextView mMonthName;
    @BindView(R.id.mNext)
    ImageView mNext;
    @BindView(R.id.calendar_view)
    MonthPager monthPager;
    @BindView(R.id.mPaperList)
    RecyclerView mPaperList;
    @BindView(R.id.content)
    CoordinatorLayout content;

    private int selectedMonth = 0;
    private RecyclerView.LayoutManager layoutManager;
    private PaperAdapter paperAdapter;
    List<PaperBean> paperBeanList = new ArrayList<>();
    List<PaperDates> dates = new ArrayList<>();

    private OnSelectDateListener onSelectDateListener;
    private ArrayList<Calendar> currentCalendars = new ArrayList<>();
    private CalendarViewAdapter calendarAdapter;
    private CalendarDate currentDate;
    Subscription subscriptionPaperList, subscriptionDateList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //getActivity().getWindow().setBackgroundDrawableResource(R.mipmap.category_intro);
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);

        layoutManager = new LinearLayoutManager(getActivity());
        mPaperList.setLayoutManager(layoutManager);
        mPaperList.setItemAnimator(new DefaultItemAnimator());

        paperAdapter = new PaperAdapter(getActivity(), paperBeanList, DashBoardFragment.this);
        mPaperList.setAdapter(paperAdapter);
        monthPager.setViewHeight(Utils.dpi2px(getActivity(), 270));


        initCurrentDate();
        initCalendarView();
        initToolbarClickListener();
        Utils.scrollTo(content, mPaperList, monthPager.getCellHeight(), 200);
        calendarAdapter.switchToWeek(monthPager.getRowIndex());
//        monthPager.setCurrentItem(monthPager.getCurrentPosition() - 1);
        return view;
    }

    private void initCurrentDate() {
        currentDate = new CalendarDate();
        selectedMonth = currentDate.getMonth();
        mMonthName.setText(currentDate.getMonthName() + " " + currentDate.getYear());
        refreshClickDate(currentDate);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onPaperReloadEventEvent(PaperReloadEvent event) {
        for (int i = 0; i < paperBeanList.size(); i++) {
            PaperBean paperBean = paperBeanList.get(i);
            if (paperBean.getMCQPlannerID().equalsIgnoreCase(event.getPaperBean().getMCQPlannerID())) {
                paperBeanList.set(i, event.getPaperBean());
                paperAdapter.notifyDataSetChanged();
            }
        }
    }


    private void initListener() {
        onSelectDateListener = new OnSelectDateListener() {
            @Override
            public void onSelectDate(CalendarDate date) {
                refreshClickDate(date);
            }

            @Override
            public void onSelectOtherMonth(int offset) {
                //Offset -1 means refreshing to last month's data, 1 means refreshing to next month's data
                Log.d(TAG, "onSelectOtherMonth: 00 " + offset);
                Log.d(TAG, "onSelectOtherMonth: 11 " + monthPager.getRowIndex());
                monthPager.selectOtherMonth(offset);
            }
        };
    }

    private void initCalendarView() {
        initListener();
        CustomDayView customDayView = new CustomDayView(getActivity(), R.layout.custom_day);
        calendarAdapter = new CalendarViewAdapter(
                getActivity(),
                onSelectDateListener,
                CalendarAttr.WeekArrayType.Sunday,
                customDayView);
        initMonthPager();
    }

    private void initToolbarClickListener() {
        mPrev.setOnClickListener((View view) -> {
            Log.d(TAG, "initToolbarClickListener: pre " + monthPager.getCurrentPosition());
            Log.d(TAG, "initToolbarClickListener: pre " + (monthPager.getCurrentPosition() - 1));
            monthPager.setCurrentItem(monthPager.getCurrentPosition() - 1);
        });

        mNext.setOnClickListener((View view) -> {
            Log.d(TAG, "initToolbarClickListener: next " + monthPager.getCurrentPosition());
            Log.d(TAG, "initToolbarClickListener: next " + (monthPager.getCurrentPosition() + 1));
            monthPager.setCurrentItem(monthPager.getCurrentPosition() + 1);
        });

    }

    private void initMarkData() throws ParseException {
        HashMap<String, String> markData = new HashMap<>();
        for (int i = 0; i < dates.size(); i++) {
            markData.put(formateDateFromstring("yyyy-MM-dd", "yyyy-M-dd", dates.get(i).getDate()), "0");
        }
        calendarAdapter.setMarkData(markData);
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate) throws ParseException {
        return new SimpleDateFormat(outputFormat, java.util.Locale.getDefault()).format(new SimpleDateFormat(inputFormat, java.util.Locale.getDefault()).parse(inputDate));
    }

    private void initMonthPager() {
        monthPager.setAdapter(calendarAdapter);
        monthPager.setCurrentItem(MonthPager.CURRENT_DAY_INDEX);
        monthPager.setPageTransformer(false, (page, position) -> {
            position = (float) Math.sqrt(1 - Math.abs(position));
            page.setAlpha(position);
        });
        monthPager.addOnPageChangeListener(new MonthPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                currentCalendars = calendarAdapter.getPagers();
                if (currentCalendars.get(position % currentCalendars.size()) != null) {
                    CalendarDate date = currentCalendars.get(position % currentCalendars.size()).getSeedDate();
                    currentDate = date;
                    if (selectedMonth != date.getMonth()) {
                        java.util.Calendar cal = java.util.Calendar.getInstance();
                        cal.set(date.getYear(), date.getMonth() - 1, date.getDay());
                        if (subscriptionDateList != null && !subscriptionDateList.isUnsubscribed())
                            subscriptionDateList.unsubscribe();
                        getDates(false, new SimpleDateFormat("MM/yyyy").format(cal.getTime()));
                    }
                    selectedMonth = date.getMonth();
                    mMonthName.setText(date.getMonthName() + " " + date.getYear());
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    private void refreshClickDate(CalendarDate date) {
        currentDate = date;
        java.util.Calendar cal = java.util.Calendar.getInstance();
        cal.set(date.getYear(), date.getMonth() - 1, date.getDay());
        if (com.teachersapp.utils.Utils.isNetworkAvailable(getActivity(), true))
            getPaperByDate(true, new SimpleDateFormat("yyyy/MM/dd").format(cal.getTime()));
        (paperAdapter).setHeaderText(new SimpleDateFormat("EEEE").format(cal.getTime()) + "\'s Assignments");
//        setMonthName(new SimpleDateFormat("MMMM").format(cal.getTime()));
    }

    @Override
    public void onDestroyView() {
        if (subscriptionPaperList != null && !subscriptionPaperList.isUnsubscribed()) {
            subscriptionPaperList.unsubscribe();
            subscriptionPaperList = null;
        }
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroyView();
    }

    @Override
    public void callbackPaper(int pos, PaperBean paperBean) {
        startActivity(new Intent(getActivity(), PaperScreen.class).putExtra(Constant.paper, paperBean));
    }


    private void getPaperByDate(boolean isShow, String date) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.Date, date);
        showProgress(isShow);
        Log.d(TAG, "getPaperByDate: " + map);
        subscriptionPaperList = NetworkRequest.performAsyncRequest(restApi.getPaperList(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    paperBeanList.clear();
                    dates.clear();
                    PaperBeanMain paperBeanMain = LoganSquare.parse(jsonResponse.get(Constant.data).toString(), PaperBeanMain.class);
                    paperBeanList.addAll(paperBeanMain.getPaperInfo());
                    dates.addAll(paperBeanMain.getDates());
                    if (dates.size() > 0)
                        initMarkData();
                    paperAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                com.teachersapp.utils.Utils.serviceStatusFalseProcess(getActivity(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    private void getDates(boolean isShow, String date) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.Month, date);
        showProgress(isShow);
        Log.d(TAG, "getPaperByDate: " + map);
        subscriptionDateList = NetworkRequest.performAsyncRequest(restApi.getDates(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    dates.clear();
                    dates.addAll(LoganSquare.parseList(jsonResponse.get(Constant.data).toString(), PaperDates.class));
                    if (dates.size() > 0)
                        initMarkData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                com.teachersapp.utils.Utils.serviceStatusFalseProcess(getActivity(), data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

}
