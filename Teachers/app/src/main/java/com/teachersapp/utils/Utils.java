package com.teachersapp.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.text.Html;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.teachersapp.MobileScreen;
import com.teachersapp.R;
import com.teachersapp.bean.UserData;
import com.teachersapp.listner.DialogButtonListener;
import com.teachersapp.view.CircleTransform;

import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Locale;

import retrofit2.Response;

public class Utils {

    static boolean isShowing = false;

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static String getOptionText(int position) {
        return String.valueOf((char) (65 + position));
    }

    public static void showToast(String msg, Context ctx) {
        Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_LONG);
        toast.show();

    }

    public static String stringForTime(long timeMs) {
        if (timeMs <= 0 || timeMs >= 24 * 60 * 60 * 1000) {
            return "00:00";
        }
        long totalSeconds = timeMs / 1000;
        int seconds = (int) (totalSeconds % 60);
        int minutes = (int) ((totalSeconds / 60) % 60);
        int hours = (int) (totalSeconds / 3600);
        StringBuilder stringBuilder = new StringBuilder();
        Formatter mFormatter = new Formatter(stringBuilder, Locale.getDefault());
        if (hours > 0) {
            return mFormatter.format("%d:%02d:%02d", hours, minutes, seconds).toString();
        } else {
            return mFormatter.format("%02d:%02d", minutes, seconds).toString();
        }
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static boolean isNetworkAvailable(Context context, boolean b) {
        boolean isConnected = false;
        if (!Constant.isAlertShow) {
            if (!isNetworkConnected(context)) {
                showTwoButtonDialog(context, context.getString(R.string.network_error_title), context.getString(R.string.network_connection_error), "Ok", null, new DialogButtonListener() {
                    @Override
                    public void onPositiveButtonClicked() {
                        Constant.isAlertShow = false;
                    }

                    @Override
                    public void onNegativButtonClicked() {
                        Constant.isAlertShow = false;
                    }
                });
                Constant.isAlertShow = true;
                isConnected = false;
            } else
                isConnected = true;
        }
        return isConnected;
    }
    public static boolean isNetworkConnected(Context c) {
        boolean isConnected = false;
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) {
            // connected to the internet
            switch (activeNetwork.getType()) {
                case ConnectivityManager.TYPE_WIFI:
                    // connected to wifi
                    isConnected = true;
                    break;
                case ConnectivityManager.TYPE_MOBILE:
                    // connected to mobile data
                    isConnected = true;
                    break;
                default:
                    break;
            }
        } else {
            return false;
        }
        return isConnected;
    }
    private static void generalOkAlert(Context context, String message, Response<String> data) {
        if (context == null)
            return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setPositiveButton(context.getResources().getString(R.string.ok), (dialog, which) -> {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    dialog.dismiss();
                    isShowing = false;
                    if (data != null && data.code() == 401)
                        logout(context);
                    break;
            }
        });
        if (!isShowing) {
            builder.show();
            isShowing = true;
        }else {
            isShowing = false;
        }

    }
    public static void logout(Context context) {
        Prefs prefs = Prefs.with(context);
        prefs.save(Constant.isLogin, false);
        Intent intent = new Intent(context, MobileScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(intent);
    }

    public static String serviceStatusFalseProcess(Context context, Response<String> data) {
        String msg="";
        try {
           /* if (data.code() == 401) {
                L.logout(context);
            } else */
            if (data.code() == 500) {
                generalOkAlert(context, data.message(), data);
            } else {
                String str = data.errorBody().string();
                JSONObject jsonResponse = new JSONObject(str);
                if (jsonResponse.has(Constant.messageAr) && isArabicLanguage(context)) {
                    msg = jsonResponse.getString(Constant.messageAr);
                } else {
                    msg = jsonResponse.getString(Constant.message);
                }
                generalOkAlert(context, msg, data);
            }

        } catch (Exception e) {
            e.printStackTrace();
            generalOkAlert(context, "oops!\nsomething went wrong.\nPlease try again!", null);
            e.printStackTrace();
        }

        return msg;
    }

    public static void showTwoButtonDialog(Context context, String title, String message, String yesButtonName, String noButtonName, DialogButtonListener dialogButtonListener) {

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
        if (title != null)
            alertDialogBuilder.setTitle(title);
        else {
            alertDialogBuilder.setTitle("");
        }
        alertDialogBuilder
                .setMessage(Html.fromHtml(message))
                .setCancelable(false);
        if (yesButtonName != null) {
            yesButtonName = yesButtonName.equals("") ? "YES" : yesButtonName;
            alertDialogBuilder.setPositiveButton(yesButtonName, (dialog, id) -> {
                dialog.cancel();
                if (dialogButtonListener != null) {
                    dialogButtonListener.onPositiveButtonClicked();
                }
            });
        }

        if (noButtonName != null) {
            noButtonName = noButtonName.equals("") ? "NO" : noButtonName;
            alertDialogBuilder.setNegativeButton(noButtonName, (dialog, id) -> {
                dialog.cancel();
                if (dialogButtonListener != null) {
                    dialogButtonListener.onNegativButtonClicked();
                }
            });
        }
        // create alert dialog
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    public static android.support.v7.app.AlertDialog showOneButtonDialog(Context context, String title, String message) {

        android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(context);
        if (title != null)
            alertDialogBuilder.setTitle(title);
        else {
            alertDialogBuilder.setTitle("");
        }
        alertDialogBuilder
                .setMessage(Html.fromHtml(message));
        alertDialogBuilder.setPositiveButton("OK", null);
        android.support.v7.app.AlertDialog alertDialog = alertDialogBuilder.create();

        return alertDialog;
    }


    public static boolean isLogin(Context context) {
        return Prefs.with(context).getBoolean(Constant.isLogin, false);
    }


    public static String getAuthtoken(Context context) {
        return Prefs.with(context).getString(Constant.login_token, "");
    }

    public static UserData getUser(Context context) {
        return new Gson().fromJson(Prefs.with(context).getString(Constant.UserData, ""), UserData.class);
    }


    public static String changeDateToDDMMYYYY(String input) {
        //   21-05-1916
        //   1916-05-21//
      /*  Date date = new Date(input);
        SimpleDateFormat formatter5 = new SimpleDateFormat("yyyy-mm-dd");
        return formatter5.format(date);*/
        String outputDateStr;
        try {
            DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy");
            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // String input="2013-06-24";
            Date date = inputFormat.parse(input);

            outputDateStr = outputFormat.format(date);
            //Log.i("output", outputDateStr);
            return outputDateStr;


        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }

    public static String changeDate(String input) {
        String outputDateStr;
        try {
            DateFormat outputFormat = new SimpleDateFormat("yyyy/MM/dd");
            DateFormat inputFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date date = inputFormat.parse(input);
            outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }

    public static String changeDated(String input) {
        String outputDateStr;
        try {
            DateFormat outputFormat = new SimpleDateFormat("yyyy/MM");
            DateFormat inputFormat = new SimpleDateFormat("yyyy/MM");
            Date date = inputFormat.parse(input);
            outputDateStr = outputFormat.format(date);
            return outputDateStr;
        } catch (ParseException p) {
            p.printStackTrace();
        }
        return "";
    }

    public static void loadImageWithPicasso(Context context, String imagePath, ImageView iv, ProgressBar mProgress) {
        if (!imagePath.isEmpty()) {

            if (mProgress != null)
                mProgress.setVisibility(View.VISIBLE);
            Picasso.with(context)
                    .load(imagePath)
                    .error(R.mipmap.zookiimage).into(iv, new Callback() {
                @Override
                public void onSuccess() {
                    if (mProgress != null)
                        mProgress.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    if (mProgress != null)
                        mProgress.setVisibility(View.GONE);
                }
            });

        } else
            Picasso.with(context).load(R.mipmap.zookiimage).into(iv);
    }

    public static void loadImageWithPicassoRound(String imagePath, ImageView iv, Context context, int width, int height, ProgressBar mProgress) {
//        Log.d(TAG, "loadImageWithPicassoRound: " + imagePath);
        if (!imagePath.isEmpty()) {

            if (mProgress != null)
                mProgress.setVisibility(View.VISIBLE);
            PicassoTrustAll.getInstance(context)
                    .load(imagePath)
                    .transform(new CircleTransform())
                    .placeholder(R.mipmap.ic_user_placeholder)
                    .error(R.mipmap.ic_user_placeholder).into(iv, new Callback() {
                @Override
                public void onSuccess() {
                    if (mProgress != null)
                        mProgress.setVisibility(View.GONE);
                }

                @Override
                public void onError() {
                    if (mProgress != null)
                        mProgress.setVisibility(View.GONE);
                }
            });
        } else {
            PicassoTrustAll.getInstance(context)
                    .load(R.mipmap.ic_user_placeholder)
                    .transform(new CircleTransform()).into(iv);
        }

    }


    public static boolean isArabicLanguage(Context context) {
        Locale current = context.getResources().getConfiguration().locale;
        String language = current.getLanguage();
        return language.equals("ar");
    }

    public static void clearFilter(Prefs prefs) {
        prefs.save(Constant.selectDate, "");
        prefs.save(Constant.selectStandard, -1);
        prefs.save(Constant.selectSubject, -1);
    }
}
