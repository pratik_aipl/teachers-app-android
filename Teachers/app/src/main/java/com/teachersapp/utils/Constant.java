package com.teachersapp.utils;

import android.os.Environment;

public class Constant {


    public static final String DeviceType ="DeviceType";
    public static final String AppType = "AppType";
    public static final String AppCode = "AppCode";
    public static final String android="Android";
    public static final String teacher ="teacher";
    public static String status="status";

    public static final String PlannerID = "PlannerID";
    public static final String StudentID = "StudentID";
    public static String UserData = "UserData";
    public static String login_token = "login_token";
    public static String isLogin="isLogin";
    public static String correct="Correct";
    public static String notappeared="Not Appeared";
    public static String incorrect="In Correct";
    public static String messageAr="messageArabic";
    public static String message="message";
    public static String data="data";
    public static String user_id="user_id";
    public static String otp="otp";
    public static String standardList="standardList";
    public static String chapterBeanList="chapterBeanList";
    public static String paper="paper";
    public static String selectSubject="selectSubject";
    public static String selectStandard="selectStandard";
    public static String selectStandardAll="selectStandardAll";
    public static String selectedBatch = "selectedBatch";
    public static String selectDate="selectDate";
    public static String selectMonth="selectMonth";
    public static String folderPath="folder_path";
    public static final String Message = "Message";
    public static String selQuestion="selQuestion";


    public static boolean isAlertShow=false;

    public static final String MobileNo="MobileNo";
    public static final String Month = "Month";
    public static final String UserID = "UserID";
    public static final String OTP = "OTP";
    public static final String PlayerID = "PlayerID";
    public static final String DeviceID = "DeviceID";
    public static final String Date = "Date";
    public static final String StandardID = "StandardID";
    public static final String SubjectID = "SubjectID";
    public static final String MCQPlannerID = "MCQPlannerID";
    public static final String BatchID = "BatchID";
    public static final String PublishDate = "PublishDate";

    public static String head="head";
    public static String selectedBranch="selectedBranch";

    public static String LOCAL_IMAGE_PATH = Environment.getExternalStorageDirectory() + "/.iTeacher";
    public static String LOCAL_Library_PATH = Environment.getExternalStorageDirectory() + "/Download";


    public static String questionsList="questionsList";
    public static String isReport="isReport";
    public static String levelQuestionsList="levelQuestionList";
    public static String levelName="levelName";
    public static String planner_summary="planner_summary";
    public static String imagePath="imagePath";
    public static String batchList="batchList";
    public static String notAttandStudentList="notAttandStudentList";
    public static String attandStudentList="attandStudentList";
    public static String report1="report1";
    public static String assignment="assignment";
    public static String page="page";
    public static String assignmentpending="Pending Assignment";
    public static String assignmentnext="Next Assignment";
    public static String assignmentmissed="Missed Assignment";
    public static String studentreport="Student Report";
    public static String allAssignment="All Assignment";
    public static String isAssignment="isAssignment";
    public static String startTime="startTime";
    public static String isPlanner="isPlanner";
    public static String Performance="Performance";
    public static String studentMcqTestId="studentMcqTestId";
    public static String StudentMCQTestID="StudentMCQTestID";
    public static String fromAll="fromAll";
    public static String paper_info="paper_info";
    public static String DateMonth="DateMonth";
}
