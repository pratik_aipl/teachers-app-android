package com.teachersapp.utils;

import android.content.Context;

import com.teachersapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDataPump {
    public static HashMap<String, List<String>> getData(Context context) {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> privacypolicy = new ArrayList<String>();
        privacypolicy.add(context.getString(R.string.lipsum));

        List<String> termsconditions = new ArrayList<String>();
        termsconditions.add(context.getString(R.string.lipsum));

        List<String> refundcancellation = new ArrayList<String>();
        refundcancellation.add(context.getString(R.string.lipsum));

        List<String> licencecopyrights = new ArrayList<String>();
        licencecopyrights.add(context.getString(R.string.lipsum));


        expandableListDetail.put("Privacy Policy", privacypolicy);
        expandableListDetail.put("Terms & Conditions", termsconditions);
        expandableListDetail.put("Refund & Cancellation", refundcancellation);
        expandableListDetail.put("Licence & Copyrights", licencecopyrights);
        return expandableListDetail;
    }
}
