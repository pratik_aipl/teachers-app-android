package com.teachersapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bluelinelabs.logansquare.LoganSquare;
import com.teachersapp.adapter.ReportAttendAdapter;
import com.teachersapp.adapter.ReportNotAttendAdapter;
import com.teachersapp.base.ActivityBase;
import com.teachersapp.bean.AttendStudent;
import com.teachersapp.bean.BatchBean;
import com.teachersapp.bean.PaperBean;
import com.teachersapp.bean.PlannerSummary;
import com.teachersapp.fragment.BottomSheetBatchFilter;
import com.teachersapp.fragment.BottomSheetTypeDialog;
import com.teachersapp.listner.BatchFilterEvent;
import com.teachersapp.listner.ChangeTypeEvent;
import com.teachersapp.listner.StudentOnClick;
import com.teachersapp.network.NetworkRequest;
import com.teachersapp.utils.Constant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class ReportStudentActivity extends ActivityBase implements StudentOnClick {

    private static final String TAG = "ReportStudentActivity";
    @BindView(R.id.mBackBtn)
    ImageView mBackBtn;
    @BindView(R.id.mPageTitle)
    TextView mPageTitle;
    @BindView(R.id.mSearchText)
    EditText mSearchText;
    @BindView(R.id.mSearch)
    ImageView mSearch;
    @BindView(R.id.mRightText)
    TextView mRightText;
    @BindView(R.id.mStudent)
    TextView mStudent;
    @BindView(R.id.mStudentViewLine)
    View mStudentViewLine;
    @BindView(R.id.mNotAttempt)
    TextView mNotAttemptStudent;
    @BindView(R.id.mComplete)
    TextView mComplete;
    @BindView(R.id.mCompleteLabel)
    TextView mCompleteLabel;
    @BindView(R.id.mNotCompleteLabel)
    TextView mNotCompleteLabel;
    @BindView(R.id.mQuestionsViewLine)
    View mQuestionsViewLine;
    @BindView(R.id.mAttemptStudents)
    RecyclerView mAttemptStudents;
    @BindView(R.id.mNotAttemptStudents)
    RecyclerView mNotAttemptStudents;

    PaperBean paperBean;
    List<AttendStudent> attendStudentList = new ArrayList<>();
    List<AttendStudent> notAttendStudentList = new ArrayList<>();
    List<BatchBean> batchBeanList = new ArrayList();

    ReportAttendAdapter reportAttendAdapter;
    ReportNotAttendAdapter reportNotAttendAdapter;
    Subscription subscriptionReports;

    public BottomSheetTypeDialog bottomSheetTypeDialog;
    public BottomSheetBatchFilter bottomSheetBatchFilter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_student);
        ButterKnife.bind(this);
        prefs.save(Constant.selectedBatch, -1);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

        paperBean = (PaperBean) getIntent().getSerializableExtra(Constant.paper);
        attendStudentList = (List<AttendStudent>) getIntent().getSerializableExtra(Constant.attandStudentList);
        notAttendStudentList = (List<AttendStudent>) getIntent().getSerializableExtra(Constant.notAttandStudentList);
        batchBeanList = (List) getIntent().getSerializableExtra(Constant.batchList);
        mPageTitle.setText("Student");
        mSearch.setVisibility(View.VISIBLE);
        mNotAttemptStudent.setText(notAttendStudentList.size() + "");
        mComplete.setText(attendStudentList.size() + "");

        reportAttendAdapter = new ReportAttendAdapter(this, attendStudentList);
        mAttemptStudents.setAdapter(reportAttendAdapter);

        reportNotAttendAdapter = new ReportNotAttendAdapter(this, notAttendStudentList);
        mNotAttemptStudents.setAdapter(reportNotAttendAdapter);

    }

    private void showView(boolean isAttend) {
        if (isAttend) {
            mCompleteLabel.setVisibility(View.VISIBLE);
            mAttemptStudents.setVisibility(View.VISIBLE);
            mNotCompleteLabel.setVisibility(View.GONE);
            mNotAttemptStudents.setVisibility(View.GONE);
        } else {
            mCompleteLabel.setVisibility(View.GONE);
            mAttemptStudents.setVisibility(View.GONE);
            mNotCompleteLabel.setVisibility(View.VISIBLE);
            mNotAttemptStudents.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onChangeTypeEvent(ChangeTypeEvent event) {
        showView(event.isAttend());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBatchFilterEvent(BatchFilterEvent batchFilterEvent) {
        getReports(true, prefs.getInt(Constant.selectedBatch, -1));
    }

    private void getReports(boolean isShow, int selectBatch) {
        Map<String, String> map = new HashMap<>();
        map.put(Constant.MCQPlannerID, paperBean.getMCQPlannerID());

        if (selectBatch != 0) {
            map.put(Constant.BatchID, String.valueOf(selectBatch));
        }
        showProgress(isShow);
        Log.d(TAG, "getReports: " + map);
        subscriptionReports = NetworkRequest.performAsyncRequest(restApi.plannerSummary(map), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    PlannerSummary plannerSummary = LoganSquare.parse(jsonResponse.get(Constant.data).toString(), PlannerSummary.class);

                    mNotAttemptStudent.setText("" + plannerSummary.getNotAttandStudent().size());
                    mComplete.setText("" + plannerSummary.getAttandStudent().size());

                    attendStudentList.clear();
                    notAttendStudentList.clear();
                    attendStudentList.addAll(plannerSummary.getAttandStudent());
                    notAttendStudentList.addAll(plannerSummary.getNotAttandStudent());
                    reportAttendAdapter.notifyDataSetChanged();
                    reportNotAttendAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    @OnClick({R.id.mBackBtn, R.id.mCompleteLabel, R.id.mNotCompleteLabel,R.id.mSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mBackBtn:
                onBackPressed();
                break;
            case R.id.mCompleteLabel:
                bottomSheetTypeDialog = BottomSheetTypeDialog.newInstance();
                bottomSheetTypeDialog.show(getSupportFragmentManager(), "Custom Bottom Sheet");
                break;
            case R.id.mNotCompleteLabel:
                bottomSheetTypeDialog = BottomSheetTypeDialog.newInstance();
                bottomSheetTypeDialog.show(getSupportFragmentManager(), "Custom Bottom Sheet");
                break;
            case R.id.mSearch:
                bottomSheetBatchFilter = BottomSheetBatchFilter.newInstance(batchBeanList);
                bottomSheetBatchFilter.show(getSupportFragmentManager(), "Custom Bottom Sheet");
                return;
        }
    }

    @Override
    protected void onDestroy() {
        this.prefs.save(Constant.selectedBatch, -1);
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public void callbackStudent(int pos, AttendStudent attendStudent) {
        startActivity(new Intent(this, ReportStudentDetailsActivity.class)
                .putExtra(Constant.isPlanner, false)
                .putExtra(Constant.PlannerID, this.paperBean.getMCQPlannerID())
                .putExtra(Constant.StudentID, attendStudent));
    }
}
